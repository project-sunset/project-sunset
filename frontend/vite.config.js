import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueI18n from '@intlify/unplugin-vue-i18n/vite'
import { VitePWA } from 'vite-plugin-pwa'
import path from 'path'
import IstanbulPlugin from 'vite-plugin-istanbul'

const addBuildTimestamp = () => {
  return {
    name: 'add-build-timestamp',
    transformIndexHtml() {
      return {
        tags: [{
          tag: 'meta',
          attrs: {
            'name': 'buildtimestamp',
            'content': Date.now()
          }
        }]
      }
    }
  }
}

let BACKEND_URL = 'http://localhost:8000'
if (process.env["VITE_APP_API_URL"]) {
  BACKEND_URL = process.env["VITE_APP_API_URL"].substring(0, process.env["VITE_APP_API_URL"].length - 1)
}

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    assetsDir: 'static',
    sourcemap: true,
    rollupOptions: {
      external: ['/static/blog/bob.jpeg']
    }
  },
  server: {
    hmr: process.env["DISABLE_HMR"] !== "true",
    port: 8080,
    proxy: {
      '/static': BACKEND_URL,
      '/various/timer': BACKEND_URL,
      '/various/memory': BACKEND_URL,
    }
  },
  plugins: [
    vue(),
    addBuildTimestamp(),
    vueI18n({
      compositionOnly: false,
      include: path.resolve(__dirname, './src/locales/**')
    }),
    VitePWA({
      workbox: {
        navigateFallback: null
      },
      includeAssets: ['static/img/favicon.ico', 'static/robots.txt', 'static/img/apple-touch-icon.png'],
      filename: 'service-worker.js',
      manifest: {
        name: 'Project Sunset',
        short_name: 'Project Sunset',
        description: 'Project Sunset',
        theme_color: '#460076',
        icons: [
          {
            src: "./static/img/favicon/android-chrome-512x512.png",
            sizes: "512x512",
            type: "image/png"
          },
          {
            src: "./static/img/favicon/android-chrome-192x192.png",
            sizes: "192x192",
            type: "image/png"
          },
          {
            src: 'static/img/favicon/favicon-32x32.png',
            sizes: '32x32',
            type: 'image/png',
          },
          {
            src: 'static/img/favicon/favicon-16x16.png',
            sizes: '16x16',
            type: 'image/png',
          }
        ]
      }
    }),
    IstanbulPlugin({
      include: "src/*",
      extension: [".js", ".ts", ".vue"],
      requireEnv: true
    })],
  resolve: {
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'],
    alias: {
      "@": path.resolve(__dirname, "./src"),
      'vue-18n': 'vue-i18n/dist/vue-i18n.runtime.esm-bundler.js'
    },
  },
})
