function combinations(a, length) {
  const fn = function (n, src, got, all) {
    if (n === 0) {
      if (got.length > 0 && got.length === length) {
        all[all.length] = got
      }
      return
    }
    for (let j = 0; j < src.length; j++) {
      fn(n - 1, src.slice(j + 1), got.concat([src[j]]), all)
    }
  }
  const all = []
  for (let i = length; i < a.length; i++) {
    fn(i, a, [], all)
  }
  if (a.length === length) all.push(a)
  return all
}

const NUMBERS = [1, 2, 3, 4, 5, 6, 7, 8, 9]

function getNumbersInRow(rowNumber, s) {
  return s[rowNumber].filter(c => c.value !== null || c?.result?.known).map(c => {
    const number = c.value === null ? c.result.number : c.value
    return { id: c.id, number }
  })
}

function getNumbersInColumn(columnNumber, s) {
  return s.map(row => row[columnNumber]).filter(c => c.value !== null || c?.result?.known).map(c => {
    const number = c.value === null ? c.result.number : c.value
    return { id: c.id, number }
  })
}

function getNumbersInSubsquare(subsquare, s) {
  return s.flat().filter(c => c.subsquare === subsquare).filter(c => c.value !== null || c?.result?.known).map(c => {
    const number = c.value === null ? c.result.number : c.value
    return { id: c.id, number }
  })
}

function mirrorNumbers(numbers) {
  if (!Array.isArray(numbers)) {
    throw Error(`Numbers is not an array: ${numbers}`)
  }
  return NUMBERS.filter(n => !numbers.map(c => parseInt(c)).includes(parseInt(n)))
}

function getOptions(cell) {
  return mirrorNumbers(Object.keys(cell.eliminatedNumbers))
}

function parseSudoku(s) {
  const parsedSudoku = []
  for (let row = 0; row < 9; row++) {
    const rowData = []
    for (let column = 0; column < 9; column++) {
      const cell = {
        id: column + row * 9,
        row,
        column,
        value: s[row][column],
        subsquare: Math.floor(column / 3) + Math.floor(row / 3) * 3
      }
      if (s[row][column] === null) {
        cell.eliminatedNumbers = {}
      }
      rowData.push(cell)
    }
    parsedSudoku.push(rowData)
  }
  return parsedSudoku
}

function addEliminateReason(cell, number, reason, s = null) {
  if (cell.value !== null) return false
  if (cell.eliminatedNumbers.hasOwnProperty(number)) {
    cell.eliminatedNumbers[number].push(reason)
    return false
  }
  cell.eliminatedNumbers[number] = [reason]
  return true
}

function eliminateDirect(s) {
  let eliminatedCount = 0
  for (let row = 0; row < 9; row++) {
    for (let column = 0; column < 9; column++) {
      const cell = s[row][column]
      if (cellHasKnownValue(cell)) continue
      getNumbersInRow(row, s).forEach(n => {
        const eliminated = addEliminateReason(cell, n.number, { reason: 'row', location: n.id })
        if (eliminated) eliminatedCount++
      })
      getNumbersInColumn(column, s).forEach(n => {
        const eliminated = addEliminateReason(cell, n.number, { reason: 'column', location: n.id })
        if (eliminated) eliminatedCount++
      })
      getNumbersInSubsquare(cell.subsquare, s).forEach(n => {
        const eliminated = addEliminateReason(cell, n.number, { reason: 'subsquare', location: n.id })
        if (eliminated) eliminatedCount++
      })
    }
  }
  return eliminatedCount
}

function cellHasKnownValue(c) {
  return c.value !== null || c?.result?.known
}

function eliminateIndirect(s) {
  for (let row = 0; row < 9; row++) {
    for (let column = 0; column < 9; column++) {
      // For every cell that is empty
      const cell = s[row][column]
      if (cellHasKnownValue(cell)) continue

      const options = getOptions(cell)
      for (const option of options) {
        // Check each option of this cell, check if the other cells in the subsquare that have this option are all in the same row or column
        const otherCellsWithThatOption = s.flat().filter(c => c.subsquare === cell.subsquare && !cellHasKnownValue(c) && c.id !== cell.id && getOptions(c).includes(option))
        const allSameRow = otherCellsWithThatOption.filter(c => c.row === cell.row).length === otherCellsWithThatOption.length
        const allSameColumn = otherCellsWithThatOption.filter(c => c.column === cell.column).length === otherCellsWithThatOption.length
        if (allSameRow) {
          // We can eliminate this option from cells in the other subsquares of this row
          for (let x = 0; x < 9; x++) {
            const c = s[row][x]
            if (c.subsquare !== cell.subsquare) {
              const eliminated = addEliminateReason(c, option, {
                reason: 'indirect',
                detail: {
                  subsquare: cell.subsquare,
                  direction: 'row',
                  relatedCells: `${cell.id},${otherCellsWithThatOption.map(c => c.id).join(',')}`
                }
              }, s)
              if (eliminated) return true
            }
          }
        }
        if (allSameColumn) {
          // We can elminate this option from cells in the other subsquares of this column
          for (let x = 0; x < 9; x++) {
            const c = s[x][column]
            if (c.subsquare !== cell.subsquare) {
              const eliminated = addEliminateReason(c, option, {
                reason: 'indirect',
                detail: {
                  subsquare: cell.subsquare,
                  direction: 'column',
                  relatedCells: `${cell.id},${otherCellsWithThatOption.map(c => c.id).join(',')}`
                }
              }, s)
              if (eliminated) return true
            }
          }
        }
      }
    }
  }
  return false
}

function* houses(s) {
  for (let i = 0; i < 9; i++) {
    yield { type: 'row', location: i, cells: s[i] }
    yield { type: 'column', location: i, cells: s.map(row => row[i]) }
    yield { type: 'subsquare', location: i, cells: s.flat().filter(c => c.subsquare === i) }
  }
}

function eliminateNakedPairs(s) {
  for (let house of houses(s)) {
    for (let nakedSize = 2; nakedSize < 5; nakedSize++) {
      const cellsToCheck = house.cells.filter(c => c.value === null && (getOptions(c).length >= 2 && getOptions(c).length <= nakedSize))
      if (cellsToCheck.length < nakedSize) continue
      for (let combination of combinations(cellsToCheck, nakedSize)) {
        const optionsInCombination = [...new Set(combination.map(c => getOptions(c)).flat())]
        if (optionsInCombination.length === nakedSize) {
          // Naked pair discovered
          const combinationIds = combination.map(c => c.id)
          house.cells.filter(c => !combinationIds.includes(c.id)).forEach(c => {
            for (let option of optionsInCombination) {
              const eliminated = addEliminateReason(c, option, {
                reason: 'naked-pair',
                detail: {
                  house: house.type,
                  houseIndex: c[house.type],
                  ids: combinationIds,
                  combo: optionsInCombination.join(', ')
                }
              }, s)
              if (eliminated) return true
            }
          })
        }
      }
    }
  }
  return false
}

function checkAllCells(sudoku) {
  let hasFoundResult = false
  for (let row = 0; row < 9; row++) {
    for (let column = 0; column < 9; column++) {
      if (cellHasKnownValue(sudoku[row][column])) continue
      const result = checkCell(row, column, sudoku)
      sudoku[row][column].result = result
      if (result.known) {
        result.index = sudoku.index
        sudoku.index++
        hasFoundResult = true
        eliminateDirect(sudoku)
      }
    }
  }
  return hasFoundResult
}

function checkCell(row, column, s) {
  const cell = s[row][column]
  // single candidate
  if (Object.keys(cell.eliminatedNumbers).length === 8) {
    const number = mirrorNumbers(Object.keys(cell.eliminatedNumbers))[0]
    const allRow = Object.keys(cell.eliminatedNumbers).filter(n => cell.eliminatedNumbers[n].map(r => r.reason).includes('row')).length === 8
    const allColumn = Object.keys(cell.eliminatedNumbers).filter(n => cell.eliminatedNumbers[n].map(r => r.reason).includes('column')).length === 8
    const allSubsquare = Object.keys(cell.eliminatedNumbers).filter(n => cell.eliminatedNumbers[n].map(r => r.reason).includes('subsquare')).length === 8
    let detail = null
    if (allRow) {
      detail = 'row'
    } else if (allColumn) {
      detail = 'column'
    } else if (allSubsquare) {
      detail = 'subsquare'
    } else {
      detail = 'other'
    }
    return { known: true, reason: 'single_candidate', number, detail }
  }

  const options = getOptions(cell)
  // unique option in subsquare
  const otherCellsInSubsquare = s.flat().filter(c => c.subsquare === cell.subsquare && c.id !== cell.id && !cellHasKnownValue(c))
  const uniqueOptionSubsquare = options.filter(option => !otherCellsInSubsquare
    .map(c => getOptions(c))
    .flat()
    .includes(option))
  if (uniqueOptionSubsquare.length === 1) {
    const number = uniqueOptionSubsquare[0]
    return { known: true, reason: 'unique_option', number, detail: 'subsquare' }
  }
  // unique option in row
  const otherCellsInRow = s.flat().filter(c => c.row === cell.row && c.id !== cell.id && !cellHasKnownValue(c))
  const uniqueOptionRow = options.filter(option => !otherCellsInRow
    .map(c => getOptions(c))
    .flat()
    .includes(option))
  if (uniqueOptionRow.length === 1) {
    const number = uniqueOptionRow[0]
    return { known: true, reason: 'unique_option', number, detail: 'row' }
  }
  // unique option in column
  const otherCellsInColumn = s.flat().filter(c => c.column === cell.column && c.id !== cell.id && !cellHasKnownValue(c))
  const uniqueOptionColumn = options.filter(option => !otherCellsInColumn
    .map(c => getOptions(c))
    .flat()
    .includes(option))
  if (uniqueOptionColumn.length === 1) {
    const number = uniqueOptionColumn[0]
    return { known: true, reason: 'unique_option', number, detail: 'column' }
  }
  return { known: false, options: getOptions(cell) }
}

function solveSomething(sudoku) {
  eliminateDirect(sudoku)
  let done = false
  while (!done) {
    checkAllCells(sudoku)
    const eliminatedCount = eliminateDirect(sudoku)
    if (eliminatedCount === 0) {
      const indirectEliminatedSomething = eliminateIndirect(sudoku)
      if (!indirectEliminatedSomething) {
        eliminateNakedPairs(sudoku)
        done = !checkAllCells(sudoku)
      }
    }
  }
}

export function solveSudoku(rawSudoku) {
  const sudoku = parseSudoku(rawSudoku)
  sudoku.index = 0
  solveSomething(sudoku)
  return sudoku
}