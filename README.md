# Project Sunset

Project Sunset is my attempt to automate most of my life.

See https://www.projectsunset.nl/#/about/

## Testing

Test users:
- Superuser: Kara:supergirl01
- user: Barry:flash001

### Create E2E tests

To start creating E2E tests in VS Code
- Press F1, execute task: "Refresh DB"
- Press F5 to run the "compound" lauch configuration
- Press F1, execute task: "npm: codegen - tests"

Now just click around to create the necessary code.

If new database fixtures are needed, run the database refresh, then add the necessary stuff on the website and then run the `store_fixtures.sh` script.
