from django.contrib import admin

from links.models import Application, Category, Url

admin.site.register(Category)
admin.site.register(Application)
admin.site.register(Url)
