from django.contrib.auth.models import User
from django.db import models


class Category(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="links_categories")
    name = models.CharField(max_length=20)

    class Meta:
        verbose_name_plural = 'Categories'
        unique_together = (('name', 'user'),)

    def __str__(self):
        return self.name


class Application(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=20)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="applications")

    def __str__(self):
        return self.name


class Url(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=20)
    link = models.URLField()
    position = models.PositiveIntegerField()
    application = models.ForeignKey(Application, on_delete=models.CASCADE, related_name="urls")

    def __str__(self):
        return self.name
