from backend.permissions import IsOwner
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from links.models import Application, Category, Url
from links.serializers import (ApplicationSerializer, CategorySerializer,
                               UrlSerializer)


class CategoryViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = CategorySerializer
    pagination_class = None

    def get_queryset(self):
        return Category.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ApplicationViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = ApplicationSerializer
    pagination_class = None

    def get_queryset(self):
        return Application.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class UrlViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = UrlSerializer
    pagination_class = None

    def get_queryset(self):
        return Url.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
