from django.urls import include, path
from rest_framework import routers

from links.views import ApplicationViewSet, CategoryViewSet, UrlViewSet

router = routers.DefaultRouter()
router.register(r'categories', CategoryViewSet, basename='category')
router.register(r'applications', ApplicationViewSet, basename='application')
router.register(r'urls', UrlViewSet, basename='url')

urlpatterns = [
    path('api/', include(router.urls)),
]
