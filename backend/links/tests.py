from django.contrib.auth.models import User
from django.test import Client, TestCase

from links.models import Application, Category, Url


class ModelTests(TestCase):
    def setUp(self):
        self.u = User.objects.create(username='Tester', password='secret')

    def test_category_creation(self):
        c = Category.objects.create(user=self.u, name='testje')
        self.assertEqual(c.__str__(), 'testje')

    def test_application_creation(self):
        c = Category.objects.create(user=self.u, name='testje')
        a = Application.objects.create(user=self.u, name='testapp', category=c)
        self.assertEqual(a.__str__(), 'testapp')

    def test_url_creation(self):
        c = Category.objects.create(user=self.u, name='testje')
        a = Application.objects.create(user=self.u, name='testapp', category=c)
        u = Url.objects.create(user=self.u, name='testurl', link='http://localhost/', position=1, application=a)
        self.assertEqual(u.__str__(), 'testurl')


class ViewTests(TestCase):
    fixtures = ['user_test_data.json', 'links_test_data.json']

    def test_categories_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/links/api/categories/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(r[0]['name'], 'Monitoring')
        self.assertEqual(len(r[0]['applications']), 1)

    def test_applications_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/links/api/applications/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(r[0]['name'], 'Metrics')
        self.assertEqual(len(r[0]['urls']), 4)

    def test_urls_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/links/api/urls/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(r[0]['name'], 'ACC')
