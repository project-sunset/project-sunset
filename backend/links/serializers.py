from rest_framework import serializers

from links.models import Application, Category, Url


class UrlSerializer(serializers.ModelSerializer):
    class Meta:
        model = Url
        fields = ('link', 'name', 'position', 'pk', 'application')


class ApplicationSerializer(serializers.ModelSerializer):
    urls = UrlSerializer(read_only=True, many=True)

    class Meta:
        model = Application
        fields = ('name', 'urls', 'pk', 'category')


class CategorySerializer(serializers.ModelSerializer):
    applications = ApplicationSerializer(read_only=True, many=True)

    class Meta:
        model = Category
        fields = ('name', 'applications', 'pk')
