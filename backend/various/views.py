import json

from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from various.sudokusolver import SudokuSolver
from various.settlers import Board
from various.models import Presentation


def memory(request):
    return render(request, 'various/memory.html')

def woordle(request):
    return render(request, 'various/woordle.html')

def presentation(request, name):
    p = Presentation.objects.get(name=name)
    context = {
        'current_slide': p.current_slide,
        'slides': p.slides.split(','),
        'name': name,
    }
    return render(request, 'various/presentation.html', context)


def presentation_slide(_, name):
    p = Presentation.objects.get(name=name)
    return JsonResponse({
        'current_slide': p.current_slide,
    })


def presentation_control(request, name):
    p = Presentation.objects.get(name=name)
    context = {
        'current_slide': p.current_slide,
        'slides': p.slides.split(','),
        'name': name,
    }
    return render(request, 'various/presentation-control.html', context)


@csrf_exempt
def presentation_set(request, name):
    data = json.loads(request.body)
    p = Presentation.objects.get(name=name)
    p.current_slide = data['new_slide']
    p.save()
    return JsonResponse({'current_slide': p.current_slide})


def sudoku(request):
    data = json.loads(request.body)
    data = data.replace('\r\n', '\n')
    data = data.split('\n')
    response = {'error': False}
    for i, row in enumerate(data):
        data[i] = list(row)
    # check data
    if len(data) != 9:
        response['error'] = True
        response['message'] = "There are not 9 rows in your Sudoku"
    for row in data:
        if len(row) != 9:
            response['error'] = True
            response['message'] = "There is a row with not 9 columns in your Sudoku"
    # turn data into the right types
    if not response['error']:
        for i in range(0, 9):
            for j in range(0, 9):
                if data[i][j] == '0':
                    data[i][j] = None
                else:
                    data[i][j] = int(data[i][j])
        s = SudokuSolver(data)
        solution = s.solve()
        return JsonResponse({'error': False, 'solution': solution})
    else:
        return JsonResponse(response)


def timer(request):
    return render(request, "various/timer.html", {})


def settlers(request):
    data = json.loads(request.body)
    tiles = {}
    for tile in data['tiles']:
        tiles[(tile['tile_c']['x'], tile['tile_c']['y'])] = {
            'number': int(tile['number']), 'resource': tile['resource']}
    board = Board(tiles)
    totals = board.totals()
    locations = board.locations()
    return JsonResponse({'total_data': totals, 'location_data': locations})