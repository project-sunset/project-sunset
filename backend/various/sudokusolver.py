import time
import copy
import logging
import random

class SudokuSolver:
    def __init__(self, sudoku):
        self.sudoku = sudoku
        self.stack =[]

    def _get_options(self, row, column):
        numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        if self.sudoku[row][column] is not None:
            return [self.sudoku[row][column],]
        for i in range(0, 9):
            if self.sudoku[row][i] in numbers:
                numbers.remove(self.sudoku[row][i])
            if self.sudoku[i][column] in numbers:
                numbers.remove(self.sudoku[i][column])
        other_numbers = self._get_sub_square_numbers(row, column)
        for number in other_numbers:
            if number in numbers:
                numbers.remove(number)
        return numbers
    
    def _get_sub_square_numbers(self, row, column):
        first_row = row//3*3
        first_column = column//3*3
        numbers = []
        for i in range(0, 3):
            for j in range(0, 3):
                if self.sudoku[first_row+i][first_column+j] is not None:
                    numbers.append(self.sudoku[first_row+i][first_column+j])
        return numbers
    
    def _check(self):
        for i in range(0, 9):
            row = set(self.sudoku[i])
            try:
                row.remove(None)
            except KeyError:
                pass
            if len(row) < 9:
                return False
            column = set([self.sudoku[j][i] for j in range(0, 9)])
            try:
                column.remove(None)
            except KeyError:
                pass
            if len(column) < 9:
                return False
            square = set(self._get_sub_square_numbers(i%3*3, i//3*3))
            if len(square) < 9:
                return False
            return True
            
    def _get_cell_with_fewest_options(self):
        fewest_options = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        cell = (9, 9)
        for i in range(0, 9):
            for j in range(0, 9):
                current_cell_options = self._get_options(i, j)
                if len(current_cell_options) > 1 and len(current_cell_options) < len(fewest_options):
                    fewest_options = current_cell_options
                    cell = (i, j)
        return cell, fewest_options

    def _fill_single_option_cells(self):
        changed = True
        updates = 0
        while changed:
            changed = False
            for i in range(0, 9):
                for j in range(0, 9):
                    if self.sudoku[i][j] is None:
                        options = self._get_options(i, j)
                        if len(options) == 0:
                            return -1
                        if len(options) == 1:
                            self.sudoku[i][j] = options[0]
                            changed = True
                            updates += 1
        return updates
        
    def _print(self, sudoku):
        sudoku = copy.deepcopy(sudoku)
        for row in sudoku:
            for i, x in enumerate(row):
                if x is None:
                    row[i] = 0
                print(row)

    def solve(self):
        while self._check() == False:
            updates = self._fill_single_option_cells()
            if updates == 0:
                branch_cell, options = self._get_cell_with_fewest_options()
                for option in options:
                    sudoku = copy.deepcopy(self.sudoku)
                    sudoku[branch_cell[0]][branch_cell[1]] = option
                    self.stack.append(sudoku)
                self.sudoku = self.stack.pop()
            elif updates == -1:
                try:
                    self.sudoku = self.stack.pop()
                except IndexError:
                    return None
        return self.sudoku