from django.urls import path, register_converter
from datetime import date
from . import views


class DateConverter:
    regex = r'\d{4}-\d{2}-\d{2}'

    def to_python(self, value):
        return date(int(value[0:4]), int(value[5:7]), int(value[8:10]))

    def to_url(self, value):
        return f'{value.year}-{value.month}-{value.day}'


register_converter(DateConverter, 'date')

app_name = 'various'
urlpatterns = [
    path('api/sudoku/', views.sudoku, name='sudoku'),
    path('timer/', views.timer, name="timer"),
    path('api/settlers/', views.settlers, name='settlers'),
    path('presentation/<name>/', views.presentation, name='presentation'),
    path('presentation-slide/<name>/', views.presentation_slide, name='presentation-slide'),
    path('presentation-control/<name>/', views.presentation_control, name='presentation-control'),
    path('presentation-set/<name>/', views.presentation_set, name='presentation-set'),
    path('memory/', views.memory, name="memory"),
    path('woordle/', views.woordle, name="woordle"),
]
