from django.db import models


class Presentation(models.Model):
    name = models.CharField(max_length=100)
    slides = models.TextField()
    current_slide = models.CharField(max_length=100)

    def __str__(self):
        return self.name
