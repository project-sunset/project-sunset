from django.contrib import admin
from .models import *


@admin.register(Presentation)
class PresentationAdmin(admin.ModelAdmin):
    list_display = ('name',)
