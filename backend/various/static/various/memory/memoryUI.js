let time
let totalTime = 0
let turns = 0
let started = false

function moveCardsUI(card1, card2) {
  fadeOut(card1, 50)
  fadeOut(card2, 50)
}

function fadeOut(element, time) {
  let opacity = 1
  let timer = setInterval(function () {
    if (opacity <= 0.1) {
      clearInterval(timer)
      element.className += ' invisible'
    }
    element.style.opacity = opacity
    opacity -= 0.1
  }, time)
}

function victory() {
  // STOP THE TIME!
  stopTime()

  document.getElementById('result').textContent = "Good job! your score is: " + (100 - (totalTime - 10) - (2 * (turns - 10))).toFixed(1)
  document.getElementById('result').className = ''

  // Make the gameboard dissappear by setting the height of all canvas' to 0, and then set display: none;
  let height = document.getElementById('card0').clientHeight
  let canvii = document.getElementsByTagName('canvas')
  let i
  let timer = setInterval(function () {
    if (height <= 1) {
      clearInterval(timer)
      for (i = 0; i < canvii.length; i++) {
        canvii[i].className = 'hidden'
      }
    }
    for (i = 0; i < canvii.length; i++) {
      canvii[i].style.height = height.toString().concat('px')
    }
    height -= 1
  }, 10)
}

function start() {
  if (started === false) {
    document.getElementById('result').className = 'hidden'

    // Clear the data
    totalTime = 0
    turns = 0
    document.getElementById('time').textContent = totalTime
    document.getElementById('turns').textContent = turns

    // Show gameboard again
    let canvii = document.getElementsByTagName('canvas')
    let i
    // Show canvas again
    for (i = 0; i < canvii.length; i++) {
      canvii[i].className = ''
      canvii[i].style.opacity = 1
    }
    // Grow the canvas to the correct height
    let finalHeight = document.getElementById('card0').clientWidth
    let height = 0
    let timer = setInterval(function () {
      if (height >= finalHeight) {
        clearInterval(timer)
      }
      for (i = 0; i < canvii.length; i++) {
        canvii[i].style.height = height.toString().concat('px')
      }
      height += 1
    }, 10)

    // count down
    document.getElementById('countdown').className = 'countdown-container'

    document.getElementById('three').className = 'countdown'
    fadeOut(document.getElementById('three'), 100)

    window.setTimeout(function () {
      document.getElementById('two').className = 'countdown'
      fadeOut(document.getElementById('two'), 100)
    }, 900)

    window.setTimeout(function () {
      document.getElementById('one').className = 'countdown'
      fadeOut(document.getElementById('one'), 100)
    }, 1800)

    // start the time after the countdown finished
    window.setTimeout(function () {
      document.getElementById('countdown').className += 'hidden'
      time = setInterval(setTime, 100)
      started = true
    }, 2700)
  }
}

function setTime() {
  totalTime += 0.1
  document.getElementById('time').textContent = totalTime.toFixed(1)
}

function setTurns() {
  turns += 1
  document.getElementById('turns').textContent = turns
}

function stopTime() {
  if (started === true) {
    clearInterval(time)
    started = false
  }
}
