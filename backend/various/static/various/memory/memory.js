let cards_order = ["circle", "diamond", "heart", "house", "lightning", "parallellogram", "pentagon", "square", "star", "triangle", "circle", "diamond", "heart", "house", "lightning", "parallellogram", "pentagon", "square", "star", "triangle"]
let cards_correct = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false]
let card_1 = -1
let card_2 = -1
let waitingFlip = false
let waitingMove = false

function setup() {
  cards_correct = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false]
  cards_order = shuffleArray(cards_order)
  for (let i = 0; i < 20; i++) {
    clearCanvas(`card${i}`)
  }
  start()
}

function stop() {
  stopTime()
}

function cardClick(index) {
  // check if cards are still waiting to be flipped back
  if (waitingFlip === true) {
    flipCardsBack()
  }
  // check if cards are still waiting to be moved
  if (waitingMove === true) {
    moveCards()
  }
  // check if we've begun playing.
  if (started === false) {
    return
  }
  // check if user clicked a card that is already correct
  else if (cards_correct[index] === true) {
    return
  }
  // check if user clicked the card that was already flipped
  else if (card_1 === index) {
    return
  }
  // check if this is the first card of the two
  else if (card_1 === -1) {
    drawShape(cards_order[index], `card${index}`)
    card_1 = index
  }
  // otherwise it is the second and we need to also check if there is a match
  else {
    drawShape(cards_order[index], `card${index}`)
    card_2 = index
    checkIfMatch()
  }
}

function checkIfMatch() {
  // add one to the turns counter
  setTurns()
  // are the two cards a match?
  if (cards_order[card_1] === cards_order[card_2]) {
    cards_correct[card_1] = true
    cards_correct[card_2] = true
    waitingMove = true
    window.setTimeout(moveCards, 1000)

    // check if victory is achieved
    for (let card in cards_correct) {
      if (cards_correct[card] === false) {
        return
      }
    }
    // for loop hasn't found any wrong cards
    victory()
  }

  // wait for 1.5 seconds and then flip the cards over.
  else {
    waitingFlip = true
    window.setTimeout(flipCardsBack, 1500)
  }
}

// Flips the two cards that are turned back.
function flipCardsBack() {
  if (waitingFlip === true) {
    clearCanvas(`card${card_1}`)
    clearCanvas(`card${card_2}`)
    card_1 = -1
    card_2 = -1
    waitingFlip = false
  }
}

function moveCards() {
  if (waitingMove === true) {
    let card1 = document.getElementById(`card${card_1}`)
    let card2 = document.getElementById(`card${card_2}`)
    moveCardsUI(card1, card2)
    card_1 = -1
    card_2 = -1
    waitingMove = false
  }
}

// Durstenfeld shuffle
function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1))
    let temp = array[i]
    array[i] = array[j]
    array[j] = temp
  }
  return array
}
