class Board:
    """
    The board variable should be given as follows:
    board = {
        (x, y): {
            'number': 7,
            'resource': 'wood|brick|sheep|grain|ore|sea|desert|'
        }
    }
    """

    def __init__(self, board_data):
        self.board_data = board_data
        self.chances = [0, 0, 1/36, 2/36, 3/36, 4/36,
                        5/36, 6/36, 5/36, 4/36, 3/36, 2/36, 1/36]
        self.resources = ['brick', 'grain', 'sheep', 'wood', 'ore']
        self.move = {
            'left_up': (-1, 1),
            'right_up': (0, 1),
            'right': (1, 0),
            'right_down': (1, -1),
            'left_down': (0, -1),
            'left': (-1, 0),
        }
        self.clockwise_adjacent = [
            self.move['left_up'],
            self.move['right_up'],
            self.move['right'],
            self.move['right_down'],
            self.move['left_down'],
            self.move['left'],
            self.move['left_up'],
        ]

    def locations(self):
        """
        Return a sorted (by total output) list containing the resource value of every location.
        [
            {
                "location": {
                    "tile_c": {
                        "x": 2,
                        "y": 3
                    },
                    "type": "bottom"
                },
                "output": {
                    "wood": 0.4,
                    "ore": 0.3,
                    "total": 0.7
                }
            }
        ]
        """
        locations = {}
        for tile in self.board_data:  # loop through all tiles
            for i in range(0, 6):  # loop clockwise through locations
                location = self.location_sort([
                    tile,
                    tuple([original+self.clockwise_adjacent[i][j]
                           for j, original in enumerate(tile)]),
                    tuple([original+self.clockwise_adjacent[i+1][j]
                           for j, original in enumerate(tile)]),
                ])
                if location not in locations:
                    locations[location] = self.output_location(location)
        location_data = [{"location": self.backend_location_to_frontend_location(
            c), "output": o} for c, o in locations.items()]
        location_data.sort(
            key=lambda data: data["output"]["total"], reverse=True)
        return location_data

    def location_sort(self, location):
        """
        Return the sorted location coordinates so that the keys are deterministic.
        Sort clockwise from top or top-right.
        """
        x_max, y_max = None, None
        for coordinate in location:
            if x_max is None or x_max < coordinate[0]:
                x_max = coordinate[0]
            if y_max is None or y_max < coordinate[1]:
                y_max = coordinate[1]
        if (x_max, y_max) in location:
            return ((x_max, y_max), (x_max, y_max-1), (x_max-1, y_max))
        else:
            return ((x_max-1, y_max), (x_max, y_max-1), (x_max-1, y_max-1))

    def backend_location_to_frontend_location(self, location):
        y = [c[1] for c in location]
        a = sum(y)/len(y)
        if a - int(a) > 0.5:
            return {"tile_c": {"x": location[1][0], "y": location[1][1]}, "type": "top"}
        else:
            return {"tile_c": {"x": location[0][0], "y": location[0][1]}, "type": "bottom"}

    def output_location(self, location):
        """
        Return the resource value of the location.
        location = [(x, y), (x, y), (x, y)]
        return = {'wood': 0.111, 'ore': 0.281, 'total': 0.392}
        """
        data = {
            'total': 0
        }
        for coordinate in location:
            try:
                tile = self.board_data[coordinate]
            except KeyError:
                continue
            if tile['resource'] in self.resources:
                output = self.output_tile(tile)
                if tile['resource'] not in data:
                    data[tile['resource']] = 0
                data[tile['resource']] += output
                data['total'] += output
        for resource in self.resources:
            if resource not in data:
                data[resource] = 0
        return data

    def output_tile(self, tile):
        """
        Return the resource value of a tile.
        """
        return self.chances[tile['number']]

    def total_output(self, resource):
        """
        Return the total value of a resource on the board.
        """
        total = 0
        for coordinates, tile in self.board_data.items():
            if tile['resource'] == resource:
                total += self.output_tile(tile)
        return total

    def totals(self):
        """
        Return a dictionary containing the total resource value per resource.
        """
        totals = {'grand_total': 0}
        for resource in self.resources:
            total = self.total_output(resource)
            totals['grand_total'] += total
            totals[resource] = total
        return totals
