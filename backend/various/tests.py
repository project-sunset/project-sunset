import json
from django.test import Client, TestCase
import unittest
from various.sudokusolver import SudokuSolver
from various.settlers import Board
import copy
import random


class SimpleTests(TestCase):
    def test_woordle(self):
        c = Client()
        response = c.get('/various/woordle/')
        self.assertEqual(response.status_code, 200)
    
    def test_memory(self):
        c = Client()
        response = c.get('/various/memory/')
        self.assertEqual(response.status_code, 200)

    def test_timer(self):
        c = Client()
        response = c.get('/various/timer/')
        self.assertEqual(response.status_code, 200)

    def test_sudoku(self):
        c = Client()
        response = c.post('/various/api/sudoku/', r'"060042801\\n100000540\\n000158629\\n007200950\\n300097104\\n508000060\\n209000008\\n604803205\\n000920406"', content_type="application/json")
        self.assertEqual(response.status_code, 200)


sudoku_start = [
    [None, 6, None,    None, 4, 2,    8, None, 1],
    [1, None, None,    None, None, None,    5, 4, None],
    [None, None, None,    1, 5, 8,    6, 2, 9],

    [None, None, 7,    2, None, None,    9, 5, None],
    [3, None, None,    None, 9, 7,    1, None, 4],
    [5, None, 8,    None, None, None,    None, 6, None],

    [2, None, 9,    None, None, None,    None, None, 8],
    [6, None, 4,    8, None, 3,    2, None, 5],
    [None, None, None,    9, 2, None,    4, None, 6],
]

sudoku_solved = [
    [9, 6, 5,    7, 4, 2,    8, 3, 1],
    [1, 8, 2,    3, 6, 9,    5, 4, 7],
    [7, 4, 3,    1, 5, 8,    6, 2, 9],

    [4, 1, 7,    2, 8, 6,    9, 5, 3],
    [3, 2, 6,    5, 9, 7,    1, 8, 4],
    [5, 9, 8,    4, 3, 1,    7, 6, 2],

    [2, 5, 9,    6, 7, 4,    3, 1, 8],
    [6, 7, 4,    8, 1, 3,    2, 9, 5],
    [8, 3, 1,    9, 2, 5,    4, 7, 6],
]

hard_sudoku_start = [
    [None, 7, 9,    6, None, None,    None, None, None],
    [5, None, None,    8, None, None,    None, 1, None],
    [None, None, 4,    7, None, None,    3, None, 9],

    [None, None, None,    None, None, None,    7, None, None],
    [7, 1, 8,    None, None, None,    5, 4, 6],
    [None, None, 5,    None, None, None,    None, None, None],

    [8, None, 7,    None, None, 3,    1, None, None],
    [None, 4, None,    None, None, 1,    None, None, 5],
    [None, None, None,    None, None, 7,    9, 6, None],
]

hard_sudoku_solved = [
    [1, 7, 9,    6, 3, 5,    4, 8, 2],
    [5, 3, 2,    8, 9, 4,    6, 1, 7],
    [6, 8, 4,    7, 1, 2,    3, 5, 9],

    [3, 2, 6,    5, 4, 8,    7, 9, 1],
    [7, 1, 8,    3, 2, 9,    5, 4, 6],
    [4, 9, 5,    1, 7, 6,    2, 3, 8],

    [8, 6, 7,    9, 5, 3,    1, 2, 4],
    [9, 4, 3,    2, 6, 1,    8, 7, 5],
    [2, 5, 1,    4, 8, 7,    9, 6, 3],
]


class TestSudoku(unittest.TestCase):
    def setUp(self):
        self.solver = SudokuSolver(sudoku_start)
        self.solved = SudokuSolver(sudoku_solved)
        self.hard_solver = SudokuSolver(hard_sudoku_start)

    def test_get_options(self):
        options = self.solver._get_options(0, 0)
        self.assertCountEqual(options, [7, 9])
        options = self.solver._get_options(0, 7)
        self.assertCountEqual(options, [3, 7])

    def test_get_sub_square_numbers(self):
        numbers = self.solver._get_sub_square_numbers(7, 8)
        self.assertCountEqual(numbers, [2, 4, 8, 5, 6])

    def test_check(self):
        is_solved = self.solver._check()
        self.assertEqual(is_solved, False)
        is_solved = self.solved._check()
        self.assertEqual(is_solved, True)

    def test_solve(self):
        solution = self.solver.solve()
        self.assertEqual(solution, sudoku_solved)
        solution = self.hard_solver.solve()
        self.assertEqual(solution, hard_sudoku_solved)

    def test_get_cell_with_fewest_options(self):
        self.hard_solver._fill_single_option_cells()
        branch_cell, options = self.hard_solver._get_cell_with_fewest_options()
        self.assertEqual(branch_cell, (0, 5))
        self.assertCountEqual(options, [2, 5])

    def test_stress(self):
        for _ in range(0, 10):
            s = copy.deepcopy(sudoku_start)
            percentage_to_delete = random.randrange(50, 68)/100
            for i in range(0, 9):
                for j in range(0, 9):
                    if random.random() < percentage_to_delete:
                        s[i][j] = None
            solver = SudokuSolver(s)
            solver.solve()

###########################
# SETTLERS OF CATAN TESTS #
###########################


data = {
    (0, 0): {
        'resource': 'desert',
        'number': 0,
    },

    (0, 1): {
        'resource': 'brick',
        'number': 8,
    },
    (1, 0): {
        'resource': 'grain',
        'number': 9,
    },
    (1, -1): {
        'resource': 'grain',
        'number': 10,
    },
    (0, -1): {
        'resource': 'wood',
        'number': 2,
    },
    (-1, 0): {
        'resource': 'sheep',
        'number': 5,
    },
    (-1, 1): {
        'resource': 'sheep',
        'number': 3,
    },


    (-1, 2): {
        'resource': 'grain',
        'number': 5,
    },
    (0, 2): {
        'resource': 'ore',
        'number': 9,
    },
    (1, 1): {
        'resource': 'ore',
        'number': 10,
    },
    (2, 0): {
        'resource': 'sheep',
        'number': 12,
    },
    (2, -1): {
        'resource': 'wood',
        'number': 11,
    },
    (2, -2): {
        'resource': 'sheep',
        'number': 8,
    },
    (1, -2): {
        'resource': 'wood',
        'number': 4,
    },
    (0, -2): {
        'resource': 'grain',
        'number': 11,
    },
    (-1, -1): {
        'resource': 'brick',
        'number': 3,
    },
    (-2, 0): {
        'resource': 'ore',
        'number': 6,
    },
    (-2, 1): {
        'resource': 'brick',
        'number': 4,
    },
    (-2, -2): {
        'resource': 'wood',
        'number': 6,
    },


    (-1, 3): {
        'resource': 'sea',
        'number': 0,
    },
    (0, 3): {
        'resource': 'sea',
        'number': 0,
    },
    (1, 2): {
        'resource': 'sea',
        'number': 0,
    },
    (2, 1): {
        'resource': 'sea',
        'number': 0,
    },
    (3, 0): {
        'resource': 'sea',
        'number': 0,
    },
    (3, -1): {
        'resource': 'sea',
        'number': 0,
    },
    (3, -2): {
        'resource': 'sea',
        'number': 0,
    },
    (3, -3): {
        'resource': 'sea',
        'number': 0,
    },
    (2, -3): {
        'resource': 'sea',
        'number': 0,
    },
    (1, -3): {
        'resource': 'sea',
        'number': 0,
    },
    (0, -3): {
        'resource': 'sea',
        'number': 0,
    },
    (-1, -2): {
        'resource': 'sea',
        'number': 0,
    },
    (-2, -1): {
        'resource': 'sea',
        'number': 0,
    },
    (-3, 0): {
        'resource': 'sea',
        'number': 0,
    },
    (-3, -1): {
        'resource': 'sea',
        'number': 0,
    },
    (-3, 2): {
        'resource': 'sea',
        'number': 0,
    },
    (-3, -3): {
        'resource': 'sea',
        'number': 0,
    },
    (-2, 3): {
        'resource': 'sea',
        'number': 0,
    },
}


class SettlersTest(unittest.TestCase):
    def setUp(self):
        self.board = Board(data)

    def test_totals(self):  # also tests total_output and output_tile
        totals = self.board.totals()
        self.assertAlmostEqual(totals['grand_total'], 58/36)
        self.assertAlmostEqual(totals['brick'], 10/36)
        self.assertAlmostEqual(totals['grain'], 13/36)
        self.assertAlmostEqual(totals['sheep'], 12/36)
        self.assertAlmostEqual(totals['wood'], 11/36)
        self.assertAlmostEqual(totals['ore'], 12/36)

    def test_output_location(self):
        output = self.board.output_location([(-2, 0), (-1, 0), (-2, 1)])
        self.assertAlmostEqual(output['brick'], 3/36)
        self.assertAlmostEqual(output['sheep'], 4/36)
        self.assertAlmostEqual(output['ore'], 5/36)

    def test_output_location_sum(self):
        output = self.board.output_location([(0, 1), (0, 2), (1, 1)])
        self.assertAlmostEqual(output['ore'], 7/36)
        self.assertAlmostEqual(output['brick'], 5/36)

    def test_location_sort(self):
        location = self.board.location_sort([(3, -3), (2, -3), (2, -2)])
        self.assertEqual(location, ((2, -2), (3, -3), (2, -3)))
        location = self.board.location_sort([(0, -2), (1, -2), (1, -3)])
        self.assertEqual(location, ((1, -2), (1, -3), (0, -2)))

    def test_locations(self):
        locations = self.board.locations()
        test_location = locations[0]
        self.assertEqual(test_location['location'], {'tile_c': {'x': 0, 'y': 1}, 'type': 'top'})
        self.assertAlmostEqual(test_location['output']['ore'], 4/36)
        self.assertAlmostEqual(test_location['output']['brick'], 5/36)
        self.assertAlmostEqual(test_location['output']['grain'], 4/36)
        self.assertAlmostEqual(test_location['output']['total'], 13/36)
