#!/bin/bash

PARAMS="--indent 2"

python3 manage.py dumpdata $PARAMS \
  auth.user  \
  --output backend/fixtures/users.test.json

APPS=(finances groceries links overtime_tracker quiz)

for app in "${APPS[@]}"
do
  python3 manage.py dumpdata $PARAMS \
    $app  \
    --output $app/fixtures/$app.test.json
done