from rest_framework import filters, serializers


class UrlQueryFilter(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if not hasattr(view, 'filter_fields'):
            return queryset
        query_filters = {}
        query_order = []
        for field in [f for f in view.filter_fields if f in request.query_params]:
            or_values = []
            for value in request.query_params.getlist(field):
                if value == 'null':
                    query_filters[f'{field}__isnull'] = True
                elif value[0:3] == 'ge(':
                    query_filters[f'{field}__gte'] = value[3:-1]
                elif value[0:3] == 'lt(':
                    query_filters[f'{field}__lt'] = value[3:-1]
                elif value[0:3] == 'co(':
                    query_filters[f"{field}__icontains"] = value[3:-1]
                elif value[0:5] == 'sort(':
                    if value[5:-1] == 'desc':
                        query_order.append('-' + field)
                    else:
                        query_order.append(field)
                else:
                    or_values.append(value)
            if len(or_values) > 0:
                query_filters[f'{field}__in'] = or_values
        if len(query_order) == 0:
            return queryset.filter(**query_filters)
        return queryset.filter(**query_filters).order_by(*query_order)


class UserFilteredPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        request = self.context.get('request', None)
        queryset = super(UserFilteredPrimaryKeyRelatedField,
                         self).get_queryset()
        if not request or not queryset:
            return None
        return queryset.filter(user=request.user)


class FilterArchivedListSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        data = data.filter(archived=False)
        return super().to_representation(data)
