import logging
import time
import json
import traceback

logger = logging.getLogger('backend')


class LoggingMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        t1 = time.perf_counter()
        url = request.path_info

        response = self.get_response(request)
        try:
            user = request.user.pk
        except AttributeError:
            user = 'AnonymousUser'
        try:
            session = request.session.session_key
        except AttributeError:
            session = 'NoSession'

        t2 = time.perf_counter()
        event = json.dumps({
            'severity': 'INFO',
            'time': round(time.time(), 2),
            'user': user,
            'url': url,
            'method': request.method,
            'http_status_code': response.status_code,
            'duration': round((t2-t1)*1000, 0),
            'session': session,
            'request-content-length': 0 if request.META.get('CONTENT_LENGTH', '') == '' else int(request.META['CONTENT_LENGTH']),
            'response-content-length': 0 if response.get('Content-Length', '') == '' else int(response.get('Content-Length')),
            'user-agent': request.META.get('HTTP_USER_AGENT', None),
            'remote-addr': request.META.get('HTTP_X_REAL_IP', None),
            'referrer': request.META.get('HTTP_REFERER', None)
        })
        logger.info(event)
        return response

    def process_view(self, *_):
        return None

    def process_exception(self, request, exception):
        url = request.path_info
        try:
            user = request.user.pk
        except AttributeError:
            user = None
        try:
            session = request.session.session_key
        except AttributeError:
            session = None
        event = json.dumps({
            'severity': 'ERROR',
            'time': time.time(),
            'user': user,
            'url': url,
            'method': request.method,
            'session': session,
            'exception': {
                'message': str(exception),
                'type': type(exception).__name__,
                'traceback': traceback.format_tb(exception.__traceback__)
            },
        }, skipkeys=True)
        logger.error(event)
        return None
