"""
Settings for local development and testing.
"""
from fnmatch import fnmatch
from django.core.files.storage import FileSystemStorage
from backend.settings.base import *

SECRET_KEY = 'n8e)8@*%$p)j@w!-$m+2qwz(eqaxv+2x2!hx6j-l2&a*hq8rzy'
DEBUG = True

ALLOWED_HOSTS = ['*']

WEBAUTHN = {
    'rp_id': 'localhost',
    'origin': 'http://localhost:8080'
}

SECURE_SSL_REDIRECT = False


class glob_list(list):
    def __contains__(self, key):
        for elt in self:
            if fnmatch(key, elt):
                return True
        else:
            return False


# This allows the debug() context processor to add some variables to the template context
# which allows to check whether debug is true
INTERNAL_IPS = glob_list([
    '127.0.0.1',
    '192.168.*.*',
    'localhost',
])

FIXTURE_DIRS = ['backend/fixtures']


MIDDLEWARE = [
    'backend.dev_middleware.AddAccessControlAllowOrigin',
    'backend.dev_middleware.AlwaysReturnOkOnOPTIONS',
    'backend.dev_middleware.DisableCsrfInDrf',
] + [m for m in MIDDLEWARE if m != 'django.middleware.csrf.CsrfViewMiddleware']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        # This bug is hit without this option specified
        # https://github.com/python/cpython/issues/118172
        "OPTIONS": {
            'cached_statements': 0
        }
    }
}

B2_STORAGE = FileSystemStorage(location='local-upload')

STATIC_ROOT = 'static'
MEDIA_ROOT = 'local-upload'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'class': 'logging.FileHandler',
            'filename': 'local_debug.log',
            'mode': 'w',
        },
        'console': {
            'class': 'logging.StreamHandler',
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'INFO',
        },
        'backblaze': {
            'handlers': ['console']
        }
    },
}
