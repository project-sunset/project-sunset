"""
Settings for the production environment
"""
from backend.settings.base import *
from shared_django.b2django.B2Storage import B2Storage
from .config_production import *
from .docker import *

ALLOWED_HOSTS = ['www.projectsunset.nl']

WEBAUTHN = {
    'rp_id': 'www.projectsunset.nl',
    'origin': 'https://www.projectsunset.nl'
}

B2_STORAGE = B2Storage()
