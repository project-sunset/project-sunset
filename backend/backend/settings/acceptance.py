"""
Settings for the acceptance environment
"""
from django.core.files.storage import FileSystemStorage
from backend.settings.base import *
from .docker import *
try:
    from .config_acceptance import *
except ModuleNotFoundError:
    pass

DEPLOYED_URL = 'www-acc.projectsunset.nl'

ALLOWED_HOSTS = [DEPLOYED_URL]

WEBAUTHN = {
    'rp_id': DEPLOYED_URL,
    'origin': f'https://{DEPLOYED_URL}'
}

B2_STORAGE = FileSystemStorage(location='songs-audio')
