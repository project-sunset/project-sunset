import blog.urls
import finances.urls
import groceries.urls
import links.urls
import overtime_tracker.urls
import projectsunset.urls
import shared_django.accounts.urls
import shared_django.django_webauthn.urls
import various.urls
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
import quiz.urls

urlpatterns = [
    path('', include(projectsunset.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
    path('overtime/', include(overtime_tracker.urls)),
    path('various/', include(various.urls)),
    path('groceries/', include(groceries.urls)),
    path('finances/', include(finances.urls, namespace='finances')),
    path('links/', include(links.urls)),
    path('quiz/', include(quiz.urls)),
    path('webauthn/', include(shared_django.django_webauthn.urls)),
    path('accounts/', include(shared_django.accounts.urls)),
    path('discord/', include('discordbot.urls')),
    path('blog/', include(blog.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
