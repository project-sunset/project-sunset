from django.urls import include, path
from rest_framework import routers

from groceries.views import (GroceryListItemViewSet, GroceryListRecipeViewSet,
                             GroceryListViewSet, IngredientViewSet,
                             ItemViewSet, RecipeViewSet, UnitViewSet)

router = routers.DefaultRouter()
router.register(r'units', UnitViewSet, basename='unit')
router.register(r'ingredients', IngredientViewSet, basename='ingredient')
router.register(r'recipes', RecipeViewSet, basename='recipe')
router.register(r'grocery-lists', GroceryListViewSet, basename="grocerylist")
router.register(r'items', ItemViewSet, basename='item')
router.register(r'grocery-list-items', GroceryListItemViewSet, basename='grocerylistitem')
router.register(r'grocery-list-recipes', GroceryListRecipeViewSet, basename='grocerylistrecipe')

urlpatterns = [
    path('api/', include(router.urls)),
]
