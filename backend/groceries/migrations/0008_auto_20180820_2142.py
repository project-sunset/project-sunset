# Generated by Django 2.0.5 on 2018-08-20 19:42

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('groceries', '0007_auto_20180811_0905'),
    ]

    operations = [
        migrations.CreateModel(
            name='GroceryListRecipe',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('people', models.IntegerField()),
            ],
        ),
        migrations.RemoveField(
            model_name='grocerylist',
            name='recipes',
        ),
        migrations.AddField(
            model_name='grocerylist',
            name='recipes',
            field=models.ManyToManyField(through='groceries.GroceryListRecipe', to='groceries.Recipe'),
        ),
        migrations.AlterField(
            model_name='grocerylistitem',
            name='grocery_list',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='grocery_list_items', to='groceries.GroceryList'),
        ),
        migrations.AddField(
            model_name='grocerylistrecipe',
            name='grocery_list',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='grocery_list_recipes', to='groceries.GroceryList'),
        ),
        migrations.AddField(
            model_name='grocerylistrecipe',
            name='recipe',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='groceries.Recipe'),
        ),
        migrations.AddField(
            model_name='grocerylistrecipe',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
