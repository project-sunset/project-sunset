# Generated by Django 2.0.5 on 2018-08-07 19:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('groceries', '0005_auto_20180805_1427'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='grocerylist',
            name='date',
        ),
        migrations.RemoveField(
            model_name='grocerylist',
            name='finished',
        ),
        migrations.AddField(
            model_name='grocerylist',
            name='date_finished',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
