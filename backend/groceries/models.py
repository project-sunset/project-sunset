from datetime import date

from django.contrib.auth.models import User
from django.db import models


class Unit(models.Model):
    name = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name


class Item(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    unit = models.ForeignKey(
        Unit, on_delete=models.PROTECT, null=True, blank=True)
    name = models.CharField(max_length=50)
    supermarket_order = models.IntegerField(default=50)

    class Meta:
        unique_together = (('name', 'user'),)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    recipe = models.ForeignKey('Recipe', on_delete=models.CASCADE, blank=True)
    amount = models.FloatField()

    def __str__(self):
        return f'{self.item} in {self.recipe}'


class Recipe(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    instructions = models.TextField()
    items = models.ManyToManyField(Item, through=Ingredient)
    date_last_used = models.DateField(default=date(2016, 1, 1))
    default_servings = models.IntegerField(default=2)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = (('name', 'user'),)


class GroceryList(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_finished = models.DateTimeField(null=True, blank=True)
    items = models.ManyToManyField(Item, through='GroceryListItem')
    recipes = models.ManyToManyField(Recipe, through='GroceryListRecipe')

    def __str__(self):
        return str(self.date_finished)

    def save(self, *args, **kwargs):
        if not self.pk:
            super().save(*args, **kwargs)
            longest_not_used_recipes = Recipe.objects.filter(
                user=self.user).order_by('date_last_used')[:2]
            for recipe in longest_not_used_recipes:
                GroceryListRecipe.objects.create(
                    user=self.user, grocery_list=self, recipe=recipe, people=recipe.default_servings)
        else:
            super().save(*args, **kwargs)
            if self.date_finished is not None:
                for recipe in self.recipes.all():
                    recipe.date_last_used = self.date_finished
                    recipe.save()


class GroceryListItem(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    grocery_list = models.ForeignKey(
        GroceryList, on_delete=models.CASCADE, related_name="grocery_list_items")
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    amount = models.IntegerField()


class GroceryListRecipe(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    grocery_list = models.ForeignKey(
        GroceryList, on_delete=models.CASCADE, related_name="grocery_list_recipes")
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    people = models.IntegerField()
