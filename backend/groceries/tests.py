from django.contrib.auth.models import User
from django.test import Client, TestCase

from groceries.models import GroceryList, Ingredient, Item, Recipe, Unit


class ModelTests(TestCase):
    def setUp(self):
        self.u = User.objects.create(username='Tester', password='secret')

    def test_unit_creation(self):
        u = Unit.objects.create(name="gram")
        self.assertTrue(isinstance(u, Unit))
        self.assertEqual(u.__str__(), 'gram')

    def test_item_creation(self):
        i = Item.objects.create(user=self.u, name='egg')
        self.assertTrue(isinstance(i, Item))
        self.assertEqual(i.__str__(), 'egg')

    def test_recipe_creation(self):
        r = Recipe.objects.create(user=self.u, name='Fried egg', instructions="Fry egg")
        self.assertTrue(isinstance(r, Recipe))
        self.assertEqual(r.__str__(), 'Fried egg')

    def test_ingredient_creation(self):
        item = Item.objects.create(user=self.u, name='egg')
        r = Recipe.objects.create(user=self.u, name='Fried egg', instructions="Fry egg")
        i = Ingredient.objects.create(user=self.u, item=item, recipe=r, amount=2)
        self.assertTrue(isinstance(i, Ingredient))
        self.assertEqual(i.__str__(), 'egg in Fried egg')

    def test_grocerylist_creation(self):
        gl = GroceryList.objects.create(user=self.u)
        self.assertTrue(isinstance(gl, GroceryList))
        self.assertEqual(gl.__str__(), 'None')


class ViewTests(TestCase):
    fixtures = ['user_test_data.json', 'groceries_test_data.json']

    def test_unit_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/groceries/api/units/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(r['count'], 3)

    def test_unit_get_item(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/groceries/api/units/?itemname=Rice')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(r['count'], 1)

    def test_unit_get_item_none(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/groceries/api/units/?itemname=Blabla')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(r['count'], 0)

    def test_ingredient_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/groceries/api/ingredients/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(r['count'], 3)

    def test_recipe_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/groceries/api/recipes/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(r['count'], 2)

    def test_recipe_post(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.post('/groceries/api/recipes/',
                          '{"name":"Pasta with eggs","instructions":"Cook pasta, boil egg.","ingredient_set":[{"item":{"name":"Pasta","unit":{"name":"gram"}},"amount":"200","id":"ing_4"},{"item":{"name":"Egg","unit":{"name":"_","id":2}},"amount":"2","id":"ing_5"}]}', 'application/json')
        self.assertEqual(response.status_code, 201)

    def test_recipe_put(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.put('/groceries/api/recipes/1/',
                         '{"id": 1,"name":"Rice and beans","instructions":"Cook and heat, and stuff.","date_last_used":"2016-01-01","pk":1,"ingredient_set":[{"id":1,"item":{"id":1,"unit":{"name":"gram","id":1},"name":"Rice","supermarket_order":50},"recipe":1,"amount":200},{"item":{"name":"Black beans","unit":{"name":"gram"}},"amount":"250","id":"ing_1"}]}', 'application/json')
        self.assertEqual(response.status_code, 200)

    def test_grocery_list_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/groceries/api/grocery-lists/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(r['count'], 1)

    def test_grocery_list_post(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.post('/groceries/api/grocery-lists/')
        self.assertEqual(response.status_code, 201)

    def test_grocery_list_get_list(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/groceries/api/grocery-lists/1/get_list/')
        self.assertEqual(response.status_code, 200)

    def test_item_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/groceries/api/items/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(r['count'], 4)
