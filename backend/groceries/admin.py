from django.contrib import admin
from .models import *

@admin.register(Unit)
class UnitAdmin(admin.ModelAdmin):
    list_display = ('name',)

@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = ('user', 'item', 'recipe')

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = ('name', 'user')

@admin.register(GroceryList)
class GroceryListAdmin(admin.ModelAdmin):
    list_display = ('date_finished', 'user')

@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'user')

@admin.register(GroceryListItem)
class GroceryListItemAdmin(admin.ModelAdmin):
    list_display = ('user', 'grocery_list', 'item', 'amount')