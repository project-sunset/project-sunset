from rest_framework import serializers

from groceries.models import (GroceryList, GroceryListItem, GroceryListRecipe,
                              Ingredient, Item, Recipe, Unit)


class UnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Unit
        fields = ('name', 'id')
        extra_kwargs = {
            'name': {
                'validators': []
            }
        }


class ItemSerializer(serializers.ModelSerializer):
    unit = UnitSerializer()

    class Meta:
        model = Item
        fields = ('id', 'unit', 'name', 'supermarket_order')
    
    def create(self, validated_data):
        unit_name = validated_data.pop('unit')['name']
        unit = Unit.objects.get(name=unit_name)
        item = Item.objects.create(unit=unit, **validated_data)
        return item
    
    def update(self, instance, validated_data):
        unit_name = validated_data.pop('unit')['name']
        unit = Unit.objects.get(name=unit_name)
        instance.name = validated_data.get('name', instance.name)
        instance.supermarket_order = validated_data.get('supermarket_order', instance.supermarket_order)
        instance.unit = unit
        instance.save()
        return instance


class IngredientSerializer(serializers.ModelSerializer):
    item = ItemSerializer()

    class Meta:
        model = Ingredient
        fields = ('id', 'item', 'recipe', 'amount')


class RecipeSerializer(serializers.ModelSerializer):
    ingredient_set = IngredientSerializer(many=True)

    class Meta:
        model = Recipe
        fields = ('id', 'name', 'instructions', 'date_last_used', 'ingredient_set', 'default_servings')

    def create(self, validated_data):
        ingredient_data = validated_data.pop('ingredient_set')
        r = Recipe(**validated_data)
        r.save()
        ris = Item.objects.filter(user=validated_data['user']).values_list('name', flat=True)

        for ingredient in ingredient_data:
            # check if a item exists for the ingredient, otherwise create one
            if ingredient['item']['name'] in ris:
                ri = Item.objects.get(user=validated_data['user'], name=ingredient['item']['name'])
            else:
                ri = Item(user=validated_data['user'], unit=Unit.objects.get(name=ingredient['item']['unit']['name']), name=ingredient['item']['name'])
                ri.save()
            # create ingredients
            ing = Ingredient(user=validated_data['user'], item=ri, recipe=r, amount=ingredient['amount'])
            ing.save()
        return r

    def update(self, instance, validated_data):
        r = instance
        ris = Item.objects.filter(user=validated_data['user']).values_list('name', flat=True)
        ings = Ingredient.objects.filter(user=validated_data['user'], recipe=r)
        ris_of_r = ings.values_list('item__name', flat=True)

        if r.name != validated_data['name'] or r.instructions != validated_data['instructions']:
            r.name = validated_data['name']
            r.instructions = validated_data['instructions']
            r.save()

        for ingredient in validated_data['ingredient_set']:
            # create item if it doesn't exist yet for this user
            if ingredient['item']['name'] not in ris:
                ri = Item(user=validated_data['user'], unit=Unit.objects.get(name=ingredient['item']['unit']['name']), name=ingredient['item']['name'])
                ri.save()
            # create ingredient if the item isn't an ingredient yet
            if ingredient['item']['name'] not in ris_of_r:
                ing = Ingredient(user=validated_data['user'], item=Item.objects.get(user=validated_data['user'], name=ingredient['item']['name']), recipe=r, amount=ingredient['amount'])
                ing.save()
            # otherwise maybe update the ingredient
            else:
                ing = ings.get(item__name=ingredient['item']['name'])
                if ing.amount != ingredient['amount']:
                    ing.amount = ingredient['amount']
                    ing.save()
        # delete ingredients that are no longer ingredients
        current = [ing['item']['name'] for ing in validated_data['ingredient_set']]
        for ing in ings:
            if ing.item.name not in current:
                ing.delete()
        return r


class GroceryListItemSerializer(serializers.ModelSerializer):
    item = ItemSerializer()

    class Meta:
        model = GroceryListItem
        fields = ('id', 'item', 'amount')

    def create(self, validated_data):
        item = validated_data.pop('item')
        i, _ = Item.objects.get_or_create(name=item['name'], user=validated_data['user'], unit=Unit.objects.get(name=item['unit']['name']))
        gl = GroceryList.objects.get(user=validated_data['user'], date_finished__isnull=True)
        gli = GroceryListItem(item=i, grocery_list=gl, **validated_data)
        gli.save()
        return gli


class GroceryListRecipeSerializer(serializers.ModelSerializer):
    recipe = RecipeSerializer(read_only=True)
    recipe_name = serializers.CharField(write_only=True)

    class Meta:
        model = GroceryListRecipe
        fields = ('id', 'recipe', 'people', 'recipe_name')

    def create(self, validated_data):
        recipe_name = validated_data.pop('recipe_name')
        r = Recipe.objects.get(name=recipe_name, user=validated_data['user'])
        gl = GroceryList.objects.get(date_finished__isnull=True, user=validated_data['user'])
        glr = GroceryListRecipe(recipe=r, grocery_list=gl, **validated_data)
        glr.save()
        return glr


class GroceryListSerializer(serializers.ModelSerializer):
    items = GroceryListItemSerializer(source='grocery_list_items', many=True, read_only=True)
    recipes = GroceryListRecipeSerializer(source='grocery_list_recipes', many=True, read_only=True)

    class Meta:
        model = GroceryList
        fields = ('id', 'date_finished', 'items', 'recipes')
