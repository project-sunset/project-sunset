
from backend.permissions import IsOwner, IsSuperuserOrReadOnly
from django.http import JsonResponse
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated

from groceries.models import (GroceryList, GroceryListItem, GroceryListRecipe,
                              Ingredient, Item, Recipe, Unit)
from groceries.serializers import (GroceryListItemSerializer,
                                   GroceryListRecipeSerializer,
                                   GroceryListSerializer, IngredientSerializer,
                                   ItemSerializer, RecipeSerializer,
                                   UnitSerializer)


class UnitViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsSuperuserOrReadOnly)
    serializer_class = UnitSerializer
    filter_fields = ('name',)

    def get_queryset(self):
        name = self.request.query_params.get('itemname', None)
        if name is not None:
            try:
                item = Item.objects.get(user=self.request.user, name=name)
                queryset = Unit.objects.filter(id=item.unit.id)
            except Item.DoesNotExist:
                queryset = Unit.objects.none()
        else:
            queryset = Unit.objects.all()
        return queryset


class IngredientViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = IngredientSerializer

    def get_queryset(self):
        return Ingredient.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RecipeViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = RecipeSerializer

    def get_queryset(self):
        return Recipe.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)


class GroceryListViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = GroceryListSerializer
    filter_fields = ('date_finished',)

    def get_queryset(self):
        return GroceryList.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @action(methods=['get'], detail=True, permission_classes=[IsAuthenticated, IsOwner])
    def get_list(self, request, pk=None):
        grouped_list = {}
        grocery_list = GroceryList.objects.get(pk=pk, user=self.request.user)
        for item in GroceryListItem.objects.filter(grocery_list=grocery_list, user=self.request.user):
            if item.item.name not in grouped_list:
                grouped_list[item.item.name] = {'amount': item.amount, 'unit': item.item.unit.name}
            else:
                grouped_list[item.item.name]['amount'] += item.amount
        for recipe in GroceryListRecipe.objects.filter(grocery_list=grocery_list, user=self.request.user):
            for item in Ingredient.objects.filter(recipe=recipe.recipe, user=self.request.user):
                if item.item.name not in grouped_list:
                    grouped_list[item.item.name] = {'amount': item.amount*recipe.people, 'unit': item.item.unit.name}
                else:
                    grouped_list[item.item.name]['amount'] += item.amount*recipe.people
        result = [{'name': name, 'amount': item['amount'], 'unit': item['unit']} for name, item in grouped_list.items()]
        return JsonResponse({'list': result})


class ItemViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = ItemSerializer
    filter_fields = ('name',)

    def get_queryset(self):
        return Item.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroceryListItemViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = GroceryListItemSerializer

    def get_queryset(self):
        return GroceryListItem.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroceryListRecipeViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = GroceryListRecipeSerializer

    def get_queryset(self):
        return GroceryListRecipe.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
