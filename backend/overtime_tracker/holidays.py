from datetime import date, timedelta

def is_holiday(d):
    easter = _calc_easter(d.year)
    if d.day == 1 and d.month == 1:
        return True  # Nieuwjaar
    elif d == easter:
        return True  # pasen
    elif d == (easter + timedelta(days=1)):
        return True  # 2de paasdag
    elif d.month == 4 and d.day == 26 and d.weekday() == 5:
        return True  # koningsdag (als 27 zondag is)
    elif d.month == 4 and d.day == 27 and d.weekday() != 6:
        return True  # koningsdag
    elif d.month == 5 and d.day == 5 and (d.year % 5 == 0):
        return True  # bevrijdingsdag
    elif d == (easter + timedelta(days=39)):
        return True  # hemelvaart
    elif d == (easter + timedelta(days=49)):
        return True  # pinksteren
    elif d == (easter + timedelta(days=50)):
        return True  # 2de pinksterdag
    elif d.month == 12 and d.day == 25:
        return True  # kerst
    elif d.month == 12 and d.day == 26:
        return True  # 2de kerstdag
    else:
        return False


def _calc_easter(year):
    """
    Returns Easter as a date object.

    Implementation of Butcher's Algorithm from
    https://code.activestate.com/recipes/576517-calculate-easter-western-given-a-year/
    """
    a = year % 19
    b = year // 100
    c = year % 100
    d = (19 * a + b - b // 4 - ((b - (b + 8) // 25 + 1) // 3) + 15) % 30
    e = (32 + 2 * (b % 4) + 2 * (c // 4) - d - (c % 4)) % 7
    f = d + e - 7 * ((a + 11 * d + 22 * e) // 451) + 114
    month = f // 31
    day = f % 31 + 1
    return date(year, month, day)
