from django.urls import include, path
from overtime_tracker.views import (BreakTimeViewSet, DayViewSet,
                                    OvertimeUserViewSet, get_user,
                                    home_page_info, start_timer, stop_timer)
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'days', DayViewSet, basename='days')
router.register(r'breaktimes', BreakTimeViewSet, basename='breaktimes')
router.register(r'overtimeuser', OvertimeUserViewSet, basename='overtimeuser')

urlpatterns = [
    path('api/', include(router.urls)),
    path('api/profile/', get_user, name="get_user"),
    path('api/home/', home_page_info, name='home_page_info'),
    path('api/start-timer/', start_timer, name='start_timer'),
    path('api/stop-timer/', stop_timer, name='stop_timer')
]
