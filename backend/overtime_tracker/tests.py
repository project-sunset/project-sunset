from datetime import date

from django.contrib.auth.models import User
from django.test import Client, TestCase, tag
from overtime_tracker.holidays import _calc_easter, is_holiday
from overtime_tracker.models import BreakTime, Day, OvertimeUser


@tag('overtime')
class ModelTests(TestCase):
    def setUp(self):
        self.u = User.objects.create(username="Tester", password='secret')
        self.ou = OvertimeUser.objects.create(user=self.u)

    def test_break_time(self):
        b = BreakTime.objects.create(user=self.u, date=date(
            2018, 6, 7), break_time=0.6, working_time_until_break=5)
        self.assertTrue(isinstance(b, BreakTime))
        self.assertEqual(b.__str__(), '2018-06-07')

    def test_day(self):
        BreakTime.objects.create(user=self.u, date=date(
            2018, 6, 7), break_time=0.5, working_time_until_break=5)
        d = Day.objects.create(user=self.u, date=date(2018, 6, 11))
        self.assertTrue(isinstance(d, Day))
        self.assertEqual(d.__str__(), '2018-06-11')

    def test_overtime_calculations(self):
        BreakTime.objects.create(user=self.u, date=date(
            2018, 1, 1), break_time=0.5, working_time_until_break=5)
        data = [
            {  # Regular day with break, negative overtime
                "date": date(2018, 6, 11),
                "hours_worked": 8,
                "expected_result": -0.5
            },
            {  # Regular day with break, overtime
                "date": date(2018, 6, 11),
                "hours_worked": 10,
                "expected_result": 1.5
            },
            {  # Regular day without break
                "date": date(2018, 6, 11),
                "hours_worked": 4,
                "expected_result": -4
            },
            {  # Sunday
                "date": date(2018, 6, 10),
                "hours_worked": 1,
                "expected_result": 2,
            },
            {  # Saturday, which is a holiday
                "date": date(2021, 12, 25),
                "hours_worked": 1,
                "expected_result": 2,
            },
            {  # Saturday
                "date": date(2018, 6, 9),
                "hours_worked": 1,
                "expected_result": 1.5,
            }
        ]
        for test in data:
            expected_result = test.pop('expected_result')
            d = Day.objects.create(user=self.u, **test)
            self.assertAlmostEqual(expected_result, d.hours_overtime)
            d.delete()

@tag('overtime')
class ViewTests(TestCase):
    fixtures = ['user_test_data.json', 'overtime_test_data.json']

    def test_home_info(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/overtime/api/home/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(r['data_exists'], True)
        self.assertEqual(r['stuff']['total_worktime'], 17)
        self.assertEqual(r['stuff']['current_overtime'], -0.2)

    def test_home_info_anonymous(self):
        c = Client()
        response = c.get('/overtime/api/home/')
        self.assertEqual(response.status_code, 403)

    def test_get_user(self):
        c = Client()
        response = c.get('/overtime/api/profile/')
        self.assertEqual(response.status_code, 403)
        c.login(username='Barry', password='flash001')
        response = c.get('/overtime/api/profile/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'user': False})
        c.logout()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/overtime/api/profile/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'user': True})

    def test_overtimeuser_get(self):
        c = Client()
        response = c.get('/overtime/api/overtimeuser/')
        self.assertEqual(response.status_code, 403)
        c.login(username='Barry', password='flash001')
        response = c.get('/overtime/api/overtimeuser/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 0)
        c.login(username='Kara', password='supergirl01')
        response = c.get('/overtime/api/overtimeuser/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 1)

    def test_overtimeuser_post(self):
        c = Client()
        c.login(username='Barry', password='flash001')
        response = c.post('/overtime/api/overtimeuser/')
        self.assertEqual(response.status_code, 201)
        response = c.get('/overtime/api/overtimeuser/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 1)

    def test_day_get(self):
        c = Client()
        response = c.get('/overtime/api/days/')
        self.assertEqual(response.status_code, 403)
        c.login(username='Barry', password='flash001')
        response = c.get('/overtime/api/days/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 0)
        c.login(username='Kara', password='supergirl01')
        response = c.get('/overtime/api/days/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 2)

    def test_day_post(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.post('/overtime/api/days/', {'date': date(2018, 7, 11)})
        self.assertEqual(response.status_code, 201)
        response = c.get('/overtime/api/days/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 3)

    def test_breaktime_get(self):
        c = Client()
        response = c.get('/overtime/api/breaktimes/')
        self.assertEqual(response.status_code, 403)
        c.login(username='Barry', password='flash001')
        response = c.get('/overtime/api/breaktimes/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 0)
        c.login(username='Kara', password='supergirl01')
        response = c.get('/overtime/api/breaktimes/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 1)

    def test_breaktime_post(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.post('/overtime/api/breaktimes/', {'date': date(
            2018, 7, 11), 'break_time': 1, 'working_time_until_break': 3})
        self.assertEqual(response.status_code, 201)
        response = c.get('/overtime/api/breaktimes/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 2)


@tag('overtime')
class TestGetData(TestCase):
    fixtures = ['user_test_data.json', 'overtime_test_data.json']

    def test_easter(self):
        easter = _calc_easter(2018)
        self.assertEqual(easter, date(2018, 4, 1))

    def test_is_holiday(self):
        self.assertTrue(is_holiday(date(2018, 1, 1)))
        self.assertTrue(is_holiday(date(2018, 4, 1)))
        self.assertTrue(is_holiday(date(2018, 4, 2)))
        self.assertTrue(is_holiday(date(2014, 4, 26)))
        self.assertTrue(is_holiday(date(2018, 4, 27)))
        self.assertTrue(is_holiday(date(2020, 5, 5)))
        self.assertTrue(is_holiday(date(2018, 5, 10)))
        self.assertTrue(is_holiday(date(2018, 5, 20)))
        self.assertTrue(is_holiday(date(2018, 5, 21)))
        self.assertTrue(is_holiday(date(2018, 12, 25)))
        self.assertTrue(is_holiday(date(2018, 12, 26)))
        self.assertFalse(is_holiday(date(2018, 1, 2)))

    