from django.contrib.auth.models import User
from django.db import models
from overtime_tracker.holidays import is_holiday


class OvertimeUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    timer_start = models.DateTimeField(blank=True, null=True)

class Day(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)
    date = models.DateField()
    hours_worked = models.FloatField(default=0)
    hours_overtime = models.FloatField(blank=True)
    holiday = models.BooleanField(default=False)
    pay_overtime = models.BooleanField(default=False)
    
    class Meta:
        unique_together = (('user', 'date'),)
        ordering = ['-date']

    def save(self, *args, **kwargs):
        self.holiday = is_holiday(self.date)
        c_date = BreakTime.objects.filter(
            date__lte=self.date, user=self.user).aggregate(models.Max('date'))['date__max']
        config = BreakTime.objects.filter(date=c_date, user=self.user)[0]
        self.hours_overtime = self.calculate_overtime(
            config.break_time, config.working_time_until_break)

        self.hours_worked = round(self.hours_worked, 2)
        self.hours_overtime = round(self.hours_overtime, 2)
        super(Day, self).save(*args, **kwargs)

    def calculate_overtime(self, break_time, working_time_until_break):
        # if holiday or weekend, all hours are overtime
        if self.holiday or self.date.weekday() == 6:
            return self.hours_worked * 2
        elif self.date.weekday() == 5:
            return self.hours_worked * 1.5
        else:
            # Normal days are 8 hours + break time, the rest is overtime
            overtime = self.hours_worked - 8
            if self.hours_worked >= working_time_until_break:
                overtime -= break_time
            return overtime

    def __str__(self):
        return str(self.date)


class BreakTime(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)
    date = models.DateField()
    break_time = models.FloatField()
    working_time_until_break = models.FloatField()

    class Meta:
        unique_together = ('user', 'date')

    def __str__(self):
        return str(self.date)
