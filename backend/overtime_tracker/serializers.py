from rest_framework import serializers

from overtime_tracker.models import BreakTime, Day, OvertimeUser


class OvertimeUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = OvertimeUser
        fields = ('id','timer_start')


class DaySerializer(serializers.ModelSerializer):
    class Meta:
        model = Day
        fields = ('id', 'date', 'hours_worked', 'hours_overtime', 'holiday', 'pay_overtime')


class BreakTimeSerializer(serializers.ModelSerializer):
    class Meta:
        model = BreakTime
        fields = ('id', 'date', 'break_time', 'working_time_until_break')
