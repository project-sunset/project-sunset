from django.contrib import admin

from overtime_tracker.models import BreakTime, Day, OvertimeUser


@admin.register(Day)
class DayAdmin(admin.ModelAdmin):
    list_display = ('date', 'user')


@admin.register(BreakTime)
class BreakTimeAdmin(admin.ModelAdmin):
    list_display = ('user', 'date')


@admin.register(OvertimeUser)
class OvertimeUserAdmin(admin.ModelAdmin):
    list_display = ('user',)
