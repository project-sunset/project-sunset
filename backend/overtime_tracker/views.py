from django.contrib.auth.models import AnonymousUser
from django.db import transaction
from django.db.models import Sum
from django.http import HttpResponse, JsonResponse
from django.utils import timezone
from overtime_tracker.models import BreakTime, Day, OvertimeUser
from overtime_tracker.serializers import (BreakTimeSerializer, DaySerializer,
                                          OvertimeUserSerializer)
from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

from backend.permissions import IsOwner


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def home_page_info(request):
    if not isinstance(request.user, AnonymousUser) and Day.objects.filter(user=request.user).exists():
        current_overtime = round(Day.objects.filter(user=request.user, pay_overtime=False).aggregate(
            Sum('hours_overtime'))['hours_overtime__sum'], 2)

        total_worktime = round(Day.objects.filter(user=request.user).aggregate(
            Sum('hours_worked'))['hours_worked__sum'], 2)

        days = Day.objects.filter(user=request.user).order_by('date')
        running_overtime = []
        running_total = 0
        for day in days:
            if not day.pay_overtime:
                running_total = round(running_total + day.hours_overtime, 2)
            running_overtime.append(running_total)
        labels = [i for i in range(0, len(days))][-100:]
        data = running_overtime[-100:]

        return JsonResponse({
            'data_exists': True,
            'stuff': {
                'total_worktime': total_worktime,
                'current_overtime': current_overtime,
                'data': data,
                'labels': labels,
            }
        })
    return JsonResponse({'data_exists': False})


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_user(request):
    try:
        OvertimeUser.objects.get(user=request.user)
        return JsonResponse({'user': True})
    except OvertimeUser.DoesNotExist:
        return JsonResponse({'user': False})


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def start_timer(request):
    user = OvertimeUser.objects.get(user=request.user)
    user.timer_start = timezone.now()
    user.save()
    return HttpResponse(status=204)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@transaction.atomic
def stop_timer(request):
    user = OvertimeUser.objects.get(user=request.user)
    hours_timed = (timezone.now() - user.timer_start).total_seconds() / 3600
    day, _ = Day.objects.get_or_create(user=request.user, date=timezone.now())
    day.hours_worked += hours_timed
    day.save()
    user.timer_start = None
    user.save()
    return HttpResponse(status=204)

class OvertimeUserViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = OvertimeUserSerializer

    def get_queryset(self):
        return OvertimeUser.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class DayViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = DaySerializer

    def get_queryset(self):
        return Day.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BreakTimeViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = BreakTimeSerializer

    def get_queryset(self):
        return BreakTime.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
