# Generated by Django 3.2.8 on 2022-02-12 16:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('overtime_tracker', '0009_auto_20181020_1533'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='vacationhours',
            unique_together=None,
        ),
        migrations.RemoveField(
            model_name='vacationhours',
            name='user',
        ),
        migrations.DeleteModel(
            name='TimerEntry',
        ),
        migrations.DeleteModel(
            name='VacationHours',
        ),
    ]
