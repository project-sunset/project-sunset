from rest_framework import serializers

from quiz.models import Quiz, QuizQuestion, QuizTeam, QuizAnswer, QuizRound


class QuizSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quiz
        fields = ('id', 'name', 'current_round', 'current_question')

class QuizRoundSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuizRound
        fields = ('id', 'quiz', 'name', 'order')

class QuizQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuizQuestion
        fields = ('id', 'question', 'order', 'quiz', 'answer', 'round', 'score_weight')

class QuizTeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuizTeam
        fields = ('id', 'quiz', 'name', 'code')

class QuizAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuizAnswer
        fields = ('id', 'question', 'team', 'answer')

# PARTICIPATE SERIALIZERS

class QuizParticipateQuestion(serializers.ModelSerializer):
    class Meta:
        model = QuizQuestion
        fields = ('id', 'question', 'order', 'round')

class QuizRoundParticipateSerializer(serializers.ModelSerializer):
    questions = QuizParticipateQuestion(many=True)

    class Meta:
        model = QuizRound
        fields = ('id', 'name', 'order', 'questions')

class QuizParticipate(serializers.ModelSerializer):
    rounds = QuizRoundParticipateSerializer(many=True)

    class Meta:
        model = Quiz
        fields = ('id', 'name', 'current_round', 'current_question', 'rounds')

class QuizTeamParticipate(serializers.ModelSerializer):
    quiz = QuizParticipate()
    answers = QuizAnswerSerializer(many=True)
    
    class Meta:
        model = QuizTeam
        fields = ('id', 'quiz', 'name', 'code', 'answers')
