from django.http import JsonResponse
from quiz.models import Quiz, QuizAnswer, QuizQuestion, QuizRound, QuizTeam
from quiz.serializers import (QuizAnswerSerializer, QuizQuestionSerializer,
                              QuizRoundSerializer, QuizSerializer,
                              QuizTeamParticipate, QuizTeamSerializer)
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from backend.permissions import IsOwnerOrReadOnly


class QuizViewSet(viewsets.ModelViewSet):
    serializer_class = QuizSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    queryset = Quiz.objects.all()
    
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @action(methods=['get'], detail=True)
    def score(self, _, pk):
        quiz = Quiz.objects.get(pk=pk)
        teams = QuizTeam.objects.filter(quiz=quiz)
        score = {}
        for team in teams:
            score[team.name] = 0
            answers = QuizAnswer.objects.filter(team=team)
            for answer in answers:
                if answer.answer == answer.question.answer:
                    score[team.name] += answer.question.score_weight
        return JsonResponse(score)

class QuizRoundViewSet(viewsets.ModelViewSet):
    serializer_class = QuizRoundSerializer
    filter_fields = ('quiz','order')
    queryset = QuizRound.objects.all()

class QuizAnswerViewSet(viewsets.ModelViewSet):
    serializer_class = QuizAnswerSerializer
    filter_fields = ('quiz','team')
    queryset = QuizAnswer.objects.all()

class QuizQuestionViewSet(viewsets.ModelViewSet):
    serializer_class = QuizQuestionSerializer
    filter_fields = ('quiz',)
    queryset = QuizQuestion.objects.all()

class QuizTeamViewSet(viewsets.ModelViewSet):
    serializer_class = QuizTeamSerializer
    filter_fields = ('quiz','name')
    queryset = QuizTeam.objects.all()

    @action(methods=['get'], detail=True)
    def ping(self, _, pk):
        quiz = QuizTeam.objects.get(pk=pk)
        serializer = QuizTeamParticipate(quiz)
        return Response(serializer.data)