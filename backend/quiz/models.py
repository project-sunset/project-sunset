from django.db import models
from django.contrib.auth.models import User

class Quiz(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, unique=True)
    current_round = models.IntegerField(default=0)
    current_question = models.IntegerField(default=0)

    def __str__(self):
        return self.name

class QuizRound(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='rounds')
    name = models.CharField(max_length=100)
    order = models.IntegerField()

    def __str__(self):
        return f'{self.quiz} - Round {self.order}'

class QuizQuestion(models.Model):
    """
    The question field should be a JSON string:
    ```json
    {
        "type": "mc",
        "q": "Yes?",
        "choices": ["Yes", "No"]
    }
    ```
    """
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='questions')
    question = models.TextField()
    round = models.ForeignKey(QuizRound, on_delete=models.CASCADE, related_name='questions')
    order = models.IntegerField()
    answer = models.TextField()
    score_weight = models.IntegerField()

    def __str__(self):
        return f'{self.quiz} - {self.id}'

class QuizTeam(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=4)

    class Meta:
        unique_together = (('quiz', 'name'),)

    def __str__(self):
        return f'{self.quiz} - {self.name}'

class QuizAnswer(models.Model):
    question = models.ForeignKey(QuizQuestion, on_delete=models.CASCADE)
    team = models.ForeignKey(QuizTeam, on_delete=models.CASCADE, related_name='answers')
    answer = models.CharField(max_length=100)

    class Meta:
        unique_together = (('team', 'question'),)

    def __str__(self):
        return f'{self.team} - {self.question}'
