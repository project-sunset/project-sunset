from django.contrib import admin

from quiz.models import Quiz, QuizAnswer, QuizQuestion, QuizTeam

admin.site.register(Quiz)
admin.site.register(QuizAnswer)
admin.site.register(QuizQuestion)
admin.site.register(QuizTeam)
