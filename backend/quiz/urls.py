from django.urls import include, path
from rest_framework import routers

from quiz.views import QuizViewSet, QuizTeamViewSet, QuizAnswerViewSet, QuizQuestionViewSet, QuizRoundViewSet

router = routers.DefaultRouter()
router.register('quiz', QuizViewSet, basename='quiz')
router.register('teams', QuizTeamViewSet, basename='quiz-team')
router.register('answers', QuizAnswerViewSet, basename='quiz-answer')
router.register('questions', QuizQuestionViewSet, basename='quiz-question')
router.register('rounds', QuizRoundViewSet, basename='quiz-round')

urlpatterns = [
    path('api/', include(router.urls)),
]

