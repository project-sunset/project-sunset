from django.test import tag
from shared_django.testutils import PlaywrightTests


@tag('playwright')
class FrontendTests(PlaywrightTests):
    fixtures = [
        'users.test.json',
        'overtime_tracker.test.json',
        'groceries.test.json',
        'quiz.test.json',
        'finances.test.json'
    ]
    js_coverage = [
        'various/static/various/memory'
    ]
    config = {
        'username': 'Kara',
        'password': 'supergirl01'
    }

    @tag('various-ui')
    def test_various(self):
        self.run_playwright('various')

    @tag('overtime-tracker-ui')
    def test_overtime_tracker(self):
        self.run_playwright('overtime-tracker')

    @tag('links-ui')
    def test_links(self):
        self.run_playwright('links')

    @tag('account-ui')
    def test_account(self):
        self.run_playwright('account')

    @tag('groceries-ui')
    def test_groceries(self):
        self.run_playwright('groceries')
    
    @tag('quiz-ui')
    def test_quiz(self):
        self.run_playwright('quiz')
    
    @tag('finances-ui')
    def test_finances(self):
        self.run_playwright('finances')
