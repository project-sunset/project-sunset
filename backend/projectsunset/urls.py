from django.urls import path
from django.shortcuts import redirect
from projectsunset.views import (api_root, disclaimer, index, manifest_json,
                                 robots_txt, servertask, service_worker,
                                 support, webmanifest, workbox)

urlpatterns = [
    path('', index, name="home"),
    path('favicon.ico', lambda r: redirect('/static/favicon.ico')),
    path('index.html', index, name="home2"),
    path('api/', api_root, name="api_root"),
    path('disclaimer/', disclaimer, name="disclaimer"),

    path('robots.txt', robots_txt),
    path('service-worker.js', service_worker, name="service_worker"),
    path('manifest.json', manifest_json),
    path('manifest.webmanifest', webmanifest),
    path('workbox-<str:hashed_url>.js', workbox),

    path('support/', support, name="support"),
    path('servertask/<str:action>/', servertask, name="servertask"),
]
