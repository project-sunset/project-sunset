# Generated by Django 3.2.8 on 2022-10-02 18:02

from django.db import migrations


def move_to_new_model(apps, schema_editor):
    ps_users = apps.get_model("projectsunset", "projectsunsetuser")
    google_users = apps.get_model("shared_django", "googleuser")
    for ps_user in ps_users.objects.all():
        google_users.objects.create(user=ps_user.user, google_id=ps_user.google_id)


class Migration(migrations.Migration):

    dependencies = [
        ('projectsunset', '0002_auto_20180922_0049'),
        ('shared_django', '0001_initial')
    ]

    operations = [
        migrations.RunPython(move_to_new_model),
        migrations.DeleteModel(
            name='ProjectSunsetUser',
        ),
    ]
