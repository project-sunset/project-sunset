from django.test import Client, TestCase


class TestViews(TestCase):
    fixtures = ['user_test_data.json']

    def text_index(self):
        c = Client()
        response = c.get("/")
        self.assertEqual(response.status_code, 200)
