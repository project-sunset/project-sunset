import os

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.db import connection
from django.shortcuts import redirect, render
from django.template import TemplateDoesNotExist
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import HttpResponse


@ensure_csrf_cookie
def index(request):
    if hasattr(settings, 'GOOGLE_CLIENT_ID'):
        return render(request, 'index.html', {'google_client_id': settings.GOOGLE_CLIENT_ID})
    else:
        try:
            return render(request, 'index.html', {})
        except TemplateDoesNotExist:
            return HttpResponse('No index.html present')


def robots_txt(request):
    return render(request, 'robots.txt', content_type='text/plain')


def service_worker(request):
    return render(request, 'service-worker.js', content_type="application/javascript")


def manifest_json(request):
    return render(request, 'manifest.json', content_type="application/json")


def webmanifest(request):
    return render(request, 'manifest.webmanifest', content_type="application/manifest+json")


def workbox(request, hashed_url):
    return render(request, f'workbox-{hashed_url}.js', content_type="application/javascript")


def api_root(request):
    context = {
        'apis': [
            {
                'name': 'Overtime',
                'href': '/overtime/api/'
            },
            {
                'name': 'Groceries',
                'href': '/groceries/api/'
            },
            {
                'name': 'Consumer',
                'href': '/consumer/api/'
            },
            {
                'name': "Finances",
                'href': '/finances/api/'
            }
        ]
    }
    return render(request, 'projectsunset/api_root.html', context)


def disclaimer(request):
    return render(request, 'projectsunset/privacy_and_disclaimer.html')


@user_passes_test(lambda u: u.is_superuser)
def support(request):
    users = User.objects.all()
    context = {
        'users': users,
        'info': {
            'DJANGO_SETTINGS_MODULE': os.environ['DJANGO_SETTINGS_MODULE']
        }
    }
    return render(request, 'projectsunset/support.html', context)


@user_passes_test(lambda u: u.is_superuser)
def servertask(request, action):
    if action == 'vacuumdatabase':
        with connection.cursor() as cursor:
            cursor.execute("VACUUM")
        messages.info(request, 'The database has been vacuumed.')
    return redirect("support")
