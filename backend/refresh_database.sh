#!/bin/bash

rm db.sqlite3
python3 manage.py migrate 
python3 manage.py createcachetable
python3 manage.py loaddata \
  users.test.json \
  overtime_tracker.test.json \
  groceries.test.json \
  quiz.test.json \
  finances.test.json

if [ $# -eq 0 ]
  then
    # No args supplied so this is not playwright
    python3 set_desktop_webauthn.py
fi
