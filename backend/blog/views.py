import hashlib
import json
import random
import re
from copy import deepcopy
from os import listdir
from typing import Callable

import markdown
from django.http import Http404, HttpRequest, JsonResponse
from markdown.extensions import codehilite, fenced_code

ARTICLES = {}
BLOG_FEED = []

tag_classes = {
    'one day project': 'is-danger',
    'medium project': 'is-warning',
    'long term project': 'is-success'
}


def minutes_to_read(markdown: str) -> int:
    words_per_minute = 230
    words_including_code = len(markdown.split())
    images = markdown.count('<img')
    image_looking = images * 0.1  # 6 seconds per image
    # Remove code blocks
    text = re.sub(r'```[\s\S]+```\n', '', markdown)
    words_without_code = len(text.split())
    text_reading = words_without_code/words_per_minute
    code_reading = (words_including_code - words_without_code)/(words_per_minute*1.5)  # code has more boilerplate
    return round(text_reading+code_reading+image_looking)


def read_articles():
    files = listdir('blog/articles')
    BLOG_FEED.clear()
    for f in files:
        if not f.endswith('.md'):
            continue
        with open(f'blog/articles/{f}', 'r') as blog:
            file_content = blog.read()
        frontmatter = file_content[:file_content.index('#')]
        content = file_content[file_content.index('#'):]
        data = json.loads(frontmatter)
        data['slug'] = f[:-3]
        data['tags'] = [{'tag': tag, 'class': tag_classes[tag] if tag in tag_classes else ""} for tag in data['tags']]
        data['read_minutes'] = minutes_to_read(content)
        if "rank" not in data:
            data["rank"] = 999
        BLOG_FEED.append(deepcopy(data))
        data['content'] = markdown.markdown(content, extensions=[fenced_code.FencedCodeExtension(), codehilite.CodeHiliteExtension(noclasses=True)])
        ARTICLES[data['slug']] = data
    BLOG_FEED.sort(key=lambda article: article["rank"])


read_articles()


def feed(request: HttpRequest):
    if "localhost" in request.get_host():
        read_articles()
    return JsonResponse(BLOG_FEED, safe=False)


def get_suggestions(current_slug: str, viewed_articles: int):
    articles = [article for article in BLOG_FEED if article['slug'] != current_slug and not in_bloom(viewed_articles, article['slug'])]
    if len(articles) >= 2:
        random.shuffle(articles)
        return articles[:2]
    else:
        picked_slugs = [article['slug'] for article in articles]
        remaining = [article for article in BLOG_FEED if article['slug'] != current_slug and article['slug'] not in picked_slugs]
        random.shuffle(remaining)
        if len(articles) == 1:
            return [articles[0], remaining[0]]
        else:
            return [remaining[0], remaining[1]]


def in_bloom(viewed_articles: int, slug: str):
    sha1_result = run_hash(slug, hashlib.sha1)
    md5_result = run_hash(slug, hashlib.md5)
    result = (sha1_result | md5_result)
    return viewed_articles & result == result


def add_to_bloom(session, slug: str):
    if 'viewed_articles' not in session or isinstance(session['viewed_articles'], list):
        session['viewed_articles'] = 0
    sha1_result = run_hash(slug, hashlib.sha1)
    md5_result = run_hash(slug, hashlib.md5)
    session['viewed_articles'] |= sha1_result | md5_result


def run_hash(slug: str, hash_func: Callable) -> int:
    m = hash_func()
    m.update(slug.encode('utf8'))
    hash_int = int.from_bytes(m.digest(), byteorder='big', signed=False)
    return 2**(hash_int % 200)


def article(request: HttpRequest, slug: str):
    if "localhost" in request.get_host():
        read_articles()
    if slug not in ARTICLES:
        raise Http404("Unknown blog")
    add_to_bloom(request.session, slug)
    return JsonResponse({
        'article': ARTICLES[slug],
        'suggested': get_suggestions(slug, request.session['viewed_articles'])
    })
