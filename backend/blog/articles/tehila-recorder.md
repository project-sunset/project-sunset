{
  "title": "Recording songs with Spring Boot",
  "snippet": "Building a server component to record practice sessions",
  "tags": ["medium project", "Java", "Spring Boot", "JavaScript", "Vue"]
}

# Tehila recorder

Tehila is the webapp I created for the band I play with in my church. Often when practicing we want to have a recording of the song so we can listen back to it at home. To practice and to remember how we wanted to play a song. At first the band did this by dropping a phone somewhere and recording a voice message with WhatsApp. That works really simple, but the audio quality is pretty bad. We can get significantly better quality if we use the mixer that we use to stream our services to YouTube. We just need a simple interface to use that mixer. And I think I can make that.

The first step is to figure out how to record audio in software. Most of my backend code is Python, so I first looked into that. There are several third party libraries that can record audio. Some natively, some depend on other libraries like ffmpeg. But looking through the documentation I didn't really like any of them. Also, I thought this use case would be a good place to use a different language. It will run locally on a computer connected to the mixer at church. So there is no need to use the same language. 

I first looked into Rust and played around with it for a bit, but it was hard to use. Maybe because I was new to Rust, and maybe also because Rust is relatively new and audio stuff hasn't matured enough for that language. But anyway, I went to consider another option. What language would have great compatibility with audio recording and running on Windows desktops? There is an obvious answer! It is C# of course, probably the language with the best integration into everything that is Windows. 

But I didn't think of that one, and instead I thought of Java 🙃

Java has the `javax.sound` package baked in. And because I use Java daily at work, writing a simple recording app proved to be easy. An HTTP endpoint to get all the connected USB sound cards, an endpoint to start a recording on a card, and an endpoint to stop the recording and return the audio file.

The biggest challenge was that the audio recording returned a file in wave format, which is pretty big. So I wanted to turn it into MP3 (earlier I wanted to use Ogg, but Safari on iOS has no support for Ogg, which would be sad for our band members with iPhones). I couldn't find a Java library that does this, or more accurately, not a library that does this without external dependencies. In the end I opted for something that felt a little janky. I added the [LAME](https://lame.sourceforge.io/) executable to the project, and call the LAME CLI in a process in Java.

An advantage of Java is that it comes with `jpackage` a CLI to easily bundle the whole project in a Windows installer. I can configure everything in the POM, so with a `mvn install` command it packages the app, adds the LAME executable, zip's it up and dumps it into my OneDrive. And then in church I can unzip and install a new version in no time.

All that is left to do is add a big red button to Tehila and do some networking shenanigans so the browser can call the endpoints on a computer in the local network, while the rest of the site is hosted elsewhere.

<img src="/static/blog/tehila-recorder/big-button.jpg" alt="A webpage showing a big red record button and an HTML audio element." />

And now the bands have easy access to high quality recordings for practice sessions!