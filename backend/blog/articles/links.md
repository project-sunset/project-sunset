{
  "title": "Too many links for a bookmark toolbar",
  "snippet": "Creating a nice start page to reach all the websites for my work",
  "tags": ["medium project", "web", "JavaScript", "Vue", "Python", "Django"],
  "rank": 5
}

# Links

I started working at a bank in 2017 in a service management team. We were responsible for solving all sorts of incidents in the software stack around mortgages. As you can imagine with any large company that has existed in various ways for well over a century the IT stack is pretty diverse. There are components built in a home-grown framework running on on-premise servers in our data center, which connect to components build in a proprietary third party framework running on VMs in a public cloud, which connect to SaaS applications via a connection that runs through a proxy server somewhere in a private cloud from another vendor. And all of those things exist four times because [DTAP street](https://en.wikipedia.org/wiki/Development,_testing,_acceptance_and_production).

Of course every team involved has their own documentation, maybe some extra website with tooling for debugging, another one for some self-service password stuff, HR, SharePoints, downloads, news. There is an endless list of websites that you may need to go to at one point or another.

So on one of my first days I received an HTML file with a whole bunch of links to all sorts of websites. It was just a plain HTML file with no styling and a whole bunch of `ul>li>a` elements to show all the links.

<img src="/static/blog/links/first_links.jpg" alt="An unstyled webpage showing links in a basic list." />

I used that for a while, but decided to put some styling on it to please my eyes a bit more.

<img src="/static/blog/links/second_links.jpg" alt="A green webpage with the links organized in styled groups." />

One issue with the HTML file is that adding a link is a bit of a hassle. Certainly now that I added a bunch of styling and the HTML has become more complicated. Keeping an eye out for all the nested `div`s to organize and sort everything properly is becoming more complicated with it as plain HTML.

The solution is of course to switch to a proper separation between the data and the presentation! So I put all the links nicely organized and normalized in a database and wrote some Vue to load the data and present it with a nice interface, together with a couple input fields to add more links.

<img src="/static/blog/links/third_links.jpg" alt="A lightblue webpage with the links and a large search input." />

<img src="/static/blog/links/admin_links.jpg" alt="A lightblue webpage with input forms to add new links." />

With this improvement also came an issue. Loading a local, plain HTML file is very quick. But now there is an HTTP server, SQL database and JavaScript framework involved. So now I needed to wait for a spinner to tell me that the data and the application where downloaded and parsed before I could search for a link.

Obviously it is still pretty fast. But waiting for half a second every time when I needed to go somewhere was annoying. Luckily there is a solution to speed all of this up! The solution is found in [PWA](https://en.wikipedia.org/wiki/Progressive_web_app) technologies. Instead of loading the website and data from the server every time I open the website, I can make the browser store all of the HTML, JavaScript, CSS and data locally. So when going to my website the browser will use these cached files to immediately show the website, and in the background it can fetch any updates that have happened since my last visit.

Much better for my productivity.