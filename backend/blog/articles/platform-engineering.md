{
  "title": "Platform engineering at home",
  "snippet": "Creating a generic web platform for my stack and deployment",
  "tags": ["medium project", "cicd", "Docker", "Git"]
}

# Platform engineering at home

I've had my personal website since 2015. In 2019 I built some new functionality to keep track of the repertoires of the bands I play in. And in 2020 I decided to split that functionality off into it's own separate website. That came with a couple of advantages. But one downside is that there was a whole lot of duplication. Both sites use the same basic structure. With a Django backend that mostly uses Django REST framework, and a Vue application that connects to it. Both are first deployed to my acceptance server (AKA old laptop in a cabinet) and after some tests deployed to PythonAnywhere for production use.

How can I [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) this setup?

## DRY'ing the application code

In the backend most of the duplication is found in endpoints for account management. Endpoints for changing your password, username, adding new authorized devices. And also some functionality to use Backblaze B2 for database backups and uploaded media storage.

For the frontend there is more duplication. There are components that use the aforementioned account management endpoints and some related storage. But mostly I have build a library of Vue components that use Bulma for styling. So I have a set of input elements for text, dates, files and whatnot. And those also combine together into components that retrieve data from a Django REST framework endpoint and create forms and update Vue state. You can just place a config into a component and it will generate a complete CRUD interface for the object!

```html
<model-table
  class="box"
  modelName="Days"
  v-model="dayModel"
  :config="dayConfig"
  :data="days"
  apiStore="overtime/days"
  :tableFields="['date', 'hours_overtime']" />
```

<img src="/static/blog/platform-engineering/modeltable.jpg" alt="A webpage showing a simple table to do CRUD operations." />

One way of sharing this code is to create an NPM package for those Bulma components, publish it and then reference it in the `package.json` of both projects. And for the Python code that would go via PyPI. Or I could also use the registries of Gitlab so I don't need to publicly publish the packages. But a major downside of these options is that performing any change in these libraries would be more work, since I would have to make the change and publish the package before I can use it.

The much easier way of sharing this code is to use git submodules. When I need to update a part of the library I can do that locally in the project and immediately see the result. The build system sees the library as just another folder of source code so hot reload also works normally. And publishing a change to use in the other project is as simple as pushing the change to the Gitlab repository of the shared code.

## DRY'ing the CICD pipeline

The first step that I needed to take for the CICD part is creating the needed Docker images. Three images are necessary. An image for testing everything. That image needs Node, Python and E2E tooling: TestCafé and a browser. The second image is an image that can run the website, so we need Python, SQLite, NGINX, uWSGI and some other stuff. And the third image is for deploying to PythonAnywhere. For that we only need rsync, SSH, and cURL. The images are build using Kaniko and are then published into the registry of the Gitlab group.

With the images published and ready to go we can create a pipeline that utilizes them. A simple pipeline is all that is needed. We want to test and package the software, run a SonarQube scan for static code analysis, deploy to an environment and then do a smoke test to see that we didn't break everything.

<img src="/static/blog/platform-engineering/pipeline.jpg" alt="A GitLab pipeline that has passed successfully." />

All of this seems pretty straight forward. But the images and pipeline contain a whole bunch of lines of code that I only figured where needed after breaking my head over why something went wrong. Just to take an example: after some work on the Vue setup Istanbul, or some other dependency that generated the test coverage report, starting reporting the paths incorrectly. Part of the path would be double, for example: `src/components/src/components/Something.vue` which caused SonarQube to report a 0% coverage. I couldn't figure out why this happened, but as it was consistent I figured that I could use `sed` to fix the paths. So I wrote a query that I tested on my Ubuntu WSL install: `sed -i -r "s@:src/(.+)/src/\1@:src/\1@g" frontend/coverage/lcov.info`. But when I ran this in the pipeline it wouldn't do anything. After more Googling I found out that the image I was using was based on Alpine, which uses Busybox. And for some reason their build of `sed` didn't like this query. The easy solution is to install `sed` via `apk` because that would retrieve the GNU build which would execute this query correctly.

Having these kind of fixes in one place prevents me from bumping into them in both projects.

## Platform engineering

I sometimes tell myself that I don't really like configuring servers or writing all the necessary boilerplate to deploy some software, and that I just want to write the application code. But then I look at all the shared infrastructure, automated deploys and testing that I've build, and I think that maybe I do also really like creating a nice platform to engineer on top of, even though it sometimes breaks my brain to get everything to work together nicely.