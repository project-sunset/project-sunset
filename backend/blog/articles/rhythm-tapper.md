{
  "title": "Tap tap tapping",
  "snippet": "Creating the missing link for writing sheet music",
  "tags": ["one day project", "web", "HTML canvas"]
}

# Rhythm Tapper

As a musician I sometimes transcribe a song into sheet music. With a guitar or piano I can usually figure out the melody pretty quickly. But I often struggle to recognize how long each note is. it is quite frustrating to be able to play a melody, but not be able to write down what I played. The pitch is easy to see when I play a melody, but I can't see the duration.

Unless, I create a webpage for that of course! Set a metronome and then just tap the rhythm and it will be transcribed into 16th notes for you! You can find it [here](#/various/rhythm-tapper/).

<img src="/static/blog/rhythm-tapper/rhythm_tapper.jpg" alt="A webpage showing a metronome and a line of musical notes." />

The logic of the app is pretty straightforward. I think I spend the most time on the notes. I used an HTML canvas and the notes are composed of a couple elements. A line is easy to create, an ellipse for the note head is not too hard. What took a long time is getting the [Bézier curves](https://en.wikipedia.org/wiki/B%C3%A9zier_curve) of the flags on the stem just right.

I am still not entirely happy about the tips of the flag. It looks a little bit too much like there is a little dot instead of a smooth curve 🤔

<img src="/static/blog/rhythm-tapper/flag_tip.jpg" alt="A close up of the flags of a sixteenth note, showing small irregularity." />

But luckily I am more of a pragmatist than a perfectionist. So I apologize if it bothers you, but I am moving on to the next side project! 😁