{
  "title": "Automating my finances",
  "snippet": "A journey from Excel, to VBA, to Django",
  "tags": ["long term project", "web", "automation", "JavaScript", "Vue", "Python", "Django"]
}

# Finances

When I started studying I figured that I needed to keep better track of my money. So I decided I should make a budget, to plan how much I can spend on each category. And I quickly realized that was a futile idea, Because I had no idea what my current spending was. So I opened Excel and started tracking everything. Every month I would go through all transactions of the past month, categorize them and sum them up. And put them in a table:

<img src="/static/blog/finances/2011.jpg" alt="A basic Excel spreadsheet showing income and expenses." />

After a while this started to look a bit bland, so I added a splash of color:

<img src="/static/blog/finances/2013.jpg" alt="A colored Excel spreadsheet showing income, expenses, and error amount" />

The first couple of years I would manually go through all my transactions and add them up one by one to create this table. But then I went to work at Calco, a company that helps young professionals with no study background in IT to get started. Somewhere during my time there they found a company that needed someone with VBA experience. My experience was literally none, so I quickly got started to learn it! And for me the best way to learn a new language or framework, is to just build something with it. And my manual finance administration seemed a perfect target. It was already in Excel, and with VBA I should be able to make something that processes transactions automatically!

So I created some buttons and a bunch of VBA code that would process transactions automatically based on the configured search terms.

<img src="/static/blog/finances/vba_1.jpg" alt="Buttons on an Excel spreadsheet to add transactions, enter balance and check for errors, and a bunch of VBA code." />

<img src="/static/blog/finances/vba_2.jpg" alt="A spreadsheet showing the raw data stored in the Excel" />

A major advantage of this was that I could create pivot tables to view my transactions in any way I liked. Instead of only in the one table that the data was in prior to this update.

<img src="/static/blog/finances/vba_3.jpg" alt="A pivot table showing income and expenses" />

But, the major downside is that the vacancy for a VBA developer fell through. So after building this spreadsheet I barely touched the code anymore and, as is the saying, after a while only God knew how the code worked. Which is an issue because inevitably I would want to update something or other. The code sort of emulated a relational database between Categories, Search terms and Transactions in a single function, which is not great for understanding how it worked. Luckily during this time I had also started building what would become Project Sunset, which was a nice Django website with a proper SQL database. So while riding on the burning mess that is my VBA code I quickly build new, way better software on my website.

On the website I can configure the categories:

<img src="/static/blog/finances/website_2.jpg" alt="A webpage showing a UI for adding and removing categories of financial transactions." />

Then configure the search terms to match incoming transactions with those categories:

<img src="/static/blog/finances/website_3.jpg" alt="A webpage showing search terms for Albert Heijn, ING and UNICEF and which category they go in to." />

And then I would like to use an API of my bank to get the transactions. So I looked into how to do that, technically it should be possible because with PSD2 banks have to have an API. Unfortunately I found out that to get a license to use PSD2 from the Dutch authority I would basically have to become a banking company with all the necessary checks and balances. Which felt like a bit of a large hurdle to take. So I'll just keep uploading CSV files for now.

<img src="/static/blog/finances/website_4.jpg" alt="A webpage showing that 71% of the transactions where automatically processed and a table of the remaining transactions." />

And with all that done I can view all my data in different tables and graphs!

<img src="/static/blog/finances/website_5.jpg" alt="A webpage showing income and expenses" />

<img src="/static/blog/finances/website_1.jpg" alt="A webpage showing a short income statement and a graph of the spending account balance." />

I am very happy with this webapp. I do always see many places where I can improve the layout, or add more functionality. But unfortunately there is only limited time in a day and I got more stuff to do and places to be!