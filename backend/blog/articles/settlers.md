{
  "title": "Where should you settle?",
  "snippet": "Finding the best place to put your city in Settlers of Catan",
  "tags": ["one day project", "web", "JavaScript", "Vue", "Game", "Puzzle"]
}

# Settlers of Catan

In the Settlers of Catan board game you place villages and cities on a board. A settlement is always at a crossroads of three tiles and will receive the resources that those tiles produce. Every tile has a number and when that number is thrown with the dice the settlement produces its resources. So the question arises what the best location is to put your settlement. And at some point there will be a day where I am bored, and I will create a webpage that will do that for you.

That day has come.

[Tada!](#/various/settlers/)

<img src="/static/blog/settlers/catan.jpg" alt="A webpage with a Catan board and tables of how many resources you get from each position, with one position highlighted." />

On the page you can setup the board and hit "process" and the server will figure out which location gives the most resources. So you can maximize your chances of beating your opponents! Though I do seem to recall games were I clearly had the best locations, but the dice wouldn't cooperate with my theoretical dominance. So your mileage may vary 🤔