{
  "title": "Back to (web platform) basics",
  "snippet": "Creating a countdown clock",
  "tags": ["one day project", "web", "Plain HTML"]
}

# Countdown timer

Every now and then I see something and I can't stop thinking about how to create it.

We have a sprint review every two weeks at my job. Where a dozen or so teams will present some of their work. As you can imagine in this context a dozen is quite a lot. So the scrum masters use a countdown timer and everyone would just start clapping when it finished so the speaker knows that it is time to stop. They use a video on YouTube as a timer (even though the network connection would regularly mess that up). To be specific it is [this video](https://www.youtube.com/watch?v=x6ggW8ei0yU).

<img src="/static/blog/timer/timer_video.jpg" alt="A screenshot of a YouTube video showing a countdown clock, with a black background that turns white while the numbers go from black to white." />

It's a pretty simple video. Large numbers in the middle, angled lines in a circle and a white background that sweeps across while turning the numbers black. So for months I have looked at this video, fascinated by that sweeping effect.

And at some point I gave in and opened an editor to try to create it. Aside from just a timer I wanted to have an input for the time, some buttons to start and stop it, and a couple sound effects you can choose from that would sound at the end. 

The frontend of my website is all build in Vue. But sometimes I like to go back to basics and just use plain HTML with no dependencies. It helps to refresh what is part of a framework and what is part of the web platform. And sometimes it is also just the right choice to skip out on all dependencies since there is nothing to update if you are only relying on the most basic and matured functionalities of the platform.

I won't spoil how I created the effect, I think this is a nice project to do as puzzle. So I challenge you to open a simple editor and dump in some HTML and JavaScript to get this effect 😁 Here is a GIF of [my page](/various/timer/):

<img src="/static/blog/timer/my_timer.gif" alt="A GIF of the countdown timer" />