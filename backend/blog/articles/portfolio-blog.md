{
  "title": "Creating a simple blog",
  "snippet": "Specifically, calculating reading time",
  "tags": ["medium project", "web", "JavaScript", "Vue", "Python", "Django"]
}

# Creating a simple blog

There are many places one can write blogs. Medium.com, Tumblr, WordPress. But as a software developer, I like to create my own stuff. Not because it will be better, but because it is fun. So I got the idea to write my portfolio as a blog.

Obviously the only correct format for the blog is Markdown (and a bit of custom JSON frontmatter for metadata). There are plenty of libraries to parse that to HTML, and also to add some code highlighting.

Many websites with articles also post how many minutes it takes to read an article. I find that very useful to have an idea what I am getting myself into. I found a [blog from Medium](https://blog.medium.com/read-time-and-you-bc2048ab620c) where they describe how they calculate it (from 2014, so kinda old but probably a good starting point).

My first attempt was very simple. Just split the article on all white spaces. Which should then be the amount of words and then we divide it by a words per minute amount, for example 200.

```python
def minutes_to_read(markdown: str) -> int:
    words = len(markdown.split())
    text_reading = words/200
    return round(text_reading)
```

In the blog of Medium they separately consider images. I think that makes sense. So let's add that in the function.

```python
def minutes_to_read(markdown: str) -> int:
    words = len(markdown.split())
    text_reading = words/200
    images = markdown.count('<img')
    image_looking = images * 0.1  # 6 seconds per image
    return round(text_reading+image_looking)
```

This is getting somewhere. But one article I wrote seemed to have a very large number that didn't make sense. I didn't remember the blog being that long. When I opened the blog I noticed the problem. It had a whole bunch of code. And code should probably be counted different from prose.

If you read code to understand what it does you probably read it slower than the rest of the blog. I think information is more densely packed in code than in the rest of an article. But at the same time code can contain more boilerplate. If you consider something like this nested for loop in C:

```c
for (int y = 0; y < max_y; y++)
{
  for (int x = 0; x < max_x; x++)
  {
    if (diagram[x][y] > 1)
    {
      count++;
    }
  }
}
```

Every brace is now counted as a word, and the `int y = 0;` is counted as four words. I suspect that many people familiar with code will skip over all that syntax. And people not familiar with code might not read it at all. So I think it is fair to assume that the code will be read faster than the rest.

So let's expand the function to do that.

```python
def minutes_to_read(markdown: str) -> int:
    words_per_minute = 230
    words_including_code = len(markdown.split())
    images = markdown.count('<img')
    image_looking = images * 0.1  # 6 seconds per image
    # Remove code blocks
    text = re.sub(r'```[\s\S]+```\n', '', markdown)
    words_without_code = len(text.split())
    text_reading = words_without_code/words_per_minute
    code_reading = (words_including_code - words_without_code)/(words_per_minute*1.5)  # code has more boilerplate
    return round(text_reading+code_reading+image_looking)
```

And voila, a nice function to calculate a reading time for all the articles on my blog.