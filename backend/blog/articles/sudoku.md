{
  "title": "Solving all sudoku's",
  "snippet": "I don't solve a puzzle, I solve all puzzles.",
  "tags": ["one day project", "Python", "Game"],
  "rank": 3
}

# Solving all sudoku's

Once upon a time, I was on vacation with my family in the Belgium Ardennes. It was a calm evening, we were reading or puzzling. So I decided I would help them, by creating some software that can solve all sudoku's! I had just done a course on algorithms on edX, and solving sudoku's seemed like fun project. So I whipped out my tablet with bluetooth keyboard.

A very easy sudoku is one where you just need to check the rows, columns or subsquares and find a cell where only one number remains possible. It is easy to write a function that checks for a cell if that is the case. We first create a class to hold the sudoku, where the sudoku is simply an array of arrays.

```python
class SudokuSolver:
    def __init__(self, sudoku):
        self.sudoku = sudoku

sudoku_start = [
    [None, 6   , None,    None, 4   , 2   ,    8   , None, 1   ],
    [1   , None, None,    None, None, None,    5   , 4   , None],
    [None, None, None,    1   , 5   , 8   ,    6   , 2   , 9   ],
    
    [None, None, 7   ,    2   , None, None,    9   , 5   , None],
    [3   , None, None,    None, 9   , 7   ,    1   , None, 4   ],
    [5   , None, 8   ,    None, None, None,    None, 6   , None],
    
    [2   , None, 9   ,    None, None, None,    None, None, 8   ],
    [6   , None, 4   ,    8   , None, 3   ,    2   , None, 5   ],
    [None, None, None,    9   , 2   , None,    4   , None, 6   ],
]

solver = SudokuSolver(sudoku_start)
```

And then we write a function to get which numbers are still possible for a specific cell:

```python
def _get_options(self, row, column):
    # All numbers
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    # If this cell already has a number filled in, return that
    if self.sudoku[row][column] is not None:
        return [self.sudoku[row][column],]
    # For every number
    for i in range(0, 9):
        # If the number is in any column of the row, remove it
        if self.sudoku[row][i] in numbers:
            numbers.remove(self.sudoku[row][i])
        # If the number is in any row of column, remove it
        if self.sudoku[i][column] in numbers:
            numbers.remove(self.sudoku[i][column])
    # Find the numbers in the subsquare of the cell
    other_numbers = self._get_sub_square_numbers(row, column)
    for number in other_numbers:
        if number in numbers:
            # And remove it
            numbers.remove(number)
    # And then return all the remaining numbers, these are the options left for this square
    return numbers
```

The `_get_sub_square_numbers` function is easy to understand what it should return, but there are many ways to find them. I took this approach:

```python
def _get_sub_square_numbers(self, row, column):
    # find the row number of the top left cell
    first_row = row//3*3
    # find the column number of the top left cell
    first_column = column//3*3
    numbers = []
    # And then we can easily loop over the 3x3 square to get the numbers in it
    for i in range(0, 3):
        for j in range(0, 3):
            if self.sudoku[first_row+i][first_column+j] is not None:
                numbers.append(self.sudoku[first_row+i][first_column+j])
    return numbers
```

Now we can get which numbers are remaining for a specific cell. So what we can do is write a function that loops over the entire sudoku and for each cell checks if there is only one option remaining:

```python
def _fill_single_option_cells(self):
    # Loop over the sudoku
    for i in range(0, 9):
        for j in range(0, 9):
            # If the cell is not filled in
            if self.sudoku[i][j] is None:
                # find the numbers that are options
                options = self._get_options(i, j)
                # If there are no options, the sudoku is invalid, we return -1 to signify that
                if len(options) == 0:
                    return -1
                # If there is only one option, we can fill it in
                if len(options) == 1:
                    self.sudoku[i][j] = options[0]
```

But, if we fill in anything that means that now maybe a different, earlier cell in the loop, only has one option left. So we put this whole thing in a while loop to keep looping until all remaining blank cells have more than one option.

```python
def _fill_single_option_cells(self):
    changed = True
    updates = 0
    # While we have changed anything in the loop iteration
    while changed:
        # Start with "we haven't changed anything"
        changed = False
        for i in range(0, 9):
            for j in range(0, 9):
                if self.sudoku[i][j] is None:
                    options = self._get_options(i, j)
                    if len(options) == 0:
                        return -1
                    if len(options) == 1:
                        self.sudoku[i][j] = options[0]
                        # If we fill a cell, set this to true
                        changed = True
                        updates += 1
    # And finally return how many cells we have filled in
    return updates
```

This can be enough to solve the really easy sudoku's. But often a puzzle will have some point where you need a more complicated rule to figure out which number is valid. But I am lazy, and sudoku's are very small. So we can use one of the algorithms I learned to solve these. In this case a depth-first search makes sense. But to do that we first need a function to see if we have actually solved a sudoku:

```python
def _check(self):
    for i in range(0, 9):
        # For every row, get all unique numbers in the row
        row = set(self.sudoku[i])
        try:
            # Try to remove blank cells
            row.remove(None)
        except KeyError:
            # This means there are no blank cells, so that is good
            pass
        # Then we check if there are nine numbers in the set, if not there are duplicates
        if len(row) < 9:
            return False
        # Then do the same thing, but with every column
        column = set([self.sudoku[j][i] for j in range(0, 9)])
        try:
            column.remove(None)
        except KeyError:
            pass
        if len(column) < 9:
            return False
        # And finally check the subsquares in the same way
        square = set(self._get_sub_square_numbers(i%3*3, i//3*3))
        if len(square) < 9:
            return False
        return True
```

And now we are ready to get our depth-first search algorithm in place:

```python
class SudokuSolver:
    def __init__(self, sudoku):
        self.sudoku = sudoku
        # Add a stack storage to the class
        self.stack = []

    def solve(self):
        # while the sudoku is not solved
        while self._check() == False:
            # first fill in all the cells that only have a single option
            updates = self._fill_single_option_cells()
            # if we haven't updated anything, all remaining blank cells have multiple options
            if updates == 0:
                # So we find the one with the fewest options
                branch_cell, options = self._get_cell_with_fewest_options()
                # And for every option in of that cell...
                for option in options:
                    # Create a copy of the sudoku
                    sudoku = copy.deepcopy(self.sudoku)
                    # Fill in one of the options
                    sudoku[branch_cell[0]][branch_cell[1]] = option
                    # And put it on the stack
                    self.stack.append(sudoku)
                # Then we replace the current sudoku with the one on the top of the stack
                self.sudoku = self.stack.pop()
            # If the `_fill_single_options_cells` returns -1, that means the sudoku is invalid
            elif updates == -1:
                try:
                    # So we replace the current sudoku with the one on top of the stack
                    self.sudoku = self.stack.pop()
                except IndexError:
                    # Except if there is nothing on the stack, than the input apparently was already invalid
                    return None
        # And here we return the solved sudoku
        return self.sudoku
```

And that is it! This `solve` function is able to solve any sudoku. Let me explain the depth-first search a little more. If we bump into the situation were there are no single option cells left, then we find the cell with the fewest options remaining. For example, we find a cell where only the numbers 2 or 4 can be filled in. The function than creates one copy of the sudoku where 2 is used, and one copy where 4 is used. Both of those copies are put on the stack, and we grab one of them to continue with. And then for every next time we bump into a situation where we need to make copies we keep putting all of these copies on the stack. So eventually the stack will be filled with a bunch of copies of the puzzle where we have guessed a different number for those cells. And eventually one of those copies should be the correct one where we find a valid solution.

Since sudoku's are pretty small, only 81 squares, this solution works pretty fast. But for some reason my family still wanted to solve sudoku's by themselves instead of using my solver 🤔 But if you want to use it, you can find it [here on my website](#/various/sudoku/).

And a new challenge has already appeared, when I showed it later to my grandpa he didn't like it. He didn't want the full solution, he just wanted to know for his puzzle what the next step is. So maybe I should implement all the more complicated rules to give an actual reasoned solution instead of just brute forcing it 😁 And while I am at it, also make something to parse a photo of a sudoku instead of having people type everything, and maybe make it more generic to support the more crazy sudoku styles, that shouldn't be too hard, and then I could also...

Oh, maybe I don't have enough time for all of this.

Edit: I found time a couple years later! So now you can also get just a hint instead of the whole solution. That code is significantly more complex, but also fun to make!