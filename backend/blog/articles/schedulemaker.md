{
  "title": "Schedule Maker",
  "snippet": "Having waaaay to complicated ideas",
  "tags": ["medium project", "Python", "TKinter"]
}

# Schedule Maker

When I started programming I was working in a Christian hostel in Amsterdam. One of the managers makes a schedule for two weeks for all the staff. If the most experienced manager is on the job it worked great, but sometimes she went on holiday and someone else had to make schedules. And as it turned out that is pretty hard to do properly. People have preferences, but want variety. And not everyone is trained on each position and some combinations are not allowed.

Creating something that generates a schedule seemed like a good project to try to build!

All of the shifts have two letter acronyms, for example there is MR for Morning Reception, AR for Afternoon Café and CS for Cleaning Supervisor.

I created this GUI for the schedule:

<img src="/static/blog/schedule-maker/schedule-maker.jpg" alt="A GUI showing a grid of staff members and days with a shift acronym in each square. One square is green and a couple are red." />

In the GUI you can see the staff members in the top grid, and in the bottom grid you can see which shifts still need to be assigned.

In the top grid some squares are red. This means there is an issue. In the picture there is a combination red because Michael has an ER shift followed the next day by an MR shift. It is not allowed to have an Evening shift followed by a Morning shift. You'll get too little sleep. The other red combination is because only two MC's are needed, three is too many.

You can also see a green square, this is the cursor. I often like to use hotkeys, so I created a way to quickly use the keyboard to create the schedule. With the WASD keys you move around the green cursor, and with the numpad you can fill in the shifts according to the grid on the right side.

If you fill someone in for a shift they are not trained for, it appears with a "#" appended, this means this person needs to be trained. You then need to assign someone else the same shift and if they are trained for that position they will get "tr" appended to the shift to signify they are a trainer for that shift.

Creating this, as a beginner programmer, was quite the journey. But with a lot of trial and error and a couple StackOverflow questions I got it to work very smoothly 👌

The next part I wanted to tackle was to create some code that automatically generates a schedule. I tried a couple things, read some blogs, then read some more difficult to read blogs, then starting reading PhD papers, and then I concluded that maybe this task was a little too complicated for me 😅

This program never got any real use, but I did learn a lot about architecting a program. This software has a bunch of moving parts and getting them all to work well with each other without crashing required learning some lessons in how to architect different classes together into a nice program.