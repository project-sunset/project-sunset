{
  "title": "Building a WYSIWYG chord sheet editor",
  "snippet": "The most complicated frontend I've built.",
  "tags": ["medium project", "web", "JavaScript", "Vue"]
}

# Tehila Song Editor

Tehila is the webapp I created for the band I play with in my church. One part of the webapp shows the lyrics of a song, and I wanted to expand on that and also show the chords and musical directions in the webapp. I've spent many hours building this functionality over the past half year. But it is nearing completion finally!

To immediately give you a short demo of the basic functionality, see this gif!

<img src="/static/blog/tehila-song-editor/demo.gif" alt="A GIF showing a user creating a verse and adding chords to it." />

Most of the software I've build so far are basically just glorified [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete) apps. They store a simple object and you can read and edit those. Whether it is [recipes](#/blog/groceries/), [financial transactions](#/blog/finances/) or [hyperlinks](#/blog/links/). But full editor to create a song, write several verses, and chords, directions, tempo, dynamics and other things would be lot more complicated. Writing about all the challenges I faced while writing this would be a bit much. So I'm going to highlight a couple.

## Input elements

The first challenge I came across was choosing how the input would work. If it was just the lyrics a `textarea` input would work. But for this editor I need to have the chords above the lines. Technically you could do that in a `textarea` and just tell the user to do it correctly. But to show the finished product I would want to do some more styling. And then it isn't a [WYSIWYG](https://en.wikipedia.org/wiki/WYSIWYG) editor, making life harder for the users.

One option to make it WYSIWYG is to not use HTML input elements but instead handle all of the input myself. I would keep track of where the caret is where the user is typing and based on that I can make a complicated function that deals with all the keys and key combinations that can be pressed at any time. This would make sure that the editor would be WYSIWYG because the final product would be the exact same HTML, it just wouldn't listen to key events. A downside is that there are maybe many bugs and edge cases that I would need to take care of, and also all sorts of features that wouldn't work if I didn't build them. For example, did you know that if you press `control+backspace` most editors will delete a whole word? A whole bunch of those small, but nice, features I would have to create again if I didn't use a native HTML input.

Another interesting tidbit I looked into while researching the options: you want to show a caret, that is the blinking cursor you see while typing. But how would the application know where the caret should be? I figured this would be a solved problem, so I decided to look in the codebase of VS Code to see how they decide where the caret is. In VS Code it is really simple if you use a monospaced font, since every symbol takes up the exact same width it is a simple `number of characters * font size`. But you can also select a non-monospaced font. If you select a non-monospaced font VS Code will create a cache for how wide each character is. They determine this by creating a `span` tag somewhere off screen and filling it up with 256 of a character, and then reading the length that it ended up being. Divide that length by 256 and you have almost the correct value. This did lead to some bugs where if you have a line of 10s of thousands of characters the caret would drift off and not be in the correct place. Very interesting, but also not something I would want to deal with in my side project.

So in the end I decided to use normal HTML input elements, and style them completely blank so a user can't see where they are. Then the editor can listen to arrow key events so I can determine if the cursor should switch to the next input element. Which has lead something like this:

```js
// If the user presses arrow right, and the caret is at the end of the input element
if (e.key === 'ArrowRight' && e.target.value.length === e.target.selectionStart) {
  e.preventDefault()
  // Find the data of the current verse
  const verse = editing.findVerse(id)
  // if the current input element is not the last in the sentence
  if (id.element < verse[id.line].length - 1) {
    // create id of next input element
    const base = `${id.section}-${id.line}-${id.element + 1}`
    // If we are in a chord sentence
    if (id.isChord) {
      // Focus on that element starting at position 0
      this.focus(`${base}-chord`, 0)
    // Or if we are in a lyric sentce
    } else {
      // Focus on that element starting at position 0
      this.focus(base, 0)
    }
  // We are at the last input of the sentence, if we are not at the last sentence
  } else if (id.line < verse.length - 1) {
    // Same shizzle as above, but then with the ID of the next sentence
    const base = `${id.section}-${id.line + 1}-0`
    if (id.isChord) {
      this.focus(`${base}-chord`, 0)
    } else {
      this.focus(base, 0)
    }
  } else {
    // end of section, move to next section maybe?
  }
}
```

## DOM update timing

The editor has two different lines to edit, a lyric line, and a chord line. In the (very abbreviated HTML) it looks like this:

```html
<span class="chord-lyrics">
  <input v-model="chord" />
  <input v-model="lyrics" />
</span>
```

The chord is always justified to the left side of the span. So when you add a chord in the middle of a sentence, that sentence will be split into two `chord-lyrics` elements. Which is easy to create: when you press the button to add a chord the application can read the position of the caret and split the sentence accordingly.

But, it becomes a lot more complex. One function of the editor is that when you are editing a chord and you press `space` the caret jumps to the beginning of the next word. Because often a chord will start on a word, so this makes it easier to quickly add the chords to a song. This does mean that with the HTML setup as it is, it would create a new set of input elements each time you press `space`, that is no good. So we want to automatically merge a `chord-lyrics` component if there is no chord. Which is also not hard to do, if the application sees that you are moving out of a chord input element with your caret it can check if there is a chord, and if not it should merge that set with the inputs that come before.

But now, first the logic for the `space` event runs. Which will create a new input element at the next word. We can't immediately focus on that input, because technically we adjust only the data. After that Vue still needs to run to do the actual update. So before we focus we wait for `Vue.nextTick`, then we know that Vue has adjusted the HTML according to the data, and we can safely focus on the new input element. Except that that input element doesn't exist. Because during `Vue.nextTick` the `blur` event also happens that merges the chord we came from with the one before.

To solve this issue we need a two delays in our `space` processing function.

```js
// First actively trigger the blur event
document.activeElement.blur()
// Then do a setTimeout, this creates a new task on the queue
// which should run after the HTML events that we triggered before
setTimeout(() => {
  // Now we know that, if needed, the sentence is updated to reflect a merged chord-lyrics, 
  // We can now update the data for what we need
  const chordId = this.addChord(atTheCorrectPlace)
  // And then wait again before focussing, so Vue can actually create the input element we need
  Vue.nextTick(() => {
    document.getElementById(chordId).focus()
  })
// We can set the delay to 0, because we don't actually want to wait for some time,
// we just want to run this function after the blur event was processed
}, 0)
```

## Inaccurate clicking

This is a WYSIWYG editor, so if you haven't typed anything into an input element. The input element will have a size of near 0. Which is an issue if you want to click on it. And even if the element is larger, there is no visible border. So clicking in it is kinda hard. For that reason there is an `onclick` handler on the final `div` of the editor, that will try to understand what you wanted to click:

```js
focusClosest(event) {
  // If we are not editing, we don't want to focus on anything
  if (!this.editing) return
  // If the click was on a line element, that must mean that you clicked
  // at the end of the lyrics, since otherwise you would hit an input element.
  // So we just focus on the last input element of that line
  if (event.target.classList.contains("line")) {
    const result = event.target.querySelectorAll("input.text")
    result.item(result.length - 1).focus()
    event.preventDefault()
  // If you hit a chord-lyrics element, then you probably clicked above the
  // lyrics, but to the right of the chord, so we focus on the chord input
  // that belongs to those lyrics.
  } else if (event.target.classList.contains("chord-lyrics")) {
    event.target.querySelector("input.chord").focus()
    event.preventDefault()
  // If you hit the directions element, you probably missed the textarea
  // input that is in there. So we'll focus on that.
  } else if (event.target.classList.contains("directions")) {
    event.preventDefault()
    const textarea = event.target.querySelector("textarea.directions-textarea")
    if (textarea === null) {
      // no section name added, thus no directions to focus on
      return
    }
    textarea.focus()
  } else {
    // Is a cat messing with your mouse? No idea what you mean.
  }
}
```

## Not being able to drag verses

In the editor you can drag verses around to reorder them. Dragging stuff around in HTML is quite easy with the [drag and drop API](https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API). But it took me a while to figure out why it was hard in my case. In the editor it looks like this:

<img src="/static/blog/tehila-song-editor/drag-drop.gif" alt="A GIF showing a verse being dragged to the bottom and a drop zone appearing" />

When you start dragging, the browser fires a `dragstart` event. Based on that you can add the data and image that will be dragged around. But when I did that the browser also fired a `dragend` event sometimes, which means that the drag has been cancelled. Not always, but sometimes. After a lot of googling around and trying different things, I found out that issue was that I create the "insert here" drop targets. For some reason when you start dragging, and then edit the HTML during the `dragstart` event, and that causes the mouse pointer to move out of the HTML element that started the drag, then `dragend` would fire.

The solution is very simple: put the code that edits the HTML in a `setTimeout` so that the `dragstart` event can be fully completed before the HTML is changed.

## Lot's of work

Creating this editor lead me down several rabbit holes about HTML, the JavaScript event loop and editing UX. Which was a lot of work, but also very rewarding to see so much code and ideas come together in one editing experience! It is not quite done at the moment so I don't have a link for you to try it out, but hopefully I can make that soon!