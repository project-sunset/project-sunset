{
  "title": "Solving a programming puzzle in C",
  "snippet": "Advent of Code 2021, but I have too much free time.",
  "tags": ["one day project", "C", "Puzzle"]
}

# Advent of Code 2021

Every year Eric Wastl creates an advent calendar of programming puzzles, the [Advent of Code](https://adventofcode.com/). Over 200.000 people completed the first day in 2021. Though that number significantly decreases in later days since the puzzles get harder and harder. 2021 is the first year I participated, and I thought it would be a fantastic idea to learn something new while solving the puzzles. 

I started programming with Python years ago, and later also learned quite a bit of JavaScript and Java. A language that I had often heard about, but never used, is C. C is of course very widely used, but not in the webapp or enterprise/business integration software landscape. It is more low level, closer to the hardware than the other languages that I know. And I figured that using C to solve some of these puzzles would be a fun challenge! Especially if I also limit myself to only use the standard library and not use any dependencies.

The first few days of the Advent of Code were work days so I didn't have time to also learn a new language, but on the first Saturday it was time to dive into it! If you want to know the puzzle you can find it [here](https://adventofcode.com/2021/day/5).

## What is this random number in my C struct?

Coming from a mostly object oriented background, I found structs at first to be quite confusing. I wanted to first declare a variable, and then initialize the struct into it. But that isn't how things work here. Instead you just declare it:

```c
struct Coordinate
{
  int x;
  int y;
};

struct Coordinate start;
```

And now it exists. And not only does it exist, but `x` and `y` already have a value! And the value is not `null` or `0` as I expected. Instead they just have some random number in them. Where did that number come from?

The reason, as always in the end with computers, is very logical. But to understand the logic I needed to adjust my mental model. When doing object oriented programming I am always thinking about an object, that I create, and the object has some responsibility what it needs to do. Which works when using Python or Java. But not in C. Instead in C I need to consider variables as just being places in memory. So when I created `start` in the code above C only reserved some space to put the values. And that space already existed in my computer and had some value from earlier in it. And thus the struct got created with a "random" number.

## My first SEGFAULT

Another issue that cost me some time to understand was my first SEGFAULT. When I ran my solution it would crash and just say SEGFAULT. That means that my program was reading or writing to some memory that wasn't allowed. I didn't understand why that was happening. As far as I could see I just created my own variables and read and write from them. Why would some variable suddenly be in a restricted place?

So as any sane developer would do, I dumped a whole load of print statements in my code to figure out what was going on. When I ran the program I could see my functions being called, It looped over some arrays with a couple dozen items in it. And then my code entered a function where it should loop over two arrays, x and y. Which should be a couple dozen times. But it immediately and completely filled my console with my print statements. That is not what I expected. That is way too many iterations of that loop.

So that is when I found out that C does not do bounds checking. I was looping over two arrays, but I never incremented the second counter so I had written an endless loop. And in the loop the code read from a memory position based on the counters. This resulted in C reading further and further into memory as that counter increased, until it bumped into memory which it wasn't allowed to read, where it would then SEGFAULT.

This error solidified the earlier learning point. An array doesn't really exist in C. When you "create" an array, you just reserve space in memory for an x amount of the type. And when you use an index to access an item in an array, C doesn't know whether that variable was created as an array. It just moves into memory where the x'th item would be if it was an array of that item. So in my mind C now feels like a bunch of syntactic sugar to manipulate places in memory.

## The program

You can find the completed program here below. In case you are interested in reading and/or judging the C code of a developer that has never written anything in C before 🙃

```c
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

struct Coordinate
{
  int x;
  int y;
};

struct Line
{
  struct Coordinate one;
  struct Coordinate two;
};

struct ParsedInput
{
  int max_y;
  int max_x;
  int line_count;
  struct Line lines[500];
};

void tiles_with_two_overlapping_lines(int max_y, int max_x, int diagram[max_x][max_y])
{
  int count = 0;
  for (int y = 0; y < max_y; y++)
  {
    for (int x = 0; x < max_x; x++)
    {
      if (diagram[x][y] > 1)
      {
        count++;
      }
    }
  }
  printf("Result=%d\n", count);
}

struct ParsedInput read_input()
{
  FILE *file = fopen("day_5_input.txt", "r");
  char line[20];
  fgets(line, 20, file);
  struct ParsedInput parsedInput;
  parsedInput.line_count = 0;
  parsedInput.max_y = 0;
  parsedInput.max_x = 0;
  while (line != NULL)
  {
    char *ptr = strtok(line, ",");
    parsedInput.lines[parsedInput.line_count].one.x = atoi(ptr);
    if (parsedInput.lines[parsedInput.line_count].one.x > parsedInput.max_x)
    {
      parsedInput.max_x = parsedInput.lines[parsedInput.line_count].one.x;
    }
    ptr = strtok(NULL, " ");
    parsedInput.lines[parsedInput.line_count].one.y = atoi(ptr);
    if (parsedInput.lines[parsedInput.line_count].one.y > parsedInput.max_y)
    {
      parsedInput.max_y = parsedInput.lines[parsedInput.line_count].one.y;
    }
    ptr = strtok(NULL, " ");
    ptr = strtok(NULL, ",");
    parsedInput.lines[parsedInput.line_count].two.x = atoi(ptr);
    if (parsedInput.lines[parsedInput.line_count].two.x > parsedInput.max_x)
    {
      parsedInput.max_x = parsedInput.lines[parsedInput.line_count].two.x;
    }
    ptr = strtok(NULL, ",");
    parsedInput.lines[parsedInput.line_count].two.y = atoi(ptr);
    if (parsedInput.lines[parsedInput.line_count].two.y > parsedInput.max_y)
    {
      parsedInput.max_y = parsedInput.lines[parsedInput.line_count].two.y;
    }
    parsedInput.line_count++;
    if (fgets(line, 20, file) == NULL)
    {
      break;
    }
  }
  return parsedInput;
}

int max(int a, int b)
{
  return a > b ? a : b;
}
int min(int a, int b)
{
  return a <= b ? a : b;
}

void print_diagram(int max_x, int max_y, int diagram[max_x][max_y])
{
  for (int y = 0; y < max_y; y++)
  {
    for (int x = 0; x < max_x; x++)
    {
      printf("%d ", diagram[x][y]);
    }
    printf("\n");
  }
  printf("\n");
}

void fill_diagram(struct ParsedInput *parsedInput, int max_y, int max_x, int diagram[max_x][max_y])
{

  for (int i = 0; i < parsedInput->line_count; i++)
  {
    struct Line line = parsedInput->lines[i];
    if (line.one.x == line.two.x)
    {
      int larger = max(line.one.y, line.two.y);
      int smaller = min(line.one.y, line.two.y);
      for (int j = smaller; j <= larger; j++)
      {
        diagram[line.one.x][j]++;
      }
    }
    else if (line.one.y == line.two.y)
    {
      int larger = max(line.one.x, line.two.x);
      int smaller = min(line.one.x, line.two.x);
      for (int j = smaller; j <= larger; j++)
      {
        diagram[j][line.one.y]++;
      }
    }
    else
    {
      int most_left = min(line.one.x, line.two.x);
      struct Coordinate *start;
      struct Coordinate *end;
      if (line.one.x == most_left)
      {
        start = &line.one;
        end = &line.two;
      }
      else
      {
        start = &line.two;
        end = &line.one;
      }
      int y_direction;
      if (end->y > start->y)
      {
        y_direction = 1;
      }
      else
      {
        y_direction = -1;
      }
      for (int j = 0; j <= (end->x - start->x); j++)
      {
        diagram[start->x + j][start->y + (j * y_direction)]++;
      }
    }
  }
}

int main()
{
  printf("Start\n");
  struct ParsedInput parsedInput = read_input();
  parsedInput.max_x++;
  parsedInput.max_y++;
  int diagram[parsedInput.max_x][parsedInput.max_y];
  memset(diagram, 0, sizeof(diagram));
  fill_diagram(&parsedInput, parsedInput.max_y, parsedInput.max_x, diagram);
  tiles_with_two_overlapping_lines(parsedInput.max_x, parsedInput.max_y, diagram);
  return 0;
}
```