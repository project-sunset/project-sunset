{
  "title": "Automating groceries",
  "snippet": "Because I don't care what I eat",
  "tags": ["medium project", "web", "automation", "JavaScript", "Vue", "Python", "Django"]
}

# Groceries

When I started building my website, one of the first things I created was a groceries app. I don't have a passion for cooking so an app that can pick recipes for me is great. In essence it is a very simple app, you put in your recipes:

<img src="/static/blog/groceries/recipe.jpg" alt="A webpage form with inputs for a recipe, with name, instructions, default servings and ingredients." />

And then when you go to create a grocery list it will automatically add the two recipes that haven't been used for the longest time. And you can add loose items as well.

<img src="/static/blog/groceries/create_grocery_list.jpg" alt="A webpage showing two recipes to be added and a loose item." />

Then you can go shopping and see the list and cross the items off.

<img src="/static/blog/groceries/shopping.jpg" alt="A grocery list where you can check off items." />

Every item also has an order property, so they show in the order that they are in the supermarket.

It is a pretty simple webapp, also one that I haven't really changed in years. I do have plenty of ideas to improve it, that I keep needing to ignore because of all my other side projects that also have ideas that need implementing `¯\_(ツ)_/¯`