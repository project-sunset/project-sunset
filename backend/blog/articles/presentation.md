{
  "title": "Projecting without a projector",
  "snippet": "Building a simple remote controlled presentation",
  "tags": ["one day project", "web"],
  "rank": 1
}

# Presentation

As anyone with a job in IT will know, a friend might ask for your help with some IT issue. This time a friend wanted to show some photos at a wedding. But there was one challenge: the venue didn't have a projector. Everyone would probably have a phone, so we just needed to find something to put the photo's on and have the presentation on everyone's phone go the next photo when my friend pressed a button.

I figured PowerPoint or Google Slides would have some functionality to share a presentation. But those options where way too complicated and/or had very long URLs. Which would suck because she didn't have the phone number of every guest. Maybe a URL shortener would solve that specific issue, but it seemed that the existing options I found where too complicated or are a paid service.

But luckily I had some free time, and this seemed like a fun project to build in an evening!

The content of this project should be pretty simple. There are two HTML pages: one shows the photo the presentation is currently on, and the other has some controls to set the presentation to the next (or some other) slide.

I'm going to store the photos in a BackBlaze B2 bucket. For simplicity's sake I'll create a database model that just stores a comma separated string of all files that are in it, a name column, and a column to store which slide everyone should look at. For this one-off time I'm not going to make a function where someone can upload their own photos. I'll manually upload them in the bucket.

```python
class Presentation(models.Model):
    name = models.CharField(max_length=100)
    slides = models.TextField()
    current_slide = models.CharField(max_length=100)
```

Now we have the photos in the bucket, and a reference to the photos in our database. For the viewer page we'll create some HTML with an `img` that is set to full up the entire screen.

```html
<!-- Abbreviated HTML -->
<head>
  <style>
    .slide {
      max-height: 100vh;
      max-width: 100vw;
    }
  </style>
</head>
<body style="margin: 0">
  <img id="slide" class="slide">
</body>
```

And when the page is loaded we'll call the backend to retrieve the current slide and set the `src` of the `img` tag to it:

```javascript
fetch('/various/presentation-slide/{{ name }}/')
  .then(response => response.json())
  .then(data => {
    if (data.current_slide !== currentSlide) {
      img.src = baseurl + data.current_slide
      currentSlide = data.current_slide
    }
  })
```

Then we can wrap that function into a `setInterval` so that we check which slide we should show every 2 seconds.

```javascript
setInterval(function () {
  fetch('/various/presentation-slide/{{ name }}/')
    .then(response => response.json())
    .then(data => {
      if (data.current_slide !== currentSlide) {
        img.src = baseurl + data.current_slide
        currentSlide = data.current_slide
      }
    })
}, 2000)
```

This should work, but we can make it more resilient. We are only waiting for two seconds before doing the next call. If someone is on a slow network maybe the page will start doing multiple calls at the same time which can then return out of order. If we add a variable we can make sure that only one call is happening at the same time:

```javascript
var waitingForFetch = false
setInterval(function () {
  if (waitingForFetch) {
    return
  }
  waitingForFetch = true
  fetch('/various/presentation-slide/{{ name }}/')
    .then(response => response.json())
    .then(data => {
      if (data.current_slide !== currentSlide) {
        img.src = baseurl + data.current_slide
        currentSlide = data.current_slide
      }
      waitingForFetch = false
    })
}, 2000)
```

Now there should only be one fetch happening at any time.

There is one more improvement I'd like to make. Maybe for some reason stuff crashes. And the classic solution to that would be to restart whatever is crashing. We can automate that, we can reload the page if the fetch or processing it crashes four times:

```javascript
var errs = 0
var waitingForFetch = false
setInterval(function () {
  if (waitingForFetch) {
    return
  }
  waitingForFetch = true
  fetch('/various/presentation-slide/{{ name }}/')
    .then(response => response.json())
    .then(data => {
      errs = 0
      if (data.current_slide !== currentSlide) {
        img.src = baseurl + data.current_slide
        currentSlide = data.current_slide
      }
      waitingForFetch = false
    }).catch(err => {
      errs += 1
      if (errs >= 4) {
        location.reload()
      }
      waitingForFetch = false
    })
}, 2000)
```

That should be pretty resilient.

The page with the controls should only be accessible by my friend and not for the audience. For this project I'll take the "security by obscurity" approach: just not tell anyone except my friend the URL for reaching the control page 🙃 The control page itself is straightforward. Show all the slides, and buttons for restart, previous and next. All of those call the same backend URL that sets the `current_slide` column in the database, and the clients will update their view within two seconds.

<img src="/static/blog/presentation/presentation.jpg" alt="A screenshot showing two webpages, one only a slide and the other also buttons for restart, previous and next, and the other slides in the presentation." />

Now we just need a simple URL, a URL shortener is an option. But more fun is to buy a .nl domain, that is only about two euro's for the first year. So I bought a domain and used a CloudFlare page rule to redirect everything to the audience page on my website.

My friend was very happy with the result and got some reviews from the audience:

<blockquote>"This works really well"</blockquote> 
<blockquote>"Huh, how did they do this?"</blockquote>

I think these are the best two responses a software developer can get for their work 😁