{
  "title": "My primary school celebrates the jubilee year!",
  "snippet": "Creating a simple website for my primary school's jubilee event, with a lesson about UX design",
  "tags": ["medium project", "web", "Python", "Django"],
  "rank": 2
}

# PJubeljaar

The primary school I went to, and where my mother works, celebrated its 50 year existence in 2018 with an event. The school's name is "Pieter Jongelingschool", often shortened to "PJ". And in the Hebrew tradition the Jubilee ("Jubeljaar" in Dutch) happens every 50 years. So they named the event "PJubeljaar". They wanted to have a website for this event, and my mother asked if I could build one. Since I really like helping people, and of course especially my mother, I couldn't say no!

My brother-in-law had already created a logo for the event, and with a couple of old pictures and color gradients I made a nice front page.

<img src="/static/blog/pjubeljaar/frontpage.jpg" alt="The front page of the Jubilee website" />

The school wanted to have an idea of how many people to expect, and also send them some more information beforehand so I added a form that people could fill in.

<img src="/static/blog/pjubeljaar/form.jpg" alt="A sign up form on the website" />

Of course anyone interested in the event would want to know how many people from their time would come. In 50 years there are many people coming and going. So I created a page that listed per school year how many people signed up.

<img src="/static/blog/pjubeljaar/opkomst.jpg" alt="A table on the website showing how many students, teachers and other people are coming" />

And finally for the school itself I created an admin page behind a login that shows everyone that signed up and any remarks they gave in the form. They can export it to a CSV to use elsewhere and can set a remark to "seen" so they know if they answered all the questions.

<img src="/static/blog/pjubeljaar/admin.jpg" alt="A webpage showing three tables, one for sign-ups, one for new remarks, and one for seen remarks" />

This was a pretty easy build, there are only a couple of pages on the website. But the hard part, as always, are users. In the form there is a field for maiden name. In 50 years time many girls would be married now and many would have taken their husbands name. Which would be confusing since the school only has records with their maiden name, and their classmates may also only know their maiden name. So we wanted to specifically ask people to also give their maiden name (which the school would put on name tags).

In the admin screen I would then hyphenate the last name and maiden name since that is what is usually correct. Of course some people signed up with their last name like "Freek-Lieber" and also fill in "Lieber" has their maiden name. So the admin screen would show "Freek-Lieber-Lieber". Slightly wrong, but it's fine. I could have written something to fix that case.

Another case of users being annoying are the ones that decided to fill in "na", as in "not applicable" to the field, resulting in "Freek-no applicable". I don't know what gave them that idea, I would expect users to just leave it empty if it is not applicable and not required. But apparently that was not a correct assumption. But this case can also be fixed with some checks for these kind of inputs.

The case I can't fix is are the funny people. For example one of my teachers, who didn't take his wife's name, decided that he should fill in how he would like to be called as a girl: "Smit-Elizabeth" 🤨 And he was not the only one who had that idea.

Building the website was a fun project. Learning how bad users can be at filling in forms was the lesson 😁