{
  "title": "Creating a memory game",
  "snippet": "To learn a bit of JavaScript",
  "tags": ["medium project", "web", "JavaScript", "Game"]
}

# Memory game

When I started working at Calco I needed to improve my JavaScript skills. One option would be to follow a course, but I like to just get started and build something. So I decided to build a game, and I landed on building a [memory game](https://en.wikipedia.org/wiki/Concentration_(card_game)).

It was a fun project to build, for a large part because it is a quick game to play so I send a link out on Instagram and to some colleagues and quickly the high scores list filled up. At first it looked okay. But after some time my fellow engineers found hacking the high scores more fun than playing the game 🤔 So I started adding more code to try and block their cheating attempts. It went back and forth for some days. Where I mostly learned that trying to check for cheating purely on the client side is not a great way to do it.

You can find the game (without the high scores 😅) [here](/various/memory/).

<img src="/static/blog/memory/memory.jpg" alt="A memory game in progress on the website" />