{
  "title": "Building a Discord Bot",
  "snippet": "When friends inspire me and I have some leisure time to kill.",
  "tags": ["medium project", "APIs", "Python"]
}

# Discord Points Bot

Once upon a time, a friend had a Discord server were he wanted to give points to people. And if you have points, you could give them away to other people. And you can create bets and bet your points to get more. Since I was bored on a Sunday afternoon I figured I could probably build that. Writing the bot was a fun project, but this blog will be a quick demonstration of a new thing I learned, namely Python packaging!

## How to build a Python package

My personal website is build with Python using Django, and since the bot needs to be reached by Discord over the internet it made the most sense to also write the bot for Django. But to give myself something new to learn I decided to not write it as a new part of my website, but instead as a generic reusable Django application.

The part where I learned the most new things took place in `pyproject.toml`, before this I had never packaged a Python project for reusing in another Python project. I figured that it wouldn't be very hard. And if you look in the file it looks pretty straight forward. Python as a language is known to be easy to use, but figuring out how to package it was not. There are many ways to skin that cat, and because with Python you need different build backends, build frontends and publishers the documentation of how to do the full process feels a bit cluttered.

But after a lot of fiddling around I settled on about 30ish lines of code for all of the build setup. But reaching that state took me about two evenings of fiddling, so the lines of code per hour metric is pretty bad 🙃 Hence I am going to milk it in this blog, and tell you all about it! This below is my `pyproject.toml` file:

```toml
[build-system]
requires = ['setuptools>=61.0',"setuptools-scm>=6.2"]
build-backend = 'setuptools.build_meta'

[project]
name = "django-discord-points-bot"
description = "A reusable Django application that is a Discord bot"
readme = "README.md"
requires-python = ">=3.8"
classifiers = [
  "Environment :: Web Environment",
  "Framework :: Django :: 4.0",
  "License :: OSI Approved :: MIT License",
  "Operating System :: OS Independent",
  "Programming Language :: Python :: 3.8",
  "Programming Language :: Python :: 3.9",
]
dependencies = [
  "discord-interactions==0.4.0",
  "requests"
]
dynamic = ["version"]

[project.urls]
"Documentation" = "https://bobluursema.gitlab.io/django-discord-points-bot"

[tool.setuptools_scm]
```

`pyproject.toml` is a standard file for Python defined in [PEP-518](https://peps.python.org/pep-0518/). This is the file that will tell the build backend what to do. In this case it is very straightforward, so most of the content is just metadata about the package. The part that took the most time is figuring out how to version the packages. In the `pyproject.toml` file you need to have a version property, but I don't like to manually update things if I can automate it. And `setuptools-scm` has magic support to use git tags to figure out what version it should put in the data. But that required three lines in the toml that for some reason took me quite a while to figure out.

First you need to add that package to the build-system `requires` property, then add an empty section: `[tool.setuptools_scm]`. Then the build would complain that version is not defined, so in the `[project]` section you need to specify that `version` will be dynamic.

Once this is all setup, the actual build process is easy. First you call a build frontend, I am using [Build](https://pypa-build.readthedocs.io/en/stable/), so in the pipeline job I run `python -m build`, this will read the `pyproject.toml`, and then call the configured build backend to create the actual distribution. And then you need to publish it, I am using [Twine](https://twine.readthedocs.io/en/stable/) for that, so next I call: `python -m twine upload --non-interactive dist/*`.

As is often the case, once you have everything neatly configured it looks easy. But it takes a lot of reading of documentation to get to that point sometimes 😄 And after all that is done and I added the bot to my website, I can open up a Discord server and see that my fresh new bot is alive and ready to accept some meaningless bets!

<img src="/static/blog/discord-points-bot/Screenshot.jpg" alt="The UI of Discord showing the custom commands that the bot has."/>

Oh, and if you want to use it, or install it on your own Django site, [read the docs here](https://bobluursema.gitlab.io/django-discord-points-bot/).