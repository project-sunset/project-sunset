{
  "title": "Implementing a computer from scratch",
  "snippet": "Following along with \"Elements of Computing Systems\"",
  "tags": ["medium project", "Go"]
}

# Implementing a computer from scratch

Once upon a time I ran into this book, [The Elements of Computing Systems](https://www.amazon.com/Elements-Computing-Systems-Building-Principles/dp/0262640686/). The book explains how a computer works. Starting from the logic gates, and going all the way to the screen. It uses a hands-on approach where you will build all the parts yourself. I found this really interesting so I decided to take a look! With the book also comes some software to use to build everything, instead of using that I decided to at the same time learn some [Go](https://go.dev/). So while reading the book I implemented all the parts of the computer. Starting with, of course, a NAND gate:

```go
func nand(input1 bool, input2 bool) bool {
  return !(input1 && input2)
}
```

Which we can use to create a XOR gate:
```go
func xor(input1 bool, input2 bool) bool {
  result1 := nand(input1, input2)
  result2 := nand(result1, input1)
  result3 := nand(result1, input2)
  return nand(result2, result3)
}
```

And we can use those gates to create larger things, like a full adder:

```go
func fullAdder(input1 bool, input2 bool, input3 bool) (bool, bool) {
  sum1, carry1 := halfAdder(input1, input2)
  sum2, carry2 := halfAdder(sum1, input3)
  return sum2, or(carry1, carry2)
}
```

For memory I couldn't just use my `nand` function, since we need someway to keep the state in Go, so we get a bit with a flip flop:

```go
type bit struct {
  dff dataFlipFlop
  out bool
}

func (b *bit) tick(in bool, load bool) {
  b.dff.tick(mux(b.dff.currentValue, in, load))
  b.out = b.dff.currentValue
}
```

And after some more chapters and more writing of Go, and a bit of Vue to visualize it, I got myself a computer!

<img src="/static/blog/computing-systems/GUI.jpg" alt="A webpage showing the decoded ROM, the RAM, the CPU registers and a screen" />

You can find the source code of my project on [GitLab](https://gitlab.com/bobluursema/hack-computer).