{
  "title": "Domain knowledge for remembering drinks",
  "snippet": "Understanding the problem makes solving it easier",
  "tags": ["one day project", "web", "JavaScript", "Vue"]
}

# Drinks Rememberer

At my job there was a custom that if you went to get a drink you asked everyone nearby if they would like something. But, that could sometimes be twelve people. And remembering twelve coffee types and tea flavors can get hard. There were three strategies that colleagues used to deal with this problem:

1. Grab pen and paper and write everything down.
2. Vaguely remember it and make sure to bring enough. And just give people multiple drinks if you brought too many. And/or go a second time if you forgot something.
3. Just [git gud](https://en.wiktionary.org/wiki/git_gud) and remember everything.

I don't have pen and paper, and I want to be efficient, and I couldn't remember everything. So none of these options worked for me.

So, my solution is to quickly write a webpage were I can put in the orders. Easy peasy lemon squeezy.

<img src="/static/blog/drinks/drinks.jpg" alt="An orange and yellow webpage with a counter for every type of drink" />

The only reason why this was a very quick page to build was because there is very little domain knowledge. So it is easy to understand what the problem is and how to solve it. After getting the drinks I would return to my desk in the IT spaghetti of a 15.000+ employee financial company. Where building software usually isn't hard, but building the correct software is hard. Sometimes it might even be hard just to figure out what the correct value of one field in an API request is. Partly because someone, sometime, somewhere decided that real words are overrated and that fields named `CDUU` and `MDGEG` are perfectly fine.

Building an easy page that just needs to count drinks is a good distraction to feel more productive in some moments 🙃

You can find the app [here](#/various/drinks/)!