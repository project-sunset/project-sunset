{
  "title": "Why I like apps with API's",
  "snippet": "Removing another spreadsheet from my life",
  "tags": ["medium project", "web", "APIs", "automation", "JavaScript", "Vue", "Python", "Django"]
}

# Overtime Tracker

My main activity when I started working was solving incidents. Some were small incidents like a customer that couldn't see their mortgage product in the app, or an error when someone tried to upload a document. And also large issues, like a network team that did a "non-breaking firewall change" due to which the entire company's network infrastructure broke. Resulting in us trying to figure out what happens when some messages got through, others didn't, and some only partially. Fun times.

In that line of work you can't always plan when you will start work or stop work. If something bad happens at 17:00 it is probably good to stay a bit longer to fix it. And in order to reduce administrative overload, and give employees more flexibility in when they want to work most of this work was done with compensatory time. So when you worked a bit longer one day, you can go home early another day. It is a nice system 👌

But it also means that I want to keep track of how many hours I work everyday. Otherwise I don't know if I am working too much or too little. I first used a spreadsheet and fill in everyday how many hours I worked, but as a software developer I can probably automate that.

I could build all of the needed functionality myself. But that may not be necessary. I am already using [Toggl](https://toggl.com/) for some time tracking, so I could just start and stop a Toggl timer when I am working.

Toggl has an easy to use API, simply get an API key from your account and then you can access all of your data programmatically. For my use case I just need to get all the time entries between the last call and today, check if it is an entry for work and see if it is more or less than 8 hours per work day to determine how much overtime I made.

One thing goes wrong here, because if I take a vacation day it will still assume that I should have worked 8 hours. But luckily solving that is easy! Because Outlook also has an API, the Microsoft Graph API. So I can retrieve my calendar events to see if there is a vacation event (as long as I name them consistently). Getting authorization and calling it is a bit more complicated than Toggl's API, but it is not too difficult.

And after some crafting I got a nice webpage to show me the current status!

<img src="/static/blog/overtime-tracker/home_screen.jpg" alt="A webpage showing a graph of how many running overtime hours I had in the past 100 days." />

I really like services that have an API that I can use. It enables me to create any customization that I want. Of course a user building their own service on top of that API is a bit of a niche. But having an open API also enables other parties to create all sorts of integrations. And who doesn't like their tools to be better integrated?