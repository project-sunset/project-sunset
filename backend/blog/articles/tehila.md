{
  "title": "Band repertoire management",
  "snippet": "A website for my church bands",
  "tags": ["long term project", "web", "JavaScript", "Vue", "Python", "Django"],
  "rank": 4
}

# Tehila

I play in a band in my church. The songs in a service are usually chosen by the pastor with suggestions from the band. But there are literally thousands of songs to choose from, and as a band we can't learn a dozen new songs every time. So it is useful to have a repertoire of which songs we already know and have played. That makes it easier for the pastor to choose mostly songs we already know, and only a couple new ones. Before I joined this band the list was kept up to date in a spreadsheet, and every now and then they would email the pastor a new list.

As a software developer I found that unacceptable. I first created a new part in my personal website to keep track of the repertoire, but I quickly noticed that this was a) big enough to deserve it's own website, and b) important enough that it should be separate from the tinkering I do on my personal website. So I moved everything over and did some [platform engineering](#/blog/platform-engineering/) to make my life easier.

There are two parts of the website, a public part for the pastor (and guest pastors) to view and search our repertoire, and a private part where the band members can manage the events, recordings and metadata.

## Public repertoire

On the public repertoire the paster can view all the songs that we have played in earlier services. Most of them will also have some descriptive tags about the style or subject matter of the song. And there are a couple search fields to search for a title or in the lyrics and such.

<img src="/static/blog/tehila/public-repertoire.jpg" alt="A webpage with a list of songs and search inputs for title, lyrics, song book and tags." />

You can open a song and see some more information. You can see in which song books this song is found, when the last time was that we played it, the lyrics of the song, and a couple links at the bottom that link to the search function of Google, Spotify and YouTube for when the pastor wants to listen to a song. Because these buttons use the search function I don't need to configure a link for every song.

<img src="/static/blog/tehila/public-card.jpg" alt="A webpage with the data of song: the title, song books, last played date, lyrics and buttons on the bottom." />

The database query for the repertoire joins a bunch of tables to get all the information. And the repertoire itself generally doesn't change that often. So Tehila caches the result for a week (and invalidates the cache if needed).

## Event management

In Tehila the bands create events, which can be a service or a practice session. Then they add songs to those events and the repertoire is generated based on this information.

<img src="/static/blog/tehila/event.jpg" alt="A webpage with the icon and date of an event, and a list of songs." />

All events can be viewed in an agenda. Which currently looks rather awful. Because I don't want to query all of the previous and next events. But I do want to be able to reach them. A calendar grid view might make sense, but I think the list is easier to read. Definitely because a month on a calendar grid would be mostly empty in our case. So I'm not yet sure how I want to redesign this part.

<img src="/static/blog/tehila/agenda.jpg" alt="A webpage showing a list of services and practice sessions dates" />

What I do really like is the subscription functionality. There is an [IETF RFC](https://datatracker.ietf.org/doc/html/rfc5545) which describes the iCalendar format that most calendar applications can import. I first looked at using a library, but when I read the specification it became clear that for my use case a library is overkill. I can easily create the necessary file myself since I want to keep it simple anyway. Add a public URL with a UUID to make it sort of not public in case we add personal information and then everyone can add the events to their favorite app!

<img src="/static/blog/tehila/ics.jpg" alt="A webpage showing the link and explanation to add the events to your calendar." />

## Song management

In some ways Christian organizations are similar to JavaScript frontend developers. Developers like to create new JavaScript frameworks that do almost the exact same thing. And Christian organizations like to create song books with almost the exact same songs. And then you end up with websites like [kerkliedwiki.nl](https://kerkliedwiki.nl/) that try to organize that mess so you can see that you can find "God is getrouw" in 18 different song books.

<img src="/static/blog/tehila/many-song-books.jpg" alt="A webpage of kerkliedwiki of God is getrouw." />

Tehila doesn't (yet) contain all of those book, but I do like to strive to have as many of the ones we use available. So I have searched the internet for websites and file shares to find as many lyrics and song books as I can. Some websites have a predictable structure to find lyrics for a certain song book, or someone hosted a collection of `txt` files with lyrics for songs of a book for some reason. For each location I wrote a Python script to organize whatever I found into structured data that I can import into Tehila in a custom JSON structure that also does deduplication if needed.

<img src="/static/blog/tehila/import-songs.jpg" alt="A webpage showing a list of song books and imports for JSON and Open lyrics." />

Every band member can create new songs if it doesn't exist yet. But I keep this import screen for myself, since otherwise I would need to update the UI to tell a user how it works. And it works kinda janky, and kinda buggy. But since I wrote it (and can read the source code if I forget 🙃), I can work my away around the jank.

## Other functions

There are two other major functionalities in Tehila, but those deserve their own blog:

- [Song recordings](#/blog/tehila-recorder/), creating and organizing recordings of practice sessions
- [Sheet music editor](#/blog/tehila-song-editor/), creating sheet music