from django.urls import path

from blog.views import feed, article

app_name = 'blog'
urlpatterns = [
    path('', feed, name='feed'),
    path('<str:slug>/', article, name='article')
]
