from rest_framework import serializers
from backend.drf_customization import UserFilteredPrimaryKeyRelatedField, FilterArchivedListSerializer
from finances.models import (AmortizeItem, Category, Dataset, SearchTerm, Transaction, Balance, BalanceItem,
                             UnprocessedTransaction, BudgetArchive)


class SimpleCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name')


class SpecificationSerializer(serializers.ModelSerializer):
    specification_of = UserFilteredPrimaryKeyRelatedField(queryset=Category.objects.filter(specification_of__isnull=False), required=False, allow_null=True)

    class Meta:
        model = Category
        list_serializer_class = FilterArchivedListSerializer
        fields = ('id', 'name', 'budget', 'is_income', 'specification_of', 'archived', 'ignore_for_transaction_delta')


class SimpleDatasetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dataset
        fields = ('id', 'name')


class SimpleSearchTermSerializer(serializers.ModelSerializer):
    dataset = SimpleDatasetSerializer()

    class Meta:
        model = SearchTerm
        fields = ('id', 'term', 'dataset')


class CategorySerializer(serializers.ModelSerializer):
    specifications = SpecificationSerializer(many=True, read_only=True)
    specification_of = UserFilteredPrimaryKeyRelatedField(queryset=Category.objects.filter(specification_of__isnull=True), required=False, allow_null=True)

    class Meta:
        model = Category
        fields = ('id', 'name', 'is_income', 'budget', 'specifications', 'specification_of', 'archived', 'ignore_for_transaction_delta')


class TransactionSerializer(serializers.ModelSerializer):
    category_obj = SimpleCategorySerializer(read_only=True, source='category')
    specification_obj = SimpleCategorySerializer(read_only=True, source='specification')
    specification = UserFilteredPrimaryKeyRelatedField(queryset=Category.objects.filter(specification_of__isnull=False), write_only=True, required=False, allow_null=True)
    found_by = SimpleSearchTermSerializer(read_only=True)

    class Meta:
        model = Transaction
        fields = ('id', 'date', 'amount', 'specification', 'payload', 'process_datetime', 'found_by', 'category_obj', 'specification_obj')
        read_only_fields = ('process_datetime', 'found_by')


class DatasetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dataset
        fields = ('id', 'name', 'date_field', 'date_format', 'amount_field', 'processed_transactions')
        read_only_fields = ('processed_transactions',)


class SearchTermSerializer(serializers.ModelSerializer):
    dataset_obj = SimpleDatasetSerializer(read_only=True, source='dataset')
    category_obj = SimpleCategorySerializer(read_only=True, source='category')
    specification_obj = SimpleCategorySerializer(read_only=True, source='specification')
    dataset = UserFilteredPrimaryKeyRelatedField(queryset=Dataset.objects.all(), write_only=True, required=False, allow_null=True)
    specification = UserFilteredPrimaryKeyRelatedField(queryset=Category.objects.filter(specification_of__isnull=False), write_only=True, required=False, allow_null=True)

    class Meta:
        model = SearchTerm
        fields = ('id', 'dataset', 'term', 'field', 'specification', 'hits', 'category_obj', 'specification_obj', 'dataset_obj', 'archived')
        read_only_fields = ('hits',)


class BalanceItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = BalanceItem
        fields = ('id', 'name', 'balance_type', 'archived')


class BalanceListSerializer(serializers.ListSerializer):
    def update(self, instances, validated_data):
        new_ids = set()
        for item in reversed(validated_data):
            if 'id' not in item:
                b = Balance(
                    user=instances[0].user,
                    item=item['item'],
                    date=item['date'],
                    amount=item['amount'],
                )
                validated_data.remove(item)
                b.save()
                new_ids.add(b.id)
        updated_ids = set(b['id'] for b in validated_data)
        for instance in reversed(instances):
            if instance.id in new_ids:
                pass
            elif instance.id not in updated_ids:
                instance.delete()
            else:
                instance.amount = [x for x in validated_data if x['id'] == instance.id][0]['amount']
                instance.save()
        return instances.all()


class BalanceSerializer(serializers.ModelSerializer):
    item_obj = BalanceItemSerializer(read_only=True, source='item')
    item = UserFilteredPrimaryKeyRelatedField(queryset=BalanceItem.objects.all(), write_only=True)
    id = serializers.IntegerField(required=False)

    class Meta:
        model = Balance
        fields = ('id', 'date', 'amount', 'item', 'item_obj')
        list_serializer_class = BalanceListSerializer


class UnprocessedTransactionSerializer(serializers.HyperlinkedModelSerializer):
    dataset = SimpleDatasetSerializer(read_only=True)

    class Meta:
        model = UnprocessedTransaction
        fields = ('id', 'process_datetime', 'data', 'dataset')


class BudgetArchiveSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BudgetArchive
        fields = ('id', 'payload', 'creation_date')


class AmortizeItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = AmortizeItem
        fields = ('id', 'name', 'price', 'expected_lifetime_in_months', 'date_of_purchase', 'archived')