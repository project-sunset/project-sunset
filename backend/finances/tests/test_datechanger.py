import unittest
from finances.datechanger import * 

"""Tests for the DateChanger module."""
class TestDateChanger(unittest.TestCase):
    # change month tests
    def test_dont_change_month(self):
        self.assertEqual(delta_month(date(2017, 4, 8), 0), date(2017, 4, 1))
    def test_add_one_month(self):
        self.assertEqual(delta_month(date(2017, 1, 1), 1), date(2017, 2, 1))
    def test_subtract_one_month(self):
        self.assertEqual(delta_month(date(2017, 2, 1), -1), date(2017, 1, 1))
    def test_add_multiple_months(self):
        self.assertEqual(delta_month(date(2017, 4, 23), 5), date(2017, 9, 1))
    def test_subtract_multiple_months(self):
        self.assertEqual(delta_month(date(2017, 7, 12), -3), date(2017, 4, 1))
    def test_add_to_year_change(self):
        self.assertEqual(delta_month(date(2017, 11, 1), 3), date(2018, 2, 1))
    def test_subtract_to_year_change(self):
        self.assertEqual(delta_month(date(2017, 1, 5), -2), date(2016, 11, 1))
    def test_input_month_equals_twelve(self):
        self.assertEqual(delta_month(date(2017, 12, 24), -1), date(2017, 11, 1))
    def test_output_month_equals_twelve(self):
        self.assertEqual(delta_month(date(2017, 11, 11), 1), date(2017, 12, 1))
    # last day of month
    def test_last_day_of_month(self):
        self.assertEqual(last_day_of_month(date(2017, 1, 20)), date(2017, 1, 31))
    def test_february_not_leap_year(self):
        self.assertEqual(last_day_of_month(date(2017, 2, 3)), date(2017, 2, 28))
    def test_february_is_leap_year(self):
        self.assertEqual(last_day_of_month(date(2016, 2, 15)), date(2016, 2, 29))
    def test_already_last_day(self):
        self.assertEqual(last_day_of_month(date(2017, 6, 30)), date(2017, 6, 30))
    # toggle date and datetime
    def test_date_to_datetime(self):
        self.assertEqual(toggle_date_datetime(date(2017, 3, 4)), datetime(2017, 3, 4, 0, 0, 0))
    def test_datetime_to_date(self):
        self.assertEqual(toggle_date_datetime(datetime(2017, 5, 2, 0, 0, 0)), date(2017, 5, 2))
    # change month things
    def test_change_month(self):
        self.assertEqual(change_month(date(2017, 4, 3)), date(2017, 5, 1))
    def test_change_month_last_day(self):
        self.assertEqual(change_month(date(2017, 2, 3), output_day='last'), date(2017, 3, 31))
    def test_change_month_return_date_string(self):
        self.assertEqual(change_month(date(2017, 6, 17), output='sql'), '2017-07-01')
    def test_change_month_add_multiple(self):
        self.assertEqual(change_month(date(2017, 1, 2), 3), date(2017, 4, 1))
    # test formatting
    def test_sql_date_format(self):
        self.assertEqual(to_sql_date(date(2017, 8, 9)), '2017-08-09')
    def test_sql_datetime_format(self):
        self.assertEqual(to_sql_date(datetime(2017, 4, 1, 7, 43, 2)), '2017-04-01 07:43:02')
