from datetime import date

from django.contrib.auth.models import User
from django.test import TestCase, tag
from finances.models import Category, Transaction, BalanceItem, Balance, Dataset, SearchTerm, UnprocessedTransaction, BudgetArchive


@tag('finances')
class ModelTests(TestCase):
    def setUp(self):
        self.u = User.objects.create(username='Tester', password='secret')

    def test_category_creation(self):
        a = Category.objects.create(user=self.u, name="cat", is_income=True, budget=100)
        self.assertTrue(isinstance(a, Category))
        self.assertEqual(a.__str__(), 'cat')

    def test_transaction_creation(self):
        a = Category.objects.create(user=self.u, name="cat", is_income=True, budget=100)
        b = Category.objects.create(user=self.u, name="spec", is_income=True, budget=100, specification_of=a)
        c = Transaction.objects.create(user=self.u, date=date(2018, 9, 9), amount=100, category=a, specification=b)
        self.assertTrue(isinstance(c, Transaction))
        self.assertEqual(c.__str__(), '2018-09-09')

    def test_balanceitem_creation(self):
        a = BalanceItem.objects.create(user=self.u, name="test", balance_type='liability')
        self.assertTrue(isinstance(a, BalanceItem))
        self.assertEqual(a.__str__(), 'test')

    def test_balance_creation(self):
        a = BalanceItem.objects.create(user=self.u, name="test", balance_type='liability')
        b = Balance.objects.create(user=self.u, item=a, date=date(2018, 9, 9), amount=1000)
        self.assertTrue(isinstance(b, Balance))
        self.assertEqual(b.__str__(), '2018-09-01 test')

    def test_dataset_creation(self):
        a = Dataset.objects.create(user=self.u, name='test', date_field='a', date_format='a', amount_field='a')
        self.assertTrue(isinstance(a, Dataset))
        self.assertEqual(a.__str__(), 'test')

    def test_searchterm_creation(self):
        a = Dataset.objects.create(user=self.u, name='test', date_field='a', date_format='a', amount_field='a')
        b = Category.objects.create(user=self.u, name="cat", is_income=True, budget=100)
        c = Category.objects.create(user=self.u, name="spec", is_income=True, budget=100, specification_of=b)
        d = SearchTerm.objects.create(user=self.u, dataset=a, term='term', field='a', category=b, specification=c)
        self.assertTrue(isinstance(d, SearchTerm))
        self.assertEqual(d.__str__(), 'term')

    def test_unprocessed_transaction_creation(self):
        a = Dataset.objects.create(user=self.u, name='test', date_field='a', date_format='a', amount_field='a')
        b = UnprocessedTransaction.objects.create(user=self.u, payload='{"test": 1}', dataset=a)
        self.assertTrue(isinstance(b, UnprocessedTransaction))
        self.assertEqual(b.__str__(), 'UT of test')
        self.assertEqual(b.data, {'test': 1})

    def test_budget_archive_creation(self):
        a = BudgetArchive.objects.create(user=self.u, payload='{"something":"test"}')
        self.assertEqual(a.__str__(), str(date.today()))
