from django.test import TestCase, Client, tag

@tag('finances')
class ViewTests(TestCase):
    fixtures = ['user_test_data.json', 'finances_test_data.json']

    def test_categories_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/finances/api/categories/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(len(r), 11)
    
    def test_transactions_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/finances/api/transactions/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(r['count'], 28)
    
    def test_datasets_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/finances/api/datasets/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(len(r), 1)
    
    def test_searchterms_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/finances/api/search-terms/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(len(r), 3)
    
    def test_balanceitems_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/finances/api/balance-items/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(len(r), 4)
    
    def test_balances_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/finances/api/balances/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(len(r), 15)

    def test_unprocessedtransactions_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/finances/api/unprocessed-transactions/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(r['count'], 0)
    
    def test_income_statement(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/finances/api/report/income_statement/')
        self.assertEqual(response.status_code, 200)
        r = response.json()
        self.assertEqual(r['data'], [])
    
    def test_expanded_income_statement(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/finances/api/report/expanded_income_statement/')
        self.assertEqual(response.status_code, 200)
    
    def test_total_balance(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/finances/api/report/total_balance/')
        self.assertEqual(response.status_code, 200)

    def test_budgetarchive_get(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/finances/api/budgetarchive/')
        self.assertEqual(response.status_code, 200)
    