from django.test import TestCase, tag, Client


@tag('finances')
@tag('investments')
class InvestmentTests(TestCase):
    fixtures = ['user_test_data.json', 'finances_test_data.json']

    def test_account_stats(self):
        c = Client()
        c.login(username='Kara', password='supergirl01')
        response = c.get('/finances/api/investments/accounts/1/stats/')
        self.assertEqual(response.status_code, 200)
        sums = response.json()['sums']
        def find(type):
            return [s for s in sums if s['transaction_type'] == type][0]['total']
        self.assertEqual(find('costs'), '20')
        self.assertEqual(find('deposit'), '10000')
        self.assertEqual(find('interest'), '10')
        self.assertEqual(find('withdrawal'), '500')

    def test_account_transactions(self):
        c = Client()
        c.login(username="Kara", password='supergirl01')
        response = c.get('/finances/api/investments/accounts/1/transactions/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(4, len(response.json()))

    def test_security_basic_stats(self):
        c = Client()
        c.login(username="Kara", password="supergirl01")
        response = c.get('/finances/api/investments/securities/?account=1')
        self.assertEqual(response.status_code, 200)
        stats = response.json()[0]['stats']
        self.assertEqual(stats['current_balance'], 2750)
        self.assertEqual(stats['current_balance_date'], '2024-07-01')
        self.assertEqual(stats['total_input'], 2015)
        self.assertEqual(stats['total_output'], 1070)
        self.assertEqual(stats['return'], 90)