import json
from collections import OrderedDict

from django.contrib.auth.models import User
from django.db import models


class Category(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    is_income = models.BooleanField()
    budget = models.DecimalField(max_digits=9, decimal_places=2)
    specification_of = models.ForeignKey('Category', null=True, related_name='specifications', on_delete=models.PROTECT)
    archived = models.BooleanField(default=False)
    ignore_for_transaction_delta = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.specification_of is not None:
            self.is_income = self.specification_of.is_income
            self.ignore_for_transaction_delta = self.specification_of.ignore_for_transaction_delta
        if self.specification_of is None and self.archived:
            self.specifications.all().update(archived=True)
        if self.specification_of is None and self.ignore_for_transaction_delta:
            self.specifications.all().update(ignore_for_transaction_delta=True)
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Categories'
        unique_together = (('name', 'user'),)

    def __str__(self):
        return self.name


class Transaction(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField()
    amount = models.DecimalField(max_digits=9, decimal_places=2)
    category = models.ForeignKey('Category', related_name='transaction_category', on_delete=models.PROTECT)
    specification = models.ForeignKey('Category', related_name='transaction_specification', on_delete=models.PROTECT)
    payload = models.TextField(null=True)
    process_datetime = models.DateTimeField(null=True)
    found_by = models.ForeignKey('SearchTerm', related_name='transaction_search_term', on_delete=models.SET_NULL, null=True)

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return str(self.date)


class Balance(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey('BalanceItem', related_name='balance_balance_item', on_delete=models.PROTECT)
    date = models.DateField()
    amount = models.DecimalField(max_digits=9, decimal_places=2)

    def __str__(self):
        return str(self.date) + " " + str(self.item.name)

    def save(self, *args, **kwargs):
        self.date = self.date.replace(day=1)
        super().save(*args, **kwargs)

    class Meta:
        unique_together = (('user', 'date', 'item'),)


class BalanceItem(models.Model):
    TYPES = (
        ('asset', 'Asset'),
        ('liability', 'Liability'),
        ('investment', 'Investment')
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    balance_type = models.CharField(max_length=20, choices=TYPES)
    archived = models.BooleanField(default=False)

    class Meta:
        unique_together = (('name', 'user'),)

    def __str__(self):
        return self.name


class SearchTerm(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    dataset = models.ForeignKey('Dataset', related_name='search_term_dataset', on_delete=models.PROTECT)
    term = models.CharField(max_length=200)
    field = models.CharField(max_length=50)
    category = models.ForeignKey('Category', related_name='search_term_category', on_delete=models.PROTECT)
    specification = models.ForeignKey('Category', related_name='search_term_specification', on_delete=models.PROTECT)
    hits = models.IntegerField(default=0)
    archived = models.BooleanField(default=False)

    def __str__(self):
        return self.term


class Dataset(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=20)
    date_field = models.CharField(max_length=20)
    date_format = models.CharField(max_length=20)
    amount_field = models.CharField(max_length=20)
    processed_transactions = models.IntegerField(default=0)
    archived = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class UnprocessedTransaction(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    payload = models.TextField()
    dataset = models.ForeignKey('Dataset', related_name='unprocessed_transactions_dataset', on_delete=models.PROTECT)
    process_datetime = models.DateTimeField(null=True)

    @property
    def data(self):
        return json.loads(self.payload, object_pairs_hook=OrderedDict)

    def __str__(self):
        return f'UT of {self.dataset.name}'



class BudgetArchive(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    creation_date = models.DateField(auto_now_add=True)
    payload = models.TextField()

    @property
    def data(self):
        return json.loads(self.payload, object_pairs_hook=OrderedDict)

    def __str__(self):
        return str(self.creation_date)

class AmortizeItem(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    expected_lifetime_in_months = models.IntegerField()
    date_of_purchase = models.DateField()
    archived = models.BooleanField(default=False)

    def __str__(self):
        return self.name
