from datetime import date
from decimal import Decimal
from functools import reduce
from django.db import connection
from rest_framework.reverse import reverse
import finances.datechanger as dc
import finances.models as m


def total_balance(start, end, user):
    """
    {
        'data': [
            2938.13,
            2481.62
        ],
        'labels': [
            '2018-01-01',
            '2018-02-01'
        ]
    }
    """
    query = """SELECT STRFTIME('%%Y-%%m', date), ROUND(SUM(amount), 2) AS 'amount'
                FROM (
                    SELECT date, 
                            CASE balance_type WHEN 'liability' THEN ROUND(SUM(amount), 2) * -1 ELSE ROUND(SUM(amount), 2) END AS 'amount', 
                            balance_type
                        FROM finances_balance b
                            JOIN finances_balanceitem bi
                            ON b.item_id = bi.id
                        WHERE b.user_id = %s
                        AND bi.user_id = %s
                        AND b.date >= %s
                        AND b.date < %s
                    GROUP BY date, balance_type)
            GROUP BY date
            ORDER BY date
            """
    with connection.cursor() as cursor:
        cursor.execute(
            query, [user.pk, user.pk, dc.to_sql_date(start), dc.to_sql_date(end)])
        raw_data = cursor.fetchall()
    data = {'data': [], 'labels': []}
    for row in raw_data:
        data['labels'].append(row[0] + "-01")
        data['data'].append(row[1])
    return data


def income_statement(start, end, user):
    query = """SELECT STRFTIME('%%Y-%%m', t.date) AS 'month',
                      ROUND(SUM(CASE WHEN c.is_income = 1 THEN t.amount ELSE 0 END), 2), 
                      ROUND(SUM(CASE WHEN c.is_income = 0 THEN t.amount ELSE 0 END), 2)
                 FROM finances_transaction t 
                        JOIN finances_category c 
                          ON t.category_id = c.id 
                WHERE t.date >= %s 
                  AND t.date < %s
                  AND t.user_id = %s
                  AND c.user_id = %s
             GROUP BY STRFTIME('%%Y-%%m', t.date)
             ORDER BY month
             """
    with connection.cursor() as cursor:
        cursor.execute(query, [dc.to_sql_date(start),
                               dc.to_sql_date(end), user.pk, user.pk])
        raw_data = cursor.fetchall()
    data = []
    for row in raw_data:
        data.append({
            'date': row[0],
            'income': Decimal(row[1]).quantize(Decimal('.01')),
            'expenses': Decimal(row[2]).quantize(Decimal('.01'))
        })
    return data


def transactions(year=date.today().year, month=date.today().month):
    year = int(year)
    month = int(month)
    start = date(year, month, 1)
    end = dc.change_month(start)
    ts = m.Transaction.objects.filter(
        date__gte=start).filter(date__lt=end).order_by('date')
    return ts


def categories():
    category_data = {}
    cs = m.Category.objects.filter(specification_of__isnull=True)
    for category in cs:
        specifications = m.Category.objects.filter(
            specification_of__isnull=False).filter(specification_of=category)
        category_data[category.name] = []
        for specification in specifications:
            category_data[category.name].append(specification.name)
    return category_data


TWO_PLACES = Decimal("0.01")


def roundd(value):
    if value is None:
        return 0
    return Decimal(value).quantize(TWO_PLACES)


def todate(string):
    return date(int(string[0:4]), int(string[5:]), 1)


def expanded_income_statement(start, end, user):
    """
    [
        {
            "date": "2018-12",
            "income": {
                "total": Decimal('2400'),
                "categories": {
                    "work": {
                        "total": Decimal('2400'),
                        "specifications": {
                            "developer": Decimal('2200'),
                            "memereviewer": Decimal('200')
                        }
                    }
                }
            }
            "expenses": ...
            "balance" {
                "transaction_change": Decimal('21'),
                "balance_change": Decimal('20')
            }
        },
        {
            "date": "2018-11",
            ...
        }
    ]
    """
    # The first query gets a table with the sum per month of every main category
    query_sum_by_category_by_month = """
        SELECT STRFTIME('%%Y-%%m', t.date) AS 'month', 
               c.name, 
               c.id,
               c.is_income,
               ROUND(SUM(amount), 2),
               c.ignore_for_transaction_delta
          FROM finances_transaction t
                 JOIN finances_category c
                   ON t.category_id = c.id
         WHERE t.date BETWEEN %s AND %s
           AND t.user_id = %s
           AND c.user_id = %s
      GROUP BY STRFTIME('%%Y-%%m', t.date), c.id
    """
    # The second query gets a table with the sum per month of every specification
    query_sum_by_specification_by_month = """
        SELECT STRFTIME('%%Y-%%m', t.date) AS 'month', 
               c.name, 
               c.id,
               c.is_income,
               c.specification_of_id,
               ROUND(SUM(amount), 2)
          FROM finances_transaction t
                 JOIN finances_category c
                   ON t.specification_id = c.id
         WHERE t.date BETWEEN %s AND %s
           AND t.user_id = %s
           AND c.user_id = %s
      GROUP BY strftime('%%Y-%%m', t.date), c.id
    """

    # the third query gets the total income/expense per month
    query_sum_by_is_income_by_month = """
        SELECT STRFTIME('%%Y-%%m', t.date) AS 'month', 
               c.is_income, 
               ROUND(SUM(amount), 2)
          FROM finances_transaction t
                 JOIN finances_category c
                   ON t.category_id = c.id
         WHERE t.date BETWEEN %s AND %s
           AND t.user_id = %s
           AND c.user_id = %s
      GROUP BY STRFTIME('%%Y-%%m', t.date), c.is_income
    """

    # the fourth query finds the balance per month
    query_balance = """
        SELECT STRFTIME('%%Y-%%m', date), ROUND(SUM(amount), 2) AS 'amount'
          FROM (
               SELECT date, 
                     CASE balance_type WHEN 'asset' THEN ROUND(SUM(amount), 2) WHEN 'liability' THEN ROUND(SUM(amount), 2) * -1 END AS 'amount',
                     balance_type
                 FROM finances_balance b
                     JOIN finances_balanceitem bi
                       ON b.item_id = bi.id
                WHERE b.date BETWEEN %s AND %s
                  AND balance_type <> 'investment'
                  AND b.user_id = %s
                  AND bi.user_id = %s
                GROUP BY date, balance_type)
      GROUP BY date
      ORDER BY date
    """

    # The fifth query finds the liability total
    liability_total = """
        SELECT 
            strftime('%%Y-%%m', date), 
            ROUND(SUM(amount),2) as 'liability_total'
        FROM finances_balance b
            JOIN finances_balanceitem bi
            ON b.item_id = bi.id
        WHERE b.date BETWEEN %s AND %s
        AND balance_type = 'liability'
        AND b.user_id = %s
        AND bi.user_id = %s
        GROUP BY date, balance_type
    """

    # The sixth query finds the totals of each category
    query_total_totals = """
        SELECT c.name,
            ROUND(SUM(amount), 2),
            c.is_income
        FROM finances_transaction t
            JOIN finances_category c
            ON t.category_id = c.id
        WHERE t.date BETWEEN %s AND %s
            AND t.user_id = %s
            AND c.user_id = %s
        GROUP BY c.name
        UNION
        SELECT c.name,
            ROUND(SUM(amount), 2),
            c.is_income
        FROM finances_transaction t
            JOIN finances_category c
            ON t.specification_id = c.id
        WHERE t.date BETWEEN %s AND %s
            AND t.user_id = %s
            AND c.user_id = %s
        GROUP BY c.name
    """

    # execute the queries
    start = dc.to_sql_date(start)
    end = dc.to_sql_date(end)
    with connection.cursor() as cursor:
        cursor.execute(query_sum_by_category_by_month,
                       [start, end, user.pk, user.pk])
        c_data = cursor.fetchall()
        cursor.execute(query_sum_by_specification_by_month,
                       [start, end, user.pk, user.pk])
        s_data = cursor.fetchall()
        cursor.execute(query_sum_by_is_income_by_month,
                       [start, end, user.pk, user.pk])
        t_data = cursor.fetchall()
        cursor.execute(query_balance, [start, end, user.pk, user.pk])
        b_data = cursor.fetchall()
        cursor.execute(liability_total, [start, end, user.pk, user.pk])
        lb_data = cursor.fetchall()
        cursor.execute(query_total_totals, [start, end, user.pk, user.pk,start, end, user.pk, user.pk])
        totals_data = cursor.fetchall()

    data = []
    dates_added = set()
    # Add categories to every date, create date if needed
    for row in c_data:
        if row[0] not in dates_added:
            data.append({
                "date": row[0],
                "income": {
                    "total": Decimal(0),
                    "ignore_for_transaction_delta": roundd("0"),
                    "categories": {}
                },
                "expenses": {
                    "total": Decimal(0),
                    "ignore_for_transaction_delta": roundd("0"),
                    "categories": {}
                },
                'balance': {
                    "transaction_change": None,
                    "balance_change": None,
                    "paid_off_debt": None,
                    "error": None,
                }
            })
            dates_added.add(row[0])
        month = next(item for item in data if item['date'] == row[0])
        month["income" if row[3] else "expenses"]['categories'][row[1]] = {
            "total": roundd(row[4]),
            "id": row[2],
            "specifications": {}
        }
        if row[5]:
            month["income" if row[3] else "expenses"]['ignore_for_transaction_delta'] += roundd(row[4])
    # Add specifications to every category of every date
    for row in s_data:
        month = next(item for item in data if item['date'] == row[0])
        a = month["income" if row[3] else "expenses"]['categories']
        b = [value for key, value in a.items() if value["id"] == row[4]][0]
        b["specifications"][row[1]] = roundd(row[5])
    # Add total amount to every date
    for row in t_data:
        month = next(item for item in data if item['date'] == row[0])
        if row[1]:
            month["income"]["total"] = roundd(row[2])
        else:
            month["expenses"]["total"] = roundd(row[2])
    # Transform data for calculating the balance change
    bal_data = {}
    for row in b_data:
        bal_data[todate(row[0])] = roundd(row[1])
    lb_transformed_data = {}
    for row in lb_data:
        lb_transformed_data[todate(row[0])] = roundd(row[1])

    # Add the diff as calculated by transaction and the balance to every date
    for month in data:
        d = todate(month["date"])
        month["balance"]["transaction_change"] = (month["income"]["total"] - month["income"]["ignore_for_transaction_delta"]) - \
            (month["expenses"]["total"] - month["expenses"]["ignore_for_transaction_delta"])
        try:
            month["balance"]["balance_change"] = bal_data[dc.change_month(d, 1)] - bal_data[d]
        except KeyError:
            month["balance"]["balance_change"] = None
        try:
            month["balance"]["paid_off_debt"] = lb_transformed_data[d] - lb_transformed_data[dc.change_month(d, 1)]
        except KeyError:
            month["balance"]["paid_off_debt"] = None
        try:
            month["balance"]["error"] = month["balance"]["balance_change"] - month["balance"]["transaction_change"] - month["balance"]["paid_off_debt"]
        except TypeError:
            month["balance"]["error"] = None

    # Reformat the totals data
    def summer(a,b):
        if a is None:
            return b
        if b is None:
            return b
        return a + b
    totals = {row[0]: roundd(row[1]) for row in totals_data}
    # Divide by two because this counts both category and specification
    totals['income'] = roundd(reduce(summer,[row[1] for row in totals_data if row[2]], 0) / 2)
    totals['expenses'] = roundd(reduce(summer,[row[1] for row in totals_data if not row[2]], 0) / 2)
    totals['balance_change'] = roundd(reduce(summer, map(lambda month: month['balance']['balance_change'], data), 0))
    totals['transaction_change'] = roundd(reduce(summer, map(lambda month: month['balance']['transaction_change'], data), 0))
    totals['paid_off_debt'] = roundd(reduce(summer, map(lambda month: month['balance']['paid_off_debt'], data), 0))
    totals['error'] = roundd(reduce(summer, map(lambda month: month['balance']['error'], data), 0))

    return data, totals


def budget_report(user, months_back=6, reference_date=date.today()):
    data = []
    reference_date = reference_date.replace(day=1)
    categories = m.Category.objects.filter(specification_of__isnull=True, archived=False, user=user.pk)
    category_query = (
        "select sum(amount)/{}, category_id "
        "from finances_transaction "
        "where date between '{}' and '{}' "
        "and user_id = {} "
        "group by category_id "
    )
    specification_query = (
        "select sum(amount)/{}, specification_id "
        "from finances_transaction "
        "where date between '{}' and '{}' "
        "and user_id = {} "
        "group by specification_id "
    )

    with connection.cursor() as cursor:
        cursor.execute(category_query.format(months_back, dc.change_month(reference_date, months_delta=months_back*-1,
                                                                          output='sql'), dc.change_month(reference_date, months_delta=-1, output='sql', output_day='last'), user.pk))
        raw_data_c = cursor.fetchall()
        cursor.execute(specification_query.format(months_back, dc.change_month(reference_date, months_delta=months_back*-1,
                                                                               output='sql'), dc.change_month(reference_date, months_delta=-1, output='sql', output_day='last'), user.pk))
        raw_data_s = cursor.fetchall()

    average_data = {}
    for r in raw_data_c:
        average_data[r[1]] = r[0]
    for r in raw_data_s:
        average_data[r[1]] = r[0]

    for c in categories:
        spec_data = []
        for s in c.specifications.filter(archived=False):
            try:
                a = average_data[s.id]
            except KeyError:
                a = 0
            spec_data.append(
                {
                    'id': s.id,
                    'name': s.name,
                    'budget': s.budget,
                    'monthly_average': round(a, 2),
                }
            )
        try:
            a = average_data[c.id]
        except KeyError:
            a = 0
        data.append(
            {
                'id': c.id,
                'name': c.name,
                'budget': c.budget,
                'monthly_average': round(a, 2),
                'is_income': c.is_income,
                'specifications': spec_data,
            }
        )
    return data


def new_budget(request, data):
    ba = m.BudgetArchive.objects.create(user=request.user, payload=data)
    for category in data:
        c = m.Category.objects.get(id=category['id'])
        c.budget = category['new_budget']
        c.save()
        for specification in category['specifications']:
            s = m.Category.objects.get(id=specification['id'])
            s.budget = specification['new_budget']
            s.save()
    return reverse('finances:budgetarchive-detail', args=[ba.id], request=request)
