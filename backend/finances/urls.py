from django.urls import include, path
from finances.investments.urls import urlpatterns as investments_paths
from finances.views import (AmortizeItemViewSet, BalanceItemViewSet,
                            BalanceViewSet, BudgetArchiveViewSet,
                            CategoryViewSet, DatasetViewSet, SearchTermViewSet,
                            TransactionViewSet, UnprocessedTransactionViewSet,
                            budget_report, expanded_income_statement,
                            income_statement, new_budget,
                            processing_timestamps, total_balance, upload_file)
from rest_framework import routers

router = routers.DefaultRouter()
router.register('categories', CategoryViewSet, basename='category')
router.register('transactions', TransactionViewSet, basename='transaction')
router.register('datasets', DatasetViewSet, basename='dataset')
router.register('search-terms', SearchTermViewSet, basename='searchterm')
router.register('balance-items', BalanceItemViewSet, basename='balanceitem')
router.register('balances', BalanceViewSet, basename='balance')
router.register('unprocessed-transactions', UnprocessedTransactionViewSet, basename='unprocessedtransaction')
router.register('budgetarchive', BudgetArchiveViewSet, basename='budgetarchive')
router.register('amortize-items', AmortizeItemViewSet, basename='amortizeitem')

app_name = 'finances'
urlpatterns = [
    path('api/new-budget/', new_budget),
    path('api/upload-file/', upload_file),
    path('api/report/income_statement/', income_statement),
    path('api/report/expanded_income_statement/', expanded_income_statement),
    path('api/report/total_balance/', total_balance),
    path('api/report/budget/', budget_report),
    path('api/processing-timestamps/', processing_timestamps),
    path('api/', include(router.urls)),
]
urlpatterns += investments_paths
