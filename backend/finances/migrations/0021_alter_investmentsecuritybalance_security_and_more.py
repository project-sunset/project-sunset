# Generated by Django 5.1.6 on 2025-02-26 08:41

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('finances', '0020_investmentsecurity_account'),
    ]

    operations = [
        migrations.AlterField(
            model_name='investmentsecuritybalance',
            name='security',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='balances', to='finances.investmentsecurity'),
        ),
        migrations.AlterField(
            model_name='investmentsecuritytransaction',
            name='security',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='transactions', to='finances.investmentsecurity'),
        ),
    ]
