# Generated by Django 2.1.1 on 2019-04-02 18:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('finances', '0006_auto_20190331_1952'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='investmenttransaction',
            name='into',
        ),
        migrations.AddField(
            model_name='investmenttransaction',
            name='transaction_type',
            field=models.CharField(choices=[('dividend', 'Dividend'), ('deposit', 'Deposit'), ('withdrawal', 'Withdrawal'), ('purchase', 'Purchase')], default='deposit', max_length=20),
            preserve_default=False,
        ),
    ]
