from django.db import migrations, models
from datetime import datetime

def update_dates(apps, schema_editor):
    transactions = apps.get_model("finances", "transaction")
    for t in transactions.objects.all():
        if t.process_datetime is not None:
            t.temp_process_datetime = datetime.combine(t.process_datetime, datetime.min.time())
            t.save()

class Migration(migrations.Migration):

    dependencies = [
        ('finances', '0008_merge_20190406_2041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='investmenttransaction',
            name='transaction_type',
            field=models.CharField(choices=[('dividend', 'Dividend'), ('deposit', 'Deposit'), ('withdrawal', 'Withdrawal'), ('purchase', 'Purchase'), ('sale', 'Sale'), ('interest', 'Interest')], max_length=20),
        ),
        migrations.AddField(
            model_name='transaction',
            name='temp_process_datetime',
            field=models.DateTimeField(null=True),
        ),
        migrations.RunPython(update_dates),
        migrations.RemoveField(
            model_name='transaction',
            name='process_datetime'
        ),
        migrations.RenameField(
            model_name='transaction',
            old_name='temp_process_datetime',
            new_name='process_datetime'
        ),
    ]
