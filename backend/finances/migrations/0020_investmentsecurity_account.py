# Generated by Django 5.0.6 on 2024-06-13 11:32

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('finances', '0019_investmentsecuritytransaction_service_cost_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='investmentsecurity',
            name='account',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='finances.investmentaccount'),
            preserve_default=False,
        ),
    ]
