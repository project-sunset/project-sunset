from django.contrib import admin
from finances.models import (Category, Transaction, Balance, BalanceItem, SearchTerm,
                             Dataset, UnprocessedTransaction)
from finances.investments.admin import (InvestmentAccountAdmin, InvestmentAccountTransactionAdmin, InvestmentSecurityAdmin, InvestmentSecurityBalanceAdmin, InvestmentSecurityTransactionAdmin)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'user', 'specification_of')


admin.site.register(Transaction)
admin.site.register(Balance)
admin.site.register(BalanceItem)
admin.site.register(SearchTerm)
admin.site.register(Dataset)
admin.site.register(UnprocessedTransaction)
