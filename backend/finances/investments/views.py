from io import TextIOWrapper

from django.db.models import Sum
from django.http import HttpResponse, JsonResponse
from finances.investments.models import (InvestmentAccount, InvestmentAccountTransaction, InvestmentSecurity,
                                         InvestmentSecurityBalance,
                                         InvestmentSecurityTransaction)
from finances.investments.parse_file import (UnknownSecurityException,
                                             parse_investments)
from finances.investments.serializers import (
    InvestmentAccountSerializer, InvestmentAccountTransactionSerializer, InvestmentSecurityBalanceSerializer,
    InvestmentSecuritySerializer, InvestmentSecurityTransactionSerializer)
from rest_framework import viewsets
from rest_framework.decorators import action, permission_classes
from rest_framework.permissions import IsAuthenticated

from backend.permissions import IsOwner


class InvestmentAccountViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = InvestmentAccountSerializer

    def get_queryset(self):
        return InvestmentAccount.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @action(methods=['get'], detail=True)
    def stats(self, _, pk):
        account = InvestmentAccount.objects.get(user=self.request.user, pk=pk)
        transactions = InvestmentAccountTransaction.objects.filter(user=self.request.user, account=account).values('transaction_type').annotate(total=Sum('amount'))
        return JsonResponse({
            'sums': list(transactions)
        })
    
    @action(methods=['get'], detail=True)
    def transactions(self, _, pk):
        account = InvestmentAccount.objects.get(user=self.request.user, pk=pk)
        transactions = InvestmentAccountTransaction.objects.filter(user=self.request.user, account=account).order_by('-date')
        return JsonResponse(InvestmentAccountTransactionSerializer(transactions, many=True).data, safe=False)


class InvestmentSecurityViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = InvestmentSecuritySerializer
    filter_fields = ('account',)
    pagination_class = None

    def get_queryset(self):
        return InvestmentSecurity.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @action(methods=['get'], detail=True)
    def stats(self, _, pk):
        security = InvestmentSecurity.objects.get(user=self.request.user, pk=pk)
        base = InvestmentSecurityTransaction.objects.filter(user=self.request.user, security=security)
        transactions = base.values('transaction_type').annotate(total=Sum('amount'))
        tax = base.aggregate(total=Sum('tax'))
        service_costs = base.aggregate(total=Sum('service_cost'))
        try:
            balance_obj = InvestmentSecurityBalance.objects.filter(user=self.request.user, security=security).latest('date')
            balance = InvestmentSecurityBalanceSerializer(balance_obj).data
        except InvestmentSecurityBalance.DoesNotExist:
            balance = None
        return JsonResponse({
            'sums': list(transactions),
            'tax': tax['total'],
            'service_costs': service_costs['total'],
            'balance': balance
        })


class InvestmentSecurityTransactionViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = InvestmentSecurityTransactionSerializer
    filter_fields = ('transaction_type', 'date', 'amount',
                     'security', 'process_datetime')
    pagination_class = None

    def get_queryset(self):
        return InvestmentSecurityTransaction.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class InvestmentSecurityBalanceViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = InvestmentSecurityBalanceSerializer
    filter_fields = ('security',)

    def get_queryset(self):
        return InvestmentSecurityBalance.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        existing = InvestmentSecurityBalance.objects.filter(user=self.request.user, date=serializer.validated_data['date'], security=serializer.validated_data['security']).first()
        if existing is not None:
            existing.amount = serializer.validated_data['amount']
            existing.save()
        else:
            serializer.save(user=self.request.user)
        

@permission_classes([IsAuthenticated])
def upload_investment_file(request):
    f = TextIOWrapper(request.FILES['files'].file,
                      encoding='utf-16', errors='replace')
    try:
        parse_investments(
            f, InvestmentAccount.objects.get(pk=request.POST['account']), request.user)
        return HttpResponse(status=204)
    except UnknownSecurityException as ex:
        return JsonResponse({'error_type': 'unknown_security', 'unknowns': list(ex.security_name)}, status=400)
    
        