from decimal import Decimal

from django.contrib.auth.models import User
from django.db import models
from django.db.models import OuterRef, Q, Subquery, Sum


class InvestmentAccount(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
    
    @property
    def stats(self):
        latest_balance = Subquery(InvestmentSecurityBalance.objects.filter(security=OuterRef('id')).order_by('-date').values('amount'))
        current_balance = InvestmentSecurity.objects.filter(user=self.user, account=self).annotate(latest_balance=latest_balance).aggregate(total=Sum('latest_balance', default=Decimal(0)))['total']
        total_input = InvestmentAccountTransaction.objects.filter(user=self.user, transaction_type='deposit').aggregate(total=Sum('amount', default=Decimal(0)))['total']
        total_output = InvestmentAccountTransaction.objects.filter(user=self.user, transaction_type='withdrawal').aggregate(total=Sum('amount', default=Decimal(0)))['total']
        if total_input == Decimal(0):
            return {
            'current_balance': current_balance,
            'total_input': total_input,
            'total_output': total_output,
            'return': Decimal(0)
        }
        return {
            'current_balance': current_balance,
            'total_input': total_input,
            'total_output': total_output,
            'return': round(((total_output + current_balance) / total_input) * 100) - 100
        }

class InvestmentAccountTransaction(models.Model):
    TYPES = (
        ('deposit', 'Deposit'),
        ('withdrawal', 'Withdrawal'),
        ('interest', 'Interest'),
        ('costs', 'Costs')
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField()
    amount = models.DecimalField(max_digits=9, decimal_places=2)
    account = models.ForeignKey(InvestmentAccount, on_delete=models.CASCADE)
    transaction_type = models.CharField(max_length=20, choices=TYPES)
    process_datetime = models.DateTimeField(null=True)

    def __str__(self):
        return str(self.date)

class InvestmentSecurity(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    alternative_names = models.TextField(blank=True)
    account = models.ForeignKey(InvestmentAccount, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
    
    @property
    def stats(self):
        try:
            current_balance = InvestmentSecurityBalance.objects.filter(user=self.user, security=self).latest('date')
            current_balance_amount = current_balance.amount
            current_balance_date = current_balance.date
        except InvestmentSecurityBalance.DoesNotExist:
            current_balance_amount = Decimal(0)
            current_balance_date = None
        total_input_purchase = InvestmentSecurityTransaction.objects.filter(Q(transaction_type='purchase'), user=self.user, security=self).aggregate(total=Sum('amount', default=Decimal(0)))['total']
        total_service_cost = InvestmentSecurityTransaction.objects.filter(user=self.user, security=self).aggregate(total=Sum('service_cost', default=Decimal(0)))['total']
        total_input = total_input_purchase + total_service_cost
        total_output = InvestmentSecurityTransaction.objects.filter(Q(transaction_type='sale') | Q(transaction_type='dividend') | Q(transaction_type='product-conversion'), user=self.user, security=self).aggregate(total=Sum('amount', default=Decimal(0)))['total']
        if total_input == Decimal(0):
            return {
                'current_balance': current_balance_amount,
                'current_balance_date': current_balance_date,
                'total_input': total_input,
                'total_output': total_output,
                'return': Decimal(0)
            }
        return {
            'current_balance': current_balance_amount,
            'current_balance_date': current_balance_date,
            'total_input': total_input,
            'total_output': total_output,
            'return': round(((total_output + current_balance_amount) / total_input) * 100) - 100
        }

def zero_default():
    return Decimal(0)

class InvestmentSecurityTransaction(models.Model):
    TYPES = (
        ('dividend', 'Dividend'),
        ('purchase', 'Purchase'),
        ('sale', 'Sale'),
        ('product-conversion', 'Product Conversion')
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField()
    amount = models.DecimalField(max_digits=9, decimal_places=2)
    tax = models.DecimalField(max_digits=9, decimal_places=2, default=zero_default)
    service_cost = models.DecimalField(max_digits=9, decimal_places=2, default=zero_default)
    security = models.ForeignKey(InvestmentSecurity, on_delete=models.CASCADE, related_name='transactions')
    transaction_type = models.CharField(max_length=20, choices=TYPES)
    process_datetime = models.DateTimeField(null=True)

    def __str__(self):
        return str(self.date)

class InvestmentSecurityBalance(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField()
    security = models.ForeignKey(InvestmentSecurity, on_delete=models.CASCADE, related_name='balances')
    amount = models.DecimalField(max_digits=9, decimal_places=2)

    def __str__(self):
        return str(self.date)