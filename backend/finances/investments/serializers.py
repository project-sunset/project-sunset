from finances.investments.models import (InvestmentAccount, InvestmentAccountTransaction, InvestmentSecurity,
                                         InvestmentSecurityBalance,
                                         InvestmentSecurityTransaction)
from rest_framework import serializers


class InvestmentSecurityTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvestmentSecurityTransaction
        fields = ('id', 'date', 'amount', 'transaction_type', 'security', 'process_datetime', 'tax', 'service_cost')

class InvestmentAccountTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvestmentAccountTransaction
        fields = ('id', 'date', 'amount', 'transaction_type', 'account', 'process_datetime')

class InvestmentAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvestmentAccount
        fields = ('id', 'name', 'stats')
        read_only_fields = ['stats']

class InvestmentSecuritySerializer(serializers.ModelSerializer):
    class Meta:
        model = InvestmentSecurity
        fields = ('id', 'name', 'alternative_names', 'stats', "account")
        read_only_fields = ['stats']

class InvestmentSecurityBalanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvestmentSecurityBalance
        fields = ('id', 'amount', 'date', 'security')