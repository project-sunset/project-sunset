from django.urls import include, path
from finances.investments.views import (InvestmentAccountViewSet,
                                        InvestmentSecurityBalanceViewSet,
                                        InvestmentSecurityTransactionViewSet,
                                        InvestmentSecurityViewSet,
                                        upload_investment_file)
from rest_framework import routers

router = routers.DefaultRouter()
router.register('transactions', InvestmentSecurityTransactionViewSet, basename='investments-transaction')
router.register('accounts', InvestmentAccountViewSet, basename="investments-account")
router.register('securities', InvestmentSecurityViewSet, basename='investments-security')
router.register('balances', InvestmentSecurityBalanceViewSet, basename='investments-security-balance')

urlpatterns = [
    path('api/investments/upload-file/', upload_investment_file),
    path('api/investments/', include(router.urls))
]