import csv
from finances.investments.models import InvestmentSecurity, InvestmentSecurityTransaction, InvestmentAccountTransaction
from django.utils import timezone
from datetime import datetime
from decimal import Decimal
from django.db import transaction
import re

class UnknownSecurityException(Exception):
    def __init__(self, security_name):
        super().__init__()
        self.security_name = security_name

@transaction.atomic
def parse_investments(f, account, user):
    process_datetime = timezone.now()
    reader = csv.DictReader(f, delimiter='\t')
    unknown_securities = set()
    for row in reader:
        try:
            _parse_row(row, user, process_datetime, account)
        except UnknownSecurityException as ex:
            unknown_securities.add(ex.security_name)
    if len(unknown_securities) > 0:
        raise UnknownSecurityException(unknown_securities)

_EFFECTEN_KOOP = re.compile(
    r"^Effecten koop : (?P<amount>\d+(\.\d+)?) (?P<item>.+) (?P<price>\d+(\.\d+)?) (?P<currency>EUR|USD)$"
)

_EFFECTEN_VERKOOP = re.compile(
    r"^Effecten verkoop : (?P<amount>\d+(\.\d+)?) (?P<item>.+) (?P<price>\d+(\.\d+)?) (?P<currency>EUR|USD)$"
)

_INTERIM_DIVIDEND = re.compile(
    r"^Effecten verkoop : (?P<amount>\d+(\.\d+)?) (?P<item>.+) (Stock|Stocks|Rights|stock|stocks) ((\d{2}-\d{2}-\d{4})|22) (?P<price>\d+(\.\d+)?) (?P<currency>EUR)( CA: Toekennen/Aan-Verkoop/Omwisseling Claims/Stock)?$"
)

_PRODUCT_CONVERSIE = re.compile(
    r"^Effecten verkoop : (?P<amount>\d+(\.\d+)?) (?P<item>.+) -FRAC- (?P<price>\d+(\.\d+)?) (?P<currency>EUR) CA: Productconversie$"
)

_DIVIDEND = re.compile(
    r"^Uitkeren : (?P<amount>\d+(\.\d+)?) (?P<item>.+) (?P<price>\d+(\.\d+)?) (?P<currency>EUR|USD)( CA: Toekennen/Uitkeren coupon/ Dividenden)?$"
)

def _find_security(user, item: str):
    try:
        return InvestmentSecurity.objects.get(user=user, name=item)
    except InvestmentSecurity.DoesNotExist:
        try:
            return InvestmentSecurity.objects.get(user=user, alternative_names__contains=f'|{item}|')
        except InvestmentSecurity.DoesNotExist:
            raise UnknownSecurityException(item)

def _parse_row(row, user, process_datetime, account):
    common_args = {
        'date': datetime.strptime(row["Datum"], '%d-%m-%Y'),
        'amount': abs(Decimal(row["Bedrag"].replace(",", "."))),
        'process_datetime': process_datetime,
        'user': user
    }
    tax = Decimal(row["Belasting"].replace(",", "."))
    service_costs = Decimal(row["ING kosten"].replace(",", "."))
    if _INTERIM_DIVIDEND.match(row["Omschrijving"]) is not None:
        match = _INTERIM_DIVIDEND.match(row["Omschrijving"])
        security = _find_security(user, match.group("item"))
        InvestmentSecurityTransaction.objects.create(
            transaction_type='dividend',
            security=security,
            **common_args
        )
    elif "CA: Productconversie" in row["Omschrijving"]:
        match = _PRODUCT_CONVERSIE.match(row["Omschrijving"])
        security = _find_security(user, match.group("item"))
        InvestmentSecurityTransaction.objects.create(
            transaction_type='product-conversion',
            security=security,
            **common_args
        )
    elif row["Omschrijving"].startswith("Effecten koop : "):
        match = _EFFECTEN_KOOP.match(row["Omschrijving"])
        if match is None:
            raise Exception(row["Omschrijving"])
        security = _find_security(user, match.group("item"))
        InvestmentSecurityTransaction.objects.create(
            transaction_type='purchase',
            security=security,
            service_cost=service_costs,
            **{**common_args, "amount": common_args['amount'] - service_costs},
        )
    elif row["Omschrijving"].startswith("Effecten verkoop : "):
        match = _EFFECTEN_VERKOOP.match(row["Omschrijving"])
        if match is None:
            raise Exception(row["Omschrijving"])
        security = _find_security(user, match.group("item"))
        InvestmentSecurityTransaction.objects.create(
            transaction_type='sale',
            security=security,
            service_cost=service_costs,
            **{**common_args, "amount": common_args['amount'] + service_costs},
        )
    elif row["Omschrijving"].startswith("Uitkeren : "):
        match = _DIVIDEND.match(row["Omschrijving"])
        if match is None:
            raise Exception(row["Omschrijving"])
        security = _find_security(user, match.group("item"))
        InvestmentSecurityTransaction.objects.create(
            transaction_type='dividend',
            security=security,
            tax=tax,
            **{**common_args, "amount": common_args['amount'] + tax},
        )
    elif row["Omschrijving"].startswith("Basisfee en/of Servicefee"):
        InvestmentAccountTransaction.objects.create(
            account=account,
            transaction_type='costs',
            **common_args
        )
    elif row["Omschrijving"].startswith("Kosten beleggen"):
        pass
        # Booking for costs to investment account.
    elif (
        row["Omschrijving"].startswith("Overschrijving geld")
        or row["Omschrijving"].startswith("Overboeking geld")
        or row["Omschrijving"].startswith("Overschrijving beleggingsrekening")
    ):
        InvestmentAccountTransaction.objects.create(
            account=account,
            transaction_type='deposit',
            **common_args
        )
    elif row["Omschrijving"].startswith("Maandrente"):
        InvestmentAccountTransaction.objects.create(
            account=account,
            transaction_type='interest',
            **common_args
        )
    elif row["Omschrijving"] == "Correction":
        return
    elif row["Omschrijving"] == "belegging overschot":
        InvestmentAccountTransaction.objects.create(
            account=account,
            transaction_type='withdrawal',
            **common_args
        )
    else:
        raise Exception(row["Omschrijving"])