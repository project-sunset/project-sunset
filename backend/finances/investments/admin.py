from django.contrib import admin
from finances.investments.models import (InvestmentAccount, InvestmentSecurity, InvestmentSecurityBalance, InvestmentSecurityTransaction, InvestmentAccountTransaction)

@admin.register(InvestmentAccount)
class InvestmentAccountAdmin(admin.ModelAdmin):
    pass

@admin.register(InvestmentSecurity)
class InvestmentSecurityAdmin(admin.ModelAdmin):
    pass

@admin.register(InvestmentSecurityBalance)
class InvestmentSecurityBalanceAdmin(admin.ModelAdmin):
    pass

@admin.register(InvestmentSecurityTransaction)
class InvestmentSecurityTransactionAdmin(admin.ModelAdmin):
    pass

@admin.register(InvestmentAccountTransaction)
class InvestmentAccountTransactionAdmin(admin.ModelAdmin):
    pass