import csv
import re
import json
import finances.models as m
from django.utils import timezone
from datetime import datetime
from decimal import Decimal
from django.core.serializers.json import DjangoJSONEncoder
from django.db import transaction


@transaction.atomic
def parse(f, dataset, user):
    # set process date
    process_datetime = timezone.now()
    date_format = dataset.date_format

    # get relevant search terms
    search_fields = (m.SearchTerm.objects
                     .filter(dataset=dataset, user=user)
                     .values('field')
                     .distinct())
    searches = {}
    for field in search_fields:
        searches[field['field']] = m.SearchTerm.objects.filter(
            dataset=dataset,
            user=user,
            field=field['field'])
    # Resulting variable: searches = {'Description': <Queryset>, 'Comments': <Queryset>}

    # open the file
    # for every row check in every field if a search term is a match
    r = csv.DictReader(f, delimiter=";")
    count = 0
    hit_count = 0
    for c, row in enumerate(r):
        row[dataset.date_field] = datetime.strptime(
            row[dataset.date_field], date_format).date()
        row[dataset.amount_field] = Decimal(
            row[dataset.amount_field].replace(',', '.'))
        for field in searches:
            for term in searches[field]:
                found = re.search(term.term.lower(), row[field].lower())
                if found is not None:
                    ins = m.Transaction(
                        date=row[dataset.date_field],
                        amount=row[dataset.amount_field],
                        category=term.category,
                        specification=term.specification,
                        payload=json.dumps(row, cls=DjangoJSONEncoder),
                        process_datetime=process_datetime,
                        found_by=term,
                        user=user)
                    ins.save()
                    term.hits += 1
                    hit_count += 1
                    # break out of searches in this field if we have found it
                    break
            else:
                # if we exit this loop without finding something, we need to do the next field
                continue
            # break out of searches for other fields if we have found it
            break
        else:
            m.UnprocessedTransaction(
                payload=json.dumps(row, cls=DjangoJSONEncoder),
                dataset=dataset,
                process_datetime=process_datetime,
                user=user).save()
    else:
        count = c + 1

    # save updated hits counters
    for field in searches:
        for term in searches[field]:
            term.save()
    dataset.processed_transactions += count
    dataset.save()
    return (hit_count, count)
