from collections import defaultdict
import datetime
from io import TextIOWrapper

import finances.database as db
import finances.datechanger as dc
import finances.parse_file as parse_file
from django.core.serializers.json import DjangoJSONEncoder
from django.db import transaction
from django.db.models import Count, Sum
from django.http import JsonResponse
from finances.investments.models import InvestmentAccountTransaction, InvestmentSecurityTransaction
from finances.models import (AmortizeItem, Balance, BalanceItem, BudgetArchive,
                             Category, Dataset, SearchTerm, Transaction,
                             UnprocessedTransaction)
from finances.serializers import (AmortizeItemSerializer,
                                  BalanceItemSerializer, BalanceSerializer,
                                  BudgetArchiveSerializer, CategorySerializer,
                                  DatasetSerializer, SearchTermSerializer,
                                  TransactionSerializer,
                                  UnprocessedTransactionSerializer)
from rest_framework import status, viewsets
from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from backend.permissions import IsOwner


@permission_classes([IsAuthenticated])
def upload_file(request):
    f = TextIOWrapper(request.FILES['files'].file,
                      encoding='utf-8', errors='replace')
    hit_count, count = parse_file.parse(f, Dataset.objects.get(
        pk=request.POST['dataset'], user=request.user), request.user)
    return JsonResponse({'count': count, 'hits': hit_count, 'percentage': hit_count/count*100})


@api_view(['post'])
@permission_classes([IsAuthenticated])
@transaction.atomic
def new_budget(request):
    budget_url = db.new_budget(request, request.data)
    return JsonResponse({'url': budget_url})


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def income_statement(request):
    start = request.query_params.get(
        'start', dc.change_month(datetime.date.today(), -3))
    end = request.query_params.get(
        'end', dc.change_month(datetime.date.today(), 0))
    data = db.income_statement(start, end, request.user)
    return JsonResponse({'data': data})


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def expanded_income_statement(request):
    start = request.query_params.get(
        'start', dc.change_month(datetime.date.today(), -12))
    end = request.query_params.get(
        'end', dc.change_month(datetime.date.today(), 0))
    data, totals = db.expanded_income_statement(start, end, request.user)
    return JsonResponse({'data': data, 'totals': totals})


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def total_balance(request):
    start = request.query_params.get(
        'start', dc.change_month(datetime.date.today(), -12))
    end = request.query_params.get(
        'end', dc.change_month(datetime.date.today(), 0))
    return JsonResponse({'data': db.total_balance(start, end, request.user)})


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def budget_report(request):
    return JsonResponse({'data': db.budget_report(request.user)})

def combiner(list1, list2):
    combined_data = defaultdict(int)
    for item in list1:
        combined_data[item['process_datetime']] += item['transactions']
    for item in list2:
        combined_data[item['process_datetime']] += item['transactions']
    return [{'process_datetime': dt, 'transactions': count} for dt, count in combined_data.items()]

@api_view(['GET', 'DELETE'])
@permission_classes([IsAuthenticated])
def processing_timestamps(request):
    if request.method == 'GET':
        transactions = Transaction.objects.filter(user=request.user).values('process_datetime').annotate(
            transactions=Count("id")).order_by()
        investment_transactions = InvestmentSecurityTransaction.objects.filter(user=request.user).values('process_datetime').annotate(
            transactions=Count("id")).order_by()
        investment_transactions_2 = InvestmentAccountTransaction.objects.filter(user=request.user).values('process_datetime').annotate(
            transactions=Count("id")).order_by()
        return JsonResponse({'transactions': list(transactions), 'investment_transactions': combiner(investment_transactions, investment_transactions_2)}, encoder=ProcessingJSONEncoder)
    elif request.method == 'DELETE':
        category = request.query_params.get('category')
        timestamp = request.query_params.get('datetime')
        if category == 'transaction':
            deleted_count = Transaction.objects.filter(
                user=request.user, process_datetime=timestamp).delete()
            UnprocessedTransaction.objects.filter(
                user=request.user, process_datetime=timestamp).delete()
        elif category == 'investment-transaction':
            deleted_count = InvestmentSecurityTransaction.objects.filter(
                user=request.user, process_datetime=timestamp).delete()
            deleted_count += InvestmentAccountTransaction.objects.filter(
                user=request.user, process_datetime=timestamp).delete()
        return JsonResponse({'deleted_count': deleted_count})


class ProcessingJSONEncoder(DjangoJSONEncoder):
    def default(self, o):
        # We need to have the full datetime string to be able to find them back
        # DjangoJSONEncoder strips the microseconds to milliseconds.
        if isinstance(o, datetime.datetime):
            r = o.isoformat()
            if r.endswith('+00:00'):
                r = r[:-6] + 'Z'
            return r
        else:
            return super().default(o)


class CategoryViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = CategorySerializer
    pagination_class = None
    filter_fields = ('name', 'description', 'is_income', 'specification_of')

    def get_queryset(self):
        return Category.objects.filter(user=self.request.user, archived=False)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class TransactionViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = TransactionSerializer
    filter_fields = ('date', 'amount', 'category', 'specification',
                     'payload', 'process_datetime', 'found_by')

    def get_queryset(self):
        return Transaction.objects.filter(user=self.request.user)

    @transaction.atomic
    def perform_create(self, serializer):
        up = self.request.query_params.get('unprocessedtransaction', False)
        category = Category.objects.get(specifications=serializer.validated_data['specification'])
        if up:
            upt = UnprocessedTransaction.objects.get(
                pk=up, user=self.request.user)
            payload = upt.payload
            process_datetime = upt.process_datetime
            serializer.save(user=self.request.user, payload=payload,
                            process_datetime=process_datetime, category_id=category.id)
        else:
            serializer.save(user=self.request.user, category_id=category.id)
    
    @action(detail=False, methods=['get'])
    def sum(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        return JsonResponse({'sum': round(queryset.aggregate(Sum("amount"))['amount__sum'], 2)})



class DatasetViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = DatasetSerializer
    pagination_class = None

    def get_queryset(self):
        return Dataset.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class SearchTermViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = SearchTermSerializer
    filter_fields = ('dataset',)
    pagination_class = None

    def get_queryset(self):
        return SearchTerm.objects.filter(user=self.request.user, archived=False)

    def perform_create(self, serializer):
        category = Category.objects.get(specifications=serializer.validated_data['specification'])
        serializer.save(user=self.request.user, category_id=category.id)


class BalanceItemViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = BalanceItemSerializer
    filter_fields = ('date', 'balance_type')
    pagination_class = None

    def get_queryset(self):
        return BalanceItem.objects.filter(user=self.request.user, archived=False)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BalanceViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = BalanceSerializer
    filter_fields = ('date',)
    pagination_class = None

    @action(detail=False, methods=['put'])
    def update_list(self, request):
        instances = Balance.objects.filter(
            user=self.request.user, date=request.data[0]['date'])
        serializer = self.get_serializer(
            instance=instances, data=request.data, many=isinstance(request.data, list))
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_queryset(self):
        return Balance.objects.filter(user=self.request.user, item__archived=False)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(
            data=request.data, many=isinstance(request.data, list))
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class UnprocessedTransactionViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = UnprocessedTransactionSerializer
    filter_fields = ('process_datetime',)

    def get_queryset(self):
        return UnprocessedTransaction.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class BudgetArchiveViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = BudgetArchiveSerializer

    def get_queryset(self):
        return BudgetArchive.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class AmortizeItemViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = AmortizeItemSerializer

    def get_queryset(self):
        return AmortizeItem.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)