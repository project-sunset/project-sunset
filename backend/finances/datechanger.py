"""
The DateChanger module is used to deal with changing reoccuring changing of date and datetimes and return types (datetime objects or sql strings).
"""
from datetime import date, timedelta, datetime


"""Return a new date in which months have been subtracted or added, return day will always be 1."""
def delta_month(input_date, delta_months):
    output_year = input_date.year + (input_date.month - 1 + delta_months) // 12
    output_month = (input_date.month-1 + delta_months) % 12 + 1
    return date(output_year, output_month, 1)

"""Return the date but than as the last day of that month."""
def last_day_of_month(input_date):
    output_date = delta_month(input_date, 1)
    output_date = output_date - timedelta(days=1)
    return output_date

"""Turn a date object to a datetime object"""
def toggle_date_datetime(input_date):
    if type(input_date) is date:
        output_date = datetime(input_date.year, input_date.month, input_date.day, 0, 0, 0)
    elif type(input_date) is datetime:
        output_date = date(input_date.year, input_date.month, input_date.day)
    return output_date

"""Turn a date or datetime object in to the appropriate string representation for SQL."""
def to_sql_date(input_date):
    if type(input_date) is date:
        return input_date.strftime('%Y-%m-%d')
    elif type(input_date) is datetime:
        return input_date.strftime('%Y-%m-%d %H:%M:%S')
    elif type(input_date) is str:
        return input_date

"""Change a date into the requested date and return type."""
def change_month(input_date=date.today(), months_delta=1, output='object', output_day='first'):
    output_date = delta_month(input_date, months_delta)
    if output_day == 'last':
        output_date = last_day_of_month(output_date)
    if output == 'object':
        return output_date
    elif output == 'sql':
        return to_sql_date(output_date)