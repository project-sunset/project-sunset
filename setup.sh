#!/bin/bash
set -e

git submodule update

python3.12 -m venv .venv # if this fails maybe you need to install python3-pip or python3.12-venv
source .venv/bin/activate

cd frontend
npm install

cd ../tests
npm install

cd ../backend
pip3 install -r requirements.txt
./refresh_database.sh