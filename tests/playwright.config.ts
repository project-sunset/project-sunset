import { defineConfig, devices } from '@playwright/test'

/**
 * See https://playwright.dev/docs/test-configuration.
 */
const WEB_SERVER = [{
  command: './startFrontend.sh',
  url: 'http://localhost:8080',
  reuseExistingServer: false,
  stdout: "pipe",
}, {
  command: './startBackend.sh',
  url: 'http://localhost:8000',
  reuseExistingServer: false,
  stdout: "pipe",
}]

export default defineConfig({
  testDir: './e2e',
  fullyParallel: false,
  forbidOnly: !!process.env.CI,
  retries: 0,
  workers: 1,
  reporter: process.env.CI ? 'dot' : 'html',
  use: {
    trace: 'retain-on-first-failure',
    screenshot: {
      mode: 'only-on-failure',
      fullPage: true
    }
  },
  projects: [
    {
      name: 'various',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /(countdown|rhythm|sudoku|drinks|memory).spec.ts/
    },
    {
      name: 'overtime-tracker',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /overtime_tracker.spec.ts/
    },
    {
      name: 'links',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /links.spec.ts/
    },
    {
      name: 'account',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /account.spec.ts/
    },
    {
      name: 'groceries',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /groceries.spec.ts/
    },
    {
      name: 'quiz',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /quiz.spec.ts/
    },
    {
      name: 'finances',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /finances.spec.ts/
    },
    {
      name: 'smoke-test',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /smoke.spec.ts/
    },
  ],
  // @ts-ignore, the webserver type is not exported
  webServer: process.env.PW_WEBSERVER ? undefined : WEB_SERVER
})
