import { test, expect } from '@playwright/test'
import { failOnJsError, saveCoverage } from './utils'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('Rhythm tapper', async ({ page }) => {
  await page.goto('http://localhost:8080/#/')
  await page.getByRole('link', { name: 'Rhythm Tapper' }).click()
  await expect(page.getByRole('heading', { name: 'Rhythm Tapper' })).toBeVisible()
  await expect(page.getByLabel('Tempo')).toHaveValue('120')
  await page.getByRole('button', { name: 'Tik tempo' }).click()
  await page.waitForTimeout(500)
  await page.getByRole('button', { name: 'Tik tempo' }).click()
  await page.getByRole('button', { name: 'Start metronoom' }).click()
  await page.waitForTimeout(1000)
  await page.getByRole('button', { name: 'Stop metronoom' }).click()
  await page.getByRole('button', { name: 'Tik ritme' }).click()
  await page.waitForTimeout(1000)
  await page.getByRole('button', { name: 'Tik ritme' }).click()
  await expect(page.locator('#notes')).toBeVisible()
})