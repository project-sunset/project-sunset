import { test, expect } from '@playwright/test'
import { failOnJsError, saveCoverage } from './utils'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('Solve', async ({ page }) => {
  await page.goto('http://localhost:8080/#/')
  await page.getByRole('link', { name: 'Sudoku Solver' }).click()
  await page.locator('.sudoku-wrapper').press('Alt+b')
  await page.locator('#presets').selectOption('wikipedia')
  await page.getByRole('button', { name: 'Solve' }).click()
  await expect(page.getByRole('heading', { name: 'Solution' })).toBeVisible()
})

test('Hint', async ({ page }) => {
  await page.goto('http://localhost:8080/#/')
  await page.getByRole('link', { name: 'Sudoku Solver' }).click()
  await page.locator('.sudoku-wrapper').press('Alt+b')
  await page.locator('#presets').selectOption('wikipedia')
  await page.getByRole('button', { name: 'Hint' }).click()
  await expect(page.locator('div').filter({ hasText: /^1$/ }).first()).toBeVisible()
  await page.locator('div').filter({ hasText: /^1$/ }).first().click()
  await expect(page.getByRole('heading', { name: 'Analyse van cel F1' })).toBeVisible()
})