import { test, expect } from '@playwright/test'
import { loginKara, saveCoverage, failOnJsError } from './utils'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('Account', async ({ page }) => {
  await loginKara(page)
  await expect(page.getByRole('heading', { name: 'Welcome Kara!' })).toBeVisible()
  await page.getByRole('link', { name: 'Account' }).click()
  // Change password
  await page.getByRole('heading', { name: 'Change password' }).click()
  await page.getByLabel('Enter your current password').fill('supergirl01')
  await page.getByLabel('Enter a new password').fill('password01!')
  await page.getByLabel('New password again').fill('password01!')
  await page.getByRole('button', { name: 'Submit' }).click()
  // Change email
  await page.getByRole('heading', { name: 'Change email' }).click()
  await page.getByLabel('Enter a new email').fill('test')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByText('The new email seems not valid')).toBeVisible()
  await page.getByLabel('Enter a new email').fill('test@example.com')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.locator('#app')).toContainText('Email: test@example.com')
  // Change username
  await page.getByRole('heading', { name: 'Change username' }).click()
  await page.getByLabel('Enter a new username').fill('SuperGirl')
  await page.locator('#submitusernamechange').click()
  await expect(page.locator('#app')).toContainText('Username: SuperGirl')
  // Logout
  await page.getByRole('link', { name: 'Back' }).click()
  await page.getByRole('button', { name: 'Logout' }).click()
  await expect(page.getByRole('heading', { name: 'Login/Register' })).toBeVisible()
})