import { test, expect } from '@playwright/test'
import { loginKara, saveCoverage, failOnJsError, loginBarry } from './utils'
import { join } from 'path'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('CRUD categories', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Finances' }).click()
  await page.getByRole('link', { name: 'Categorieën' }).click()
  // Create new category
  await page.getByTestId('add_category_income').click()
  await page.getByLabel('Name').fill('Government')
  await page.getByLabel('Budget').fill('60')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByText('Government')).toBeVisible()
  // Create new specification
  await page.getByTestId('show_Government').click()
  await page.getByRole('button', { name: 'Add specification' }).click()
  await page.getByLabel('Name').fill('Healthcare benefit')
  await page.getByLabel('Budget').fill('60')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'Healthcare benefit' })).toBeVisible()
  // Update category
  await page.getByText('Government').click()
  await page.getByLabel('Name').fill('Government2')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByText('Government2')).toBeVisible()
  // Update specification
  await page.getByRole('cell', { name: 'Healthcare benefit' }).click()
  await page.getByLabel('Name').click()
  await page.getByLabel('Name').fill('Healthcare benefit2')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'Healthcare benefit2' })).toBeVisible()
  // Delete specification
  await page.getByRole('cell', { name: 'Healthcare benefit2' }).click()
  await page.getByRole('button', { name: 'Delete' }).click()
  await expect(page.getByRole('cell', { name: 'Healthcare benefit2' })).not.toBeVisible()
  // Delete category
  await page.getByText('Government2').click()
  await page.getByRole('button', { name: 'Delete' }).click()
  await expect(page.getByText('Government2')).not.toBeVisible()
})

test('CRUD transaction', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Finances' }).click()
  await page.getByRole('link', { name: 'Transacties' }).click()
  // Create transaction
  await page.getByRole('button', { name: 'New transaction' }).click()
  await page.getByTestId('transaction-form').getByLabel('Date').fill('2021-01-01')
  await page.getByLabel('Amount').fill('70')
  await page.getByTestId('transaction-form').getByLabel('Category').selectOption('6')
  await page.getByTestId('transaction-form').getByLabel('Specification').selectOption('8')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.locator('tbody')).toContainText('2021-01-01')
  await expect(page.locator('tbody')).toContainText('€ 70,00')
  await expect(page.locator('tbody')).toContainText('Home')
  await expect(page.locator('tbody')).toContainText('Household')
  // Update transaction
  await page.getByRole('button', { name: 'Edit' }).click()
  await page.getByLabel('Amount').press('ArrowLeft')
  await page.getByLabel('Amount').fill('80')
  await page.getByTestId('transaction-form').getByLabel('Specification').selectOption('7')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.locator('tbody')).toContainText('€ 80,00')
  await expect(page.locator('tbody')).toContainText('Rent')
  // Delete transaction
  await page.getByRole('button', { name: 'Edit' }).click()
  await page.getByRole('button', { name: 'Delete' }).click()
  await expect(page.locator('table')).not.toContainText('2021-01-01')
})

test('Search Transactions', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Finances' }).click()
  await page.getByRole('link', { name: 'Transacties' }).click()
  // Hit search
  await page.getByLabel('Start date (incl.)').fill('2020-01-01')
  await page.getByLabel('End date (excl.)').fill('2020-01-10')
  await page.getByRole('button', { name: 'Search' }).click()
  await expect(page.getByRole('cell', { name: '2020-01-05' })).toBeVisible()
  await page.getByRole('button', { name: 'sort' }).click()
  // Find out how to assert sort later
  await expect(page.getByRole('cell', { name: '2020-01-05' })).toBeVisible()
})

test('CRUD dataset', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Finances' }).click()
  await page.getByRole('link', { name: 'Zoektermen' }).click()
  // Create dataset
  await page.getByRole('button', { name: 'New Datasets' }).click()
  await page.getByLabel('name').fill('CreditCard')
  await page.getByLabel('date_field').fill('Date')
  await page.getByLabel('date_format').fill('%Y%m%d')
  await page.getByLabel('amount_field').fill('Amount')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'CreditCard' })).toBeVisible()
  // Update dataset
  await page.getByTestId('datasets-2-update').click()
  await page.getByLabel('date_field').click()
  await page.getByLabel('date_field').fill('DateTime')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'DateTime' })).toBeVisible()
  // Delete dataset
  await page.getByTestId('datasets-2-update').click()
  await page.getByRole('button', { name: 'Delete' }).click()
  await expect(page.getByRole('cell', { name: 'CreditCard' })).not.toBeVisible()
})

test('CRUD search term', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Finances' }).click()
  await page.getByRole('link', { name: 'Zoektermen' }).click()
  await page.getByRole('cell', { name: 'MyBank' }).click()
  // Create search term
  await page.getByRole('button', { name: 'New Search Terms of MyBank' }).click()
  await page.getByTestId('search-terms of mybank-form').getByLabel('term').fill('Hammer&Co')
  await page.getByTestId('search-terms of mybank-form').getByLabel('field').fill('Notes')
  await page.getByLabel('Category').selectOption('6')
  await page.getByLabel('Specification').selectOption('8')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'Hammer&Co' })).toBeVisible()
  // Update search term
  await page.getByTestId('search-terms of mybank-3-update').click()
  await page.getByTestId('search-terms of mybank-form').getByLabel('term').fill('Hammer&Company')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'Hammer&Company' })).toBeVisible()
  // Delete search term
  await page.getByTestId('search-terms of mybank-3-update').click()
  await page.getByRole('button', { name: 'Delete' }).click()
  await expect(page.getByRole('cell', { name: 'Hammer&Company' })).not.toBeVisible()
})

test('CRUD balanceitem', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Finances' }).click()
  await page.getByRole('link', { name: 'Balans' }).click()
  // Create balance item
  await page.getByRole('button', { name: 'New Balance Item' }).click()
  await page.getByLabel('Name').fill('Mortgage')
  await page.getByLabel('Type').selectOption('liability')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'Mortgage' })).toBeVisible()
  // Update balance item
  await page.getByTestId('balance-item-5-update').click()
  await page.getByLabel('Name').fill('Mortgages')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'Mortgages' })).toBeVisible()
  // Delete balance item
  await page.getByTestId('balance-item-5-update').click()
  await page.getByRole('button', { name: 'Delete' }).click()
  await expect(page.getByRole('cell', { name: 'Mortgages' })).not.toBeVisible()
})

test('CRUD balance', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Finances' }).click()
  await page.getByRole('link', { name: 'Balans' }).click()
  // Add balance
  await page.locator('#selected_date').fill('2021-01-01')
  await page.getByRole('button', { name: 'Add Balance' }).click()
  await page.getByLabel('Add item').selectOption('1')
  await page.getByRole('button', { name: 'Add', exact: true }).click()
  await page.getByLabel('Savings account').fill('1000')
  await page.getByLabel('Add item').selectOption('2')
  await page.getByRole('button', { name: 'Add', exact: true }).click()
  await page.getByLabel('Checking account').fill('500')
  await page.getByLabel('Add item').selectOption('3')
  await page.getByRole('button', { name: 'Add', exact: true }).click()
  await page.getByLabel('Study loan').fill('4000')
  await page.getByLabel('Add item').selectOption('4')
  await page.getByRole('button', { name: 'Add', exact: true }).click()
  await page.getByLabel('Stocks').fill('2000')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: '-01-01' })).toBeVisible()
  await expect(page.getByLabel('Balance', { exact: true }).locator('tbody')).toContainText('€ 1.500,00')
  await expect(page.getByTestId('balance-table').locator('tbody')).toContainText('€ 1.500,00')
  // Update balance
  await page.getByRole('cell', { name: '2021-01-01' }).click()
  await page.getByLabel('Checking account').fill('800')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByTestId('balance-table').locator('tbody')).toContainText('€ 1.800,00')
  // Delete balance
  await page.getByRole('cell', { name: '2021-01-01' }).click()
  await page.getByTestId('delete-1').click()
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByTestId('balance-table').locator('tbody')).not.toContainText('€ 1.800,00')
})

test('Budget update', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Finances' }).click()
  await page.getByRole('link', { name: 'Rapporten' }).click()
  await page.getByRole('link', { name: 'Budget' }).click()
  await expect(page.locator('tbody')).toContainText('€ 1.000,00')
  // Update
  await page.getByRole('cell', { name: 'Work' }).click()
  await page.getByTestId('new-budget-2').fill('2000')
  await page.getByRole('button', { name: 'Store new budget' }).click()
  await expect(page.locator('tbody')).toContainText('€ 2.000,00')
})

test('Year table', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Finances' }).click()
  await page.getByRole('link', { name: 'Rapporten' }).click()
  await page.getByRole('link', { name: 'Year Table' }).click()
  await page.getByLabel('Start date').fill('2020-01-01')
  await page.getByLabel('End date').fill('2021-01-01')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByLabel('Income year table').locator('tfoot')).toContainText('€ 2.000,00')
  await expect(page.getByLabel('Expense year table').locator('tfoot')).toContainText('€ 1.550,00')
  await page.getByRole('button', { name: '€ -150,00' }).click()
  await expect(page.getByText('Geen mogelijk foute transacties gevonden')).toBeVisible()
})

test('Amortize items', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Finances' }).click()
  await page.getByRole('link', { name: 'Afschrijven' }).click()
  await expect(page.getByRole('paragraph')).toContainText('Total monthly cost: € 0,00')
  await page.getByRole('button', { name: 'New Amortize Items' }).click()
  await page.getByLabel('Name').fill('Glasses')
  await page.getByLabel('Price').fill('700')
  await page.getByLabel('Expected lifetime (months)').fill('60')
  await page.getByLabel('Purchase date').fill('2020-10-10')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'Glasses' })).toBeVisible()
  await expect(page.getByRole('paragraph')).toContainText('Total monthly cost: € 11,67')
  await expect(page.getByTestId('amortize-items-1')).toContainText('oktober 2025')
})

test('Investments', async ({ page }) => {
  await loginBarry(page)
  await page.getByRole('link', { name: 'Finances' }).click()
  await page.getByRole('link', { name: 'Investeringen' }).click()
  // Create new investment account
  await page.getByRole('button', { name: 'Add new account' }).click()
  await page.getByLabel('Name').click()
  await page.getByLabel('Name').fill('Stonks')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('heading', { name: 'Stonks' })).toBeVisible()
  await page.getByRole('link', { name: 'Open' }).click()
  await expect(page.getByText('Huidige waarde € 0,00')).toBeVisible()
  // Upload file, but security is missing
  await page.getByRole('button', { name: 'Upload file' }).click()
  await page.getByText('Choose a file...').click()
  await page.getByLabel('File').setInputFiles(join(__dirname, 'test-data', 'investment_transactions.csv'))
  await page.getByRole('button', { name: 'Process' }).click()
  await expect(page.getByText('Onbekende aandelen: - VanEck')).toBeVisible()
  // Add new security
  await page.getByRole('button', { name: 'close modal' }).click()
  await page.getByRole('button', { name: 'Add new security' }).click()
  await page.getByLabel('Name', { exact: true }).fill('VanEck TM AEX UCITS ETF')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('heading', { name: 'VanEck TM AEX UCITS ETF' })).toBeVisible()
  // Upload file when security does properly exist
  await page.getByRole('button', { name: 'Upload file' }).click()
  await page.getByText('Choose a file...').click()
  await page.getByLabel('File').setInputFiles(join(__dirname, 'test-data', 'investment_transactions.csv'))
  await page.getByRole('button', { name: 'Process' }).click()
  await expect(page.getByText('Rendement: -99%')).toBeVisible()
  // Update balance
  await page.getByLabel('Current value (null)').fill('70')
  await page.getByRole('button', { name: 'Update' }).click()
  await expect(page.getByText('Rendement: 28%')).toBeVisible()
})