import { expect, test } from '@playwright/test'
import { failOnJsError, loginKara, saveCoverage } from './utils'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('Links', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Links' }).click()
  await expect(page.getByPlaceholder('search')).toBeVisible()
  // Create new category
  await page.getByRole('link', { name: 'admin' }).click()
  await page.locator('#category-name').fill('Development')
  await page.locator('#category-submit').click()
  await expect(page.getByTestId('category-1')).toBeVisible()
  // Create new application with url
  await page.getByLabel('Category').selectOption('1')
  await page.getByLabel('Application Name').fill('Gitlab')
  await page.getByPlaceholder('name').first().fill('main')
  await page.getByPlaceholder('url').first().fill('https://gitlab.com')
  await page.locator('#application-submit').click()
  await expect(page.getByTestId('application-1').getByRole('textbox')).toHaveValue('Gitlab')
  await expect(page.getByTestId('url-1').locator('input[name="url"]')).toHaveValue('https://gitlab.com')
  // Add another URL
  await page.getByLabel('Application', { exact: true }).selectOption('1')
  await page.getByLabel('URL Name').fill('secondary')
  await page.getByLabel('URL', { exact: true }).fill('https://example.com')
  await page.getByLabel('Position').fill('2')
  await page.locator('#url-submit').click()
  await expect(page.locator('input[name="url"]').nth(2)).toHaveValue('https://example.com')
  // check category on main page
  await page.getByRole('link', { name: 'Links' }).click()
  await expect(page.getByText('Development')).toBeVisible()
  await page.getByText('Development').hover()
  await expect(page.getByRole('link', { name: 'main' })).toBeVisible()
  // Check update URL
  await page.getByRole('link', { name: 'admin' }).click()
  await page.getByTestId('url-2').locator('input[name="url"]').fill('https://example.org')
  await page.getByTestId('url-2').getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByTestId('url-2').locator('input[name="url"]')).toHaveValue('https://example.org')
  // Check delete URL
  await page.getByTestId('url-2').getByRole('button', { name: 'Delete' }).click()
  await expect(page.getByTestId('url-2')).not.toBeVisible()
  // Check update application
  await page.getByTestId('application-1').getByRole('textbox').fill('Flubber')
  await page.getByTestId('application-1').getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByTestId('application-1').getByRole('textbox')).toHaveValue('Flubber')
  // Check delete application
  await page.getByTestId('application-1').getByRole('button', { name: 'Delete' }).click()
  await expect(page.getByTestId('application-1')).not.toBeVisible()
  // Check update category
  await page.locator('#category-name-1').fill('Flubber')
  await page.getByTestId('category-1').getByRole('button', { name: 'Submit' }).click()
  await expect(page.locator('#category-name-1')).toHaveValue('Flubber')
  // Check delete category
  await page.getByTestId('category-1').getByRole('button', { name: 'Delete' }).click()
  await expect(page.getByTestId('category-1')).not.toBeVisible()
})