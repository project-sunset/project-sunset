import { test, expect } from '@playwright/test'
import { failOnJsError } from './utils'

test.beforeEach(failOnJsError)

test('Smoke', async ({ page }) => {
  if (process.env.WEBSITE) {
    await page.goto(`https://${process.env.WEBSITE}/`)
  } else {
    await page.goto('http://localhost:8080/')
  }
  // Check that homepage is rendered
  await expect(page.getByRole('heading', { name: 'Hi, I am Bob Luursema' })).toBeVisible()
  await page.getByRole('link', { name: 'Sudoku Solver' }).click()
  await expect(page.getByRole('heading', { name: 'Sudoku Solver' })).toBeVisible()
  await page.locator('body').press('Alt+b')
  await page.locator('#presets').selectOption('wikipedia')
  // Hit a backend API endpoint
  await page.getByRole('button', { name: 'Solve' }).click()
  await expect(page.getByRole('heading', { name: 'Solution' })).toBeVisible()
})