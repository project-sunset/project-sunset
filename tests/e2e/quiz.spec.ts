import { test, expect } from '@playwright/test'
import { loginKara, saveCoverage, failOnJsError } from './utils'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('Create new quiz', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Quiz' }).click()
  // New quiz
  await page.getByRole('button', { name: 'New Quiz' }).click()
  await page.getByLabel('name').fill('Capitals!')
  await page.getByRole('button', { name: 'Submit' }).click()
  await page.getByTestId('quiz-2-goto').click()
  // New round
  await page.getByRole('button', { name: 'New Round' }).click()
  await page.getByLabel('Name').fill('The Netherlands')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'The Netherlands' })).toBeVisible()
  // New question
  await page.getByRole('button', { name: 'New Question' }).click()
  const question = JSON.stringify({
    type: 'mc',
    q: "What is the capital?",
    choices: ['Amsterdam', 'Den Haag']
  })
  await page.getByTestId('question-form').getByLabel('Question').fill(question)
  await page.getByLabel('Answer').fill('Amsterdam')
  await page.getByTestId('question-form').getByLabel('Round').selectOption('3')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'Amsterdam', exact: true })).toBeVisible()
})

test('Participate in quiz', async ({ page, browser }) => {
  // Start team
  await page.goto('http://localhost:8080/#/quiz/1/participate')
  await page.getByLabel('Bedenk een team naam').fill('Jabal')
  await page.getByRole('button', { name: 'Deelnemen!' }).click()
  await page.getByRole('button', { name: 'Ja!' }).click()
  await expect(page.getByText('Team: Jabal')).toBeVisible()
  await expect(page.getByText('De quiz is nog niet gestart,')).toBeVisible()
  // @ts-ignore
  const code = await page.evaluate(() => document.getElementById('teamcode').innerText)
  // Start quiz
  const managerContext = await browser.newContext()
  const managerPage = await managerContext.newPage()
  await loginKara(managerPage)
  await managerPage.getByRole('link', { name: 'Quiz' }).click()
  await managerPage.getByTestId('quiz-1-goto').click()
  await expect(managerPage.locator('#app')).toContainText('Controls')
  // Next round
  await expect(managerPage.getByTestId('current-round')).toContainText('Huidige ronde: 0')
  await managerPage.locator('#next-round').click()
  await expect(managerPage.getByTestId('current-round')).toContainText('Huidige ronde: 1')
  // Next question
  await expect(managerPage.getByTestId('current-question')).toContainText('Huidige vraag: 0')
  await managerPage.locator('#next-question').click()
  await expect(managerPage.getByTestId('current-question')).toContainText('Huidige vraag: 1')

  // Close page
  await saveCoverage({ page: managerPage })
  managerPage.close()
  managerContext.close()

  // Join team again, answer question
  const participant2Context = await browser.newContext()
  const participant2Page = await participant2Context.newPage()
  await participant2Page.goto('http://localhost:8080/#/quiz/1/participate')
  await participant2Page.getByLabel('Bedenk een team naam').fill('Jabal')
  await participant2Page.getByRole('button', { name: 'Deelnemen!' }).click()
  await participant2Page.getByLabel('Jabal bestaat al, vul hier de').fill(code)
  await participant2Page.getByRole('button', { name: 'Check' }).click()
  await expect(participant2Page.getByText('Team: Jabal')).toBeVisible()
  await expect(participant2Page.getByText('Which one is taller?')).toBeVisible()
  await participant2Page.getByRole('button', { name: 'Empire State Building' }).click()

  // Close page
  await saveCoverage({ page: participant2Page })
  participant2Page.close()
  participant2Context.close()

  // Check score
  await page.goto('http://localhost:8080/#/quiz/1/score')
  await expect(page.getByRole('cell', { name: 'Jabal' })).toBeVisible()
  await expect(page.locator('tbody')).toContainText('1')
})