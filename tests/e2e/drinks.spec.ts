import { test, expect } from '@playwright/test'
import { failOnJsError, saveCoverage } from './utils'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('Drinks Rememberer', async ({ page }) => {
  await page.goto('http://localhost:8080/#/')
  await page.getByRole('link', { name: 'Drinks Rememberer' }).click()
  await expect(page.locator('h1')).toContainText('Drinks Rememberer')
  await expect(page.locator('#option_Water')).toContainText('0')
  await page.locator('#option_Water').getByRole('button', { name: '+' }).click()
  await expect(page.locator('#option_Water')).toContainText('1')
  await page.locator('#option_Water').getByRole('button', { name: '-' }).click()
  await expect(page.locator('#option_Water')).toContainText('0')
  await page.locator('#option_Water').getByRole('button', { name: '-' }).click()
  await expect(page.locator('#option_Water')).toContainText('0')
})