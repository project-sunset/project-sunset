import { test } from '@playwright/test'
import { failOnJsError, saveCoverage } from './utils'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('Countdown', async ({ page }) => {
  await page.goto('http://localhost:8080/#/')
  await page.getByRole('link', { name: 'Countdown Timer' }).click()
  await page.getByLabel('Time in minutes').click()
  await page.getByLabel('Time in minutes').fill('1')
  await page.getByRole('button', { name: 'Start' }).click()
  await page.getByRole('button', { name: 'Pause' }).click()
  await page.getByRole('button', { name: 'Continue' }).click()
  await page.getByRole('button', { name: 'Reset' }).click()
})