import { test, expect } from '@playwright/test'
import { failOnJsError, saveCoverage } from './utils'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('Memory', async ({ page }) => {
  test.slow()
  const getShape = async (id: string): Promise<string> => {
    //@ts-ignore
    return page.evaluate((id: string) => {
      const canvas = document.getElementById(id)
      //@ts-ignore
      return canvas.dataset.shape
    }, [id])
  }

  await page.goto('http://localhost:8080/#/')
  await page.getByRole('link', { name: 'Memory game' }).click()

  const seen = {}
  let solvedSets = 0
  let nextCard = 0
  let currentTurn = 0

  await page.locator('#start').click()
  await page.waitForTimeout(4000)

  while (solvedSets < 10) {
    if (currentTurn > 20) {
      throw Error('Memory is not solved within 20 turns!')
    }
    // Click next card
    await page.locator(`#card${nextCard}`).click()
    const shape1 = await getShape(`card${nextCard}`)
    if (seen.hasOwnProperty(shape1)) {
      // If we have seen this shape already, click on the known card somewhere back
      await page.locator(`#card${seen[shape1]}`).click()
      await expect(page.locator(`#card${nextCard}`)).not.toBeVisible()
      solvedSets++
      nextCard++
    } else {
      // else store the location of this shape and click the next card
      seen[shape1] = `${nextCard}`
      await page.locator(`#card${nextCard + 1}`).click()
      const shape2 = await getShape(`card${nextCard + 1}`)
      if (shape1 === shape2) {
        // The next card happens to match the first one!
        await expect(page.locator(`#card${nextCard}`)).not.toBeVisible()
        solvedSets++
        nextCard = nextCard + 2
      } else if (seen.hasOwnProperty(shape2)) {
        // If we have already seen this shape, start with this one next turn
        nextCard++
      } else {
        // Else save it and skip to the one after it
        seen[shape2] = `${nextCard + 1}`
        nextCard = nextCard + 2
      }
    }
    currentTurn++
  }
  await expect(page.locator('#result')).toBeVisible()
})