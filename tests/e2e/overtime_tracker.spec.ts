import { expect, test } from '@playwright/test'
import { failOnJsError, loginKara, saveCoverage } from './utils'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

const chartJsRenderDuration = 1000 + 500

test('CRUD days', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Overtime Tracker' }).click()
  await page.waitForTimeout(chartJsRenderDuration)
  await expect(page.getByRole('heading', { name: 'Huidige overuren: 0' })).toBeVisible()
  await page.getByRole('link', { name: 'Dagen' }).click()
  // New day
  await page.getByRole('button', { name: 'New Dagen' }).click()
  await page.getByLabel('Datum').fill('2024-02-01')
  await page.getByLabel('Uren gewerkt').fill('8')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByTestId('dagen-4')).toContainText('2024-02-01')
  await expect(page.getByTestId('dagen-4')).toContainText('-0.5')
  // Update day
  await page.getByTestId('dagen-4-update').click()
  await page.getByLabel('Uren gewerkt').fill('9.75')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByTestId('dagen-4')).toContainText('1.25')
  // Delete day
  await page.getByTestId('dagen-4-update').click()
  await page.getByRole('button', { name: 'Delete' }).click()
  await expect(page.locator('tbody')).not.toContainText('2024-02-01')
})

test('CRUD break times', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Overtime Tracker' }).click()
  await page.waitForTimeout(chartJsRenderDuration)
  await page.getByRole('link', { name: 'Instellingen' }).click()
  // Check current
  await expect(page.getByTestId('break-times-1')).toContainText('2024-01-01')
  // Create new
  await page.getByRole('button', { name: 'New Break Times' }).click()
  await page.getByLabel('Date').fill('2024-02-01')
  await page.getByLabel('Break Time', { exact: true }).fill('1')
  await page.getByLabel('Working time until break').fill('6')
  await page.getByTestId('break-times-form').getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByTestId('break-times-2')).toContainText('2024-02-01')
  // Update
  await page.getByTestId('break-times-2').getByRole('button', { name: 'Update' }).click()
  await page.getByLabel('Break Time', { exact: true }).fill('1.5')
  await page.getByRole('button', { name: 'Submit' }).first().click()
  await expect(page.getByTestId('break-times-2')).toContainText('1.5')
  // Delete
  await page.getByTestId('break-times-2').getByRole('button', { name: 'Update' }).click()
  await page.getByRole('button', { name: 'Delete' }).click()
  await expect(page.locator('tbody')).not.toContainText('2024-02-01')
})

test('Timer', async ({ page}) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Overtime Tracker' }).click();
  await page.waitForTimeout(chartJsRenderDuration)      
  await page.getByRole('button', { name: 'Start timer' }).click();
  await expect(page.getByText('0:00')).toBeVisible();
  await page.getByRole('button', { name: 'Stop timer' }).click();
  await expect(page.getByRole('button', { name: 'Start timer' })).toBeVisible();
  await page.waitForTimeout(chartJsRenderDuration)
  await page.getByRole('link', { name: 'Dagen' }).click();
  await expect(page.getByRole('cell', { name: '-8' })).toBeVisible();
})