import { test, expect } from '@playwright/test'
import { loginKara, saveCoverage, failOnJsError } from './utils'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('CRUD items', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Groceries' }).click()
  await page.getByRole('link', { name: 'Items' }).click()
  await page.getByRole('button', { name: 'New Items' }).click()
  // Create item
  await page.getByLabel('name').fill('Cheese')
  await page.getByLabel('unit').selectOption('1')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'Cheese' })).toBeVisible()
  // Update item
  await page.getByTestId('items-5-update').click()
  await page.getByLabel('name').fill('Brie')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'Brie' })).toBeVisible()
  // Delete item
  await page.getByTestId('items-5-update').click()
  await page.getByRole('button', { name: 'Delete' }).click()
  await expect(page.getByRole('cell', { name: 'Brie' })).not.toBeVisible()
})

test('CRUD recipe', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Groceries' }).click()
  await page.getByRole('link', { name: 'Recipes' }).nth(1).click()
  // Create new recipe
  await page.getByRole('button', { name: 'New Recipes' }).click()
  await page.getByLabel('Name').fill('Sandwich')
  await page.getByLabel('Instructions').fill('Yeet the stuff on it')
  await page.getByLabel('Default servings').fill('1')
  await page.locator('#add-ingredient-name').fill('Bread')
  await page.locator('#add-ingredient-name').press('Tab')
  await expect(page.getByPlaceholder('filled automatically')).toHaveValue('_')
  await page.locator('#add-ingredient-amount').fill('1')
  await page.getByRole('button', { name: 'Add' }).click()
  await page.locator('#add-ingredient-name').fill('Egg')
  await page.locator('#add-ingredient-name').press('Tab')
  await expect(page.getByPlaceholder('filled automatically')).toHaveValue('_')
  await page.locator('#add-ingredient-amount').fill('2')
  await page.getByRole('button', { name: 'Add' }).click()
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'Sandwich' })).toBeVisible()
  // View recipe
  await page.getByTestId('recipes-3-goto').click()
  await expect(page.getByLabel('Instructions')).toBeVisible()
  await expect(page.getByLabel('Instructions')).toHaveValue('Yeet the stuff on it')
  // Update recipe
  await page.getByRole('button', { name: 'Edit' }).click()
  await page.getByLabel('Instructions').fill('Yeet the stuff on it, but first fry the eggs')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'Sandwich' })).toBeVisible()
  await page.getByTestId('recipes-3-goto').click()
  await expect(page.getByLabel('Instructions')).toHaveValue('Yeet the stuff on it, but first fry the eggs')
  // Delete recipe
  await page.getByRole('button', { name: 'Edit' }).click()
  await page.getByRole('button', { name: 'Delete' }).click()
  await expect(page.getByRole('cell', { name: 'Sandwich' })).not.toBeVisible()
})

test('Ingredient recipe update', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Groceries' }).click()
  await page.getByRole('link', { name: 'Recipes' }).nth(1).click()
  await page.getByTestId('recipes-1-goto').click()
  // Add ingredient
  await page.getByRole('button', { name: 'Edit' }).click()
  await page.locator('#add-ingredient-name').fill('Egg')
  await page.locator('#add-ingredient-name').press('Tab')
  await expect(page.getByPlaceholder('filled automatically')).toHaveValue('_')
  await page.locator('#add-ingredient-amount').fill('2')
  await page.getByRole('button', { name: 'Add' }).click()
  await page.getByRole('button', { name: 'Submit' }).click()
  await page.getByTestId('recipes-1-goto').click()
  await expect(page.getByRole('cell', { name: 'Egg' })).toBeVisible()
  await expect(page.getByRole('cell', { name: 'Beans' })).toBeVisible()
  // Remove ingredient
  await page.getByRole('button', { name: 'Edit' }).click()
  await page.getByRole('row', { name: 'Egg 2 _ Remove' }).getByRole('button').click()
  await page.getByRole('button', { name: 'Submit' }).click()
  await page.getByTestId('recipes-1-goto').click()
  await expect(page.getByRole('cell', { name: 'Egg' })).not.toBeVisible()
  await expect(page.getByRole('cell', { name: 'Beans' })).toBeVisible()
})

test('CRUD grocery list', async ({ page }) => {
  await loginKara(page)
  await page.getByRole('link', { name: 'Groceries' }).click()
  // Start grocery list
  await page.getByRole('link', { name: 'Grocery List' }).nth(1).click()
  // Add recipe
  await page.locator('#add-recipe-input').fill('Fried egg')
  await page.locator('#add-recipe').click()
  await expect(page.getByRole('cell', { name: 'Fried egg for' })).toBeVisible()
  // Delete recipe
  await page.getByRole('row', { name: 'Fried egg for 2 Delete' }).getByRole('button').click()
  await expect(page.getByRole('cell', { name: 'Fried egg for' })).not.toBeVisible()
  // Add item
  await page.locator('#item-name').fill('Beans')
  await page.locator('#item-name').press('Tab')
  await expect(page.getByPlaceholder('filled automatically')).toHaveValue('gram')
  await page.locator('#item-amount').fill('125')
  await page.locator('#add-item').click()
  await expect(page.getByRole('cell', { name: 'Beans', exact: true })).toBeVisible()
  // Remove item
  await page.getByRole('row', { name: 'Beans 125 gram Delete' }).getByRole('button').click()
  await expect(page.getByRole('cell', { name: 'Beans', exact: true })).not.toBeVisible()
  // Start shopping
  await page.getByRole('button', { name: 'Go shopping' }).click()
  await expect(page.getByText('1 Bread')).toBeVisible()
  await expect(page.getByText('gram Beans')).toBeVisible()
  await expect(page.getByText('gram Rice')).toBeVisible()
  // Cross off item
  await page.locator('li').filter({ hasText: 'Bread' }).getByRole('button').click()
  await expect(page.getByText('1 Bread')).not.toBeVisible()
  // And done
  await page.getByRole('button', { name: 'Finish list' }).click()
})