import { Page, expect } from "@playwright/test"
import crypto from 'crypto'
import { writeFileSync } from 'fs'

export async function loginKara(page: Page) {
  await page.goto('http://localhost:8080/#/')
  await page.getByLabel('Username').fill('Kara')
  await page.getByLabel('Username').press('Enter')
  await page.getByLabel('Password').fill('supergirl01')
  await page.getByRole('button', { name: 'Login' }).click()
}

export async function loginBarry(page: Page) {
  await page.goto('http://localhost:8080/#/')
  await page.getByLabel('Username').fill('Barry')
  await page.getByLabel('Username').press('Enter')
  await page.getByLabel('Password').fill('flash001')
  await page.getByRole('button', { name: 'Login' }).click()
}

declare global {
  interface Window {
    __coverage__: object
  }
}

export async function saveCoverage({ page }: { page: Page }) {
  const filename = crypto.randomUUID()
  const coverage = await page.evaluate(() => {
    if (!window.__coverage__) {
      console.warn('No coverage!')
      return '{}'
    }
    return JSON.stringify(window.__coverage__)
  })
  writeFileSync(`../.nyc_output/${filename}.json`, coverage)
}

export function failOnJsError({ page }: { page: Page }) {
  page.on('pageerror', exception => {
    console.log(`Uncaught exception: "${exception}"\n${exception.stack}`)
    expect(false, 'Javascript error happened on page!').toBe(true)
  })
}